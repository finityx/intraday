import time,json,numpy as np
from model_builder import live_run,run_prediction
import pandas as pd,os
from google.cloud import storage

main_dir = rf'D:/intraday_models/'

for prefix in ['stay_on_invest_shilo_21_10_2021']:#['small_aum_simple_model_short']:#['small_aum_simple_model_short']:#'small_aum_simple_model/',
    print(prefix)
    chosen_objective = 'default'

    start = time.time()
    checkpoint_path = rf"{main_dir}/{prefix}/checkpoints/{chosen_objective}/checkpoint"

    # for b in storage.client.Client().get_bucket('free_market_intraday_live').list_blobs(prefix=prefix+'/'):
    #     name = b.name.lstrip(prefix)
    #     if name == '/':
    #         continue
    #
    #     if name.endswith('.mat'):
    #         continue
    #     filepath = fr'{main_dir}/{name}'
    #     filepath = f'{main_dir}/{prefix}/{b.name[len(prefix)+1::]}'
    #
    #     os.makedirs(os.path.dirname(filepath),exist_ok=True)
    #     print(filepath)
    #     b.download_to_filename(filepath)

    with open(fr"{main_dir}/{prefix}/run.json") as f:
        dct = json.load(f)

    # model, invests = live_run(main_dir, dct['gui_dict'], dct['tickers'], checkpoint_path=checkpoint_path,curr_date=datetime(2021,7,21))
    model, invests = run_prediction(f'{main_dir}/{prefix}/', dct['gui_dict'], dct['tickers'], checkpoint_path=checkpoint_path)
