from live.run_live_models import *
import pandas_market_calendars as mcal

if __name__=='__main__':
    nyse = mcal.get_calendar('NYSE')



    main_dir = r'D:/test_live_int_multi_model'
    broker_runner = BrokerRunner()
    model_name='stay_on_invest_21_10_2021'
    for kdays in range(10):
        print(kdays)
        curr_date = datetime.now()-timedelta(kdays)
        trade_date = nyse.schedule(curr_date - timedelta(100),curr_date).market_open.keys()

        if trade_date.date[-1]!=curr_date.date():
            continue

        print(kdays,curr_date)
        download = False
        check_full_prediction = False
        start = time.time()

        models_params = {}

        trail_stop_loss = True
        handle_positions_separately = True
        trade_mod = 10
        model_params = {}
        model_params['chosen_objective'] = 'default'
        model_params['prefix'] = model_name
        model_params['amount_minute'] = amount_minute = 2000
        model_params['min_invest'] = 100
        model_params['quantization'] = amount_minute * 0.2
        models_params[model_name] = model_params

        position_handler_save_path = f'{main_dir}/position_handler/{curr_date.date()}.json'

        if download:
            for model_name,model_params in models_params.items():
                prefix = model_params['prefix']
                for b in storage.client.Client().get_bucket('free_market_intraday').list_blobs(prefix=prefix + '/'):
                    filepath = fr'{main_dir}/{model_name}/{b.name.lstrip(prefix)}'
                    os.makedirs(os.path.dirname(filepath), exist_ok=True)
                    print(filepath)
                    b.download_to_filename(filepath)

        tickers = []
        model_run_dicts = {}
        for model_name,model_params in models_params.items():
            with open(fr"{main_dir}/{model_name}/run.json") as f:
                model_run_dicts[model_name] = json.load(f)
            tickers+=model_run_dicts[model_name]['tickers']
        tickers = list(set(tickers))

        lookback = max([v['gui_dict']['ndays'] for v in model_run_dicts.values()])
        rel_dates = trade_date[-lookback::]
        actual_days = (rel_dates[-1]-rel_dates[0]).days + 1
        data_table_name = gen_curr_table_name(curr_date)

        if curr_date.date() == datetime.now().date():
            for ticker in tickers:
                if ticker!='cash':
                    contract = BrokerRunner.get_contract(ticker)
                    broker_runner.ib.reqHistoricalData(contract, '', f"{actual_days} D", "1 min", "TRADES", useRTH=1, keepUpToDate=True)

        os.makedirs(os.path.dirname(position_handler_save_path),exist_ok=True)

        prediction_queue = {}
        mission_queue = {}
        done_mission_queue = {}
        module_run_processes=[]
        max_iminute = {}
        for model_name, model_params in models_params.items():
            prediction_queue[model_name] = Queue(maxsize=1)
            mission_queue[model_name] = Queue(maxsize=1)
            done_mission_queue[model_name] = Queue(maxsize=1)
            checkpoint_path = rf"{main_dir}/{model_name}/checkpoints/{model_params['chosen_objective']}/checkpoint"
            prediction_dir = f'{main_dir}/offline_run/{model_name}/{curr_date.date().isoformat()}'
            module_run_process = Process(target=main_live_run, kwargs=
            {'prediction_queue': prediction_queue[model_name], 'mission_queue': mission_queue[model_name], 'done_mission_queue': done_mission_queue[model_name],
             'prediction_dir': prediction_dir, 'checkpoint_path': checkpoint_path, 'dct': model_run_dicts[model_name], 'curr_date': curr_date,
             'chosen_objective': model_params['chosen_objective'], 'amount_minute': model_params['amount_minute'], 'min_invest': model_params['min_invest'],
             'replace_dates':rel_dates})
            module_run_process.start()
            max_iminute[model_name] = 0
            module_run_processes.append(module_run_process)

        data_queue = Queue(maxsize=1)
        data_run_process = Process(target=main_table_create, kwargs={'data_table_name': data_table_name, 'mission_queue': mission_queue, 'done_mission_queue': done_mission_queue, 'data_queue': data_queue})
        data_run_process.start()

        broker_runner.ib.sleep(0)
        periodic_data_bars = OrderedDict()
        lengths = []
        for ticker in tickers:
            if ticker != 'cash':
                contract = BrokerRunner.get_contract(ticker)
                periodic_data_bars[ticker] = [b for b in broker_runner.ib.reqHistoricalData(contract, curr_date.date(), f"{actual_days} D", "1 min","TRADES", useRTH=1) if b.date.date() in [d.date() for d in rel_dates]]
                lengths.append(len(periodic_data_bars[ticker]))


        if (all([l==lengths[0] for l in lengths])):
            if lengths[0]==0:
                print('skip',curr_date)
                continue
            print('running', curr_date)

            last_price = {}
            last_date = {}
            bars = []
            iminute = 0
            for ticker, pdb in periodic_data_bars.items():
                tmp = [p.__dict__ for p in pdb]
                [t.update({'ticker': ticker}) for t in tmp]
                max_tmp = max(tmp, key=lambda x: x['date'])
                last_price[ticker] = max_tmp['close']
                last_date[ticker] = max_tmp['date']
                bars += tmp

                curr_iminute  = len([d for d in tmp if d['date'].date() == curr_date.date()]) - 1
                iminute = max([iminute, curr_iminute])

            assert all([v == last_date[ticker] for v in last_date.values()]), 'all tickers dates should be same'

            if iminute > max(max_iminute.values()):
                for model_name in mission_queue.keys():
                    max_iminute[model_name] = iminute
                data_queue.put({'bars': bars, 'iminute': iminute})

        for model_name in prediction_queue:
            prediction_result = prediction_queue[model_name].get()
            max_iminute[model_name] = prediction_result['max_iminute']
            print(prediction_result['max_iminute'],prediction_result)

        data_run_process.terminate()
        for p in module_run_processes:
            p.terminate()
