from datetime import datetime,timedelta
import pandas as pd
import pytz,numpy as np

live_data_dir = r"D:\test_live_int\375\data\stock_data"

trade_day   = '2021-07-21'
report_path = r"C:\Users\admin\Downloads\trade_reports.csv"


df = pd.read_csv(report_path)
df = df.sort_values(by='DateTime')
df.DateTime = pd.to_datetime(df.DateTime,format='%Y%m%d;%H%M%S')
df.index = df['DateTime']
df.DateTime = df.index.tz_localize(tz=pytz.timezone('America/New_York')).tz_convert(pytz.timezone('Asia/Jerusalem'))
trade_df = df[(df.DateTime.dt.date==datetime.fromisoformat(trade_day).date())]

stop_prices = []
for rn,row in trade_df.iterrows():
    sp = np.nan
    if row['Buy/Sell']=='BUY':
        df = pd.read_csv(fr"{live_data_dir}/{row.Symbol}_{trade_day}.csv")
        df = df.sort_values('date')
        df.index = pd.to_datetime(df['date'])
        df['date'] = df.index.tz_localize(None).tz_localize(tz=pytz.timezone('Asia/Jerusalem'))

        rel_df = df[df.date>=row.DateTime]
        high = rel_df.open.values[0]
        sp = np.nan
        for h,l in zip(rel_df.high,rel_df.low):
            if l<=high*(0.998):
                sp = l
                break
            h=max([h,high])

    stop_prices.append(sp)
trade_df['stop_prices']=stop_prices

print(trade_df[trade_df['stop_prices'].isna()==False][['Symbol','stop_prices']])

print('************************************************************************')
print(trade_df[trade_df['Buy/Sell']=='SELL'][['Symbol','TradePrice']])