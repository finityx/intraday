import json
import datetime,os
from model_builder import get_model
main_dir = r'D:\temp_test'
live_date = datetime.datetime(2021,9,14)
checkpoint_path = rf'{main_dir}\run_saves\checkpoints\default\checkpoint'
with open(rf'{main_dir}\run.json') as f:
    dct = json.load(f)
for cm in os.listdir(f'{main_dir}/live_test'):
    print(cm)
    pred_dir = f'{main_dir}/live_test/{cm}'
    dct['gui_dict']['batch_size'] = 1

    model = get_model(pred_dir,dct['gui_dict'],dct['tickers'],start_date=live_date,end_date=live_date,replace_dates=[],update_table_date=live_date,live=False)
    invests = model['runner'].predict_live(save_dir=f'{pred_dir}/live_prediction', checkpoint_path=checkpoint_path, ds_start_date=live_date, ds_end_date=live_date)
