from google.cloud import storage
import os
def download_mats(model_name,main_dir):
    prefix = f'{model_name}/mats'
    for b in storage.client.Client().get_bucket('free_market_intraday').list_blobs(prefix=prefix):
        filepath = f'{main_dir}/{model_name}/{b.name[len(prefix)+1::]}'
        if filepath.endswith('.mat'):
            try :
                os.makedirs(os.path.dirname(filepath),exist_ok=True)
                b.download_to_filename(filepath)
                print(filepath)

            except Exception as e:
                print(e)

if __name__ =='__main__':
    # download_mats('small_aum_simple_model_weighted_by_minute', fr'D:\int_models')
    # download_mats('stateless_trail_sl_sortino', fr'D:\int_models')
    # download_mats('small_aum_simple_model_short', fr'D:\int_models')
    # download_mats('small_aum_simple_model_spread_05', fr'D:\int_models')

    # download_mats('profit_test_new', fr'D:\int_models')
    # download_mats('profit_per_minute_test', fr'D:\int_models')
    # download_mats('position_mgr_etf_profit_per_minute_invest_change', fr'D:\int_models')
    # download_mats('position_mgr_etf_profit_per_minute_invest_change_zero_first_buy', fr'D:\int_models')

    # download_mats('crypto_profit_per_minute_invest_change', fr'D:\int_models')
    # download_mats('crypto_profit_per_minute_invest_change_zero_first_buy', fr'D:\int_models')

    # download_mats('profit_per_minute_invest_change', fr'D:\int_models')
    download_mats('profit_per_minute_invest_change_without_miunte_exists', fr'D:\int_models')

    # download_mats('crypto_safer_with_comis', fr'D:\int_models')
    # download_mats('crypto_safer', fr'D:\int_models')
