from binance import Client, ThreadedWebsocketManager, ThreadedDepthCacheManager
import numpy as np
from datetime import datetime,timedelta
import time
TABLE_NAME = f'algo_dataset.intraday_1min_crypto_binance_usdt'

def get_binance_pair(client,ticker_a,ticker_b,table_name):
    curr_date = datetime.now()
    dfs = []
    start_time = time.time()
    while curr_date>=datetime(2018,1,1):
        print(ticker_a,ticker_b,curr_date,time.time()-start_time)
        next_date = curr_date+timedelta(days=1)
        curr_date.strftime('%Y-%M-%d')
        klines = np.array(client.get_historical_klines(f'{ticker_a}{ticker_b}', Client.KLINE_INTERVAL_1MINUTE, curr_date.strftime('%d %b,%Y'),next_date.strftime('%d %b,%Y')))

        df = pd.DataFrame(klines.reshape(-1,12),dtype=float, columns = ('open_time',
                                                                        'open',
                                                                        'high',
                                                                        'low',
                                                                        'close',
                                                                        'volume',
                                                                        'close_time',
                                                                        'quote_asset_volume',
                                                                        'number_of_trades',
                                                                        'taker_buy_base_asset_volume',
                                                                        'taker_buy_quote_asset_volume',
                                                                        'ignore'))
        df['ticker'] = f'{ticker_a}-{ticker_b}'
        df['open_time']  = [pd.Timestamp(ot, unit='ms') for ot in df['open_time']]
        df['close_time'] = [pd.Timestamp(ot, unit='ms') for ot in df['close_time']]

        dfs.append(df)
        curr_date = curr_date - timedelta(days=1)
        if len(dfs)>1000:
            pd.concat(dfs).to_gbq(table_name,if_exists='append')
            dfs = []

    if len(dfs)>1:
        try:
            pd.concat(dfs).to_gbq(table_name,if_exists='append')
        except:
            pass


def download_crypto(pairs,table_name=TABLE_NAME):
    api_key    = 'Iei7ntsqnvtQWTUI7g9CWG3ndWLMuFYw7UEyiNNO4SkL0E4PZf2sDM2pmvTXZw4b'
    api_secret = 'qeqeMuOqp2Utlib1tUvYuDQjpQ0J4P61ptuGJHOU01ywFjAUzfPxjoqf9ugFCWqJ'
    client = Client(api_key, api_secret)
    binance_pairs = client.get_all_tickers()
    binance_pairs = [p['symbol'] for p in binance_pairs]
    for ticker_a,ticker_b in pairs:
        print(ticker_a,ticker_b)
        if f'{ticker_a}{ticker_b}' in binance_pairs:
             get_binance_pair(client,ticker_a,ticker_b,table_name)
        elif f'{ticker_b}{ticker_a}' in binance_pairs:
             get_binance_pair(client,ticker_b,ticker_a,table_name)
        # else:
        #     df_a = get_binance_pair(client, f'{ticker_a}USDT')
        #     df_b = get_binance_pair(client, f'{ticker_b}USDT')
        #     merged_df = pd.merge(df_a, df_b, on='open_time',how='inner')
        #     df = df_a.iloc[0:0]
        #     df.open_time = merged_df.open_time
        #     df.close_time = merged_df.close_time_x
        #
        #     for k in ['open', 'high', 'low', 'close']:
        #         df[k] = merged_df[f'{k}_x']/merged_df[f'{k}_y']
        #     for k in [ 'volume', 'quote_asset_volume', 'number_of_trades', 'taker_buy_base_asset_volume', 'taker_buy_quote_asset_volume', 'ignore']:
        #         df[k] = (merged_df[f'{k}_x']+merged_df[f'{k}_y'])/2
        #
        #     df.ticker = f'{ticker_a}{ticker_b}'

if __name__=='__main__':
    import pandas as pd
    import os
    df = pd.read_csv(f'{os.path.dirname(os.path.dirname(__file__))}/misc/crypto_pairs.csv')
    # pairs = [tuple(k.split('-')) for k in df['synthetic pairs'][df['synthetic pairs'].isna()==False].tolist() + df['Pairs'][df['Pairs'].isna()==False].tolist()]
    pairs = [tuple(k.split('-')) for k in df['Pairs'][df['Pairs'].isna()==False].tolist()]
    pairs =[(p,'USDT') for p in  list(set([p[0] for p in pairs] +[p[1] for p in pairs]))]
    # pairs = pairs[5::]
    download_crypto(pairs)
    # sd = StocksData(Data,r"C:\Users\admin\Documents\pic_models\daily\daily_stock_data")
    # sd.transform()
    # from data2net.data2net import Data2Net
    # from objective.objective import Objective
    # from datetime import datetime
    # tmp =Objective(r"test",'test')
    # tmp.label = CryptoLabel(r"crypto_label",tmp.name,1)
    # d2n = Data2Net(r"D:\crypto_debug",[CryptoData(r"crypto_data",1),Turnover(r"crypto_turnover",1)],[tmp],'open2close',tickers=['YFIUSDT','ALGOUSDT'],asset_type='HOURLY_CRYPTO')
    # # sl = CryptoData(r"crypto_data",1)
    # # sl.get(r"D:\crypto_debug", ['YFIUSDT','ALGOUSDT'],max_date=datetime.now(), query_handler = QueryHandler())
    # # sl.transform()
    # d2n.merge_data()

