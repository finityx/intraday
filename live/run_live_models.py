import time,json,numpy as np
from datetime import datetime,timedelta
from model_builder import live_run,run_prediction
from broker.broker_runner import BrokerRunner
from collections import OrderedDict
import pandas as pd,os
from data.stock_data import gen_curr_table_name
from google.cloud import storage
import random
import ib_insync
import pandas_market_calendars as mcal
from broker import config as broker_config
from multiprocessing import Process,Queue

def main_table_create(data_table_name,data_queue,done_mission_queue,mission_queue):
    while True:
        mission = data_queue.get()
        bars = mission['bars']
        iminute = mission['iminute']
        print('main_table_create',datetime.now(), iminute)
        bars_df = pd.DataFrame(bars)
        bars_df.to_gbq(data_table_name, if_exists='replace')
        for model_name,c_mission_queue in mission_queue.items():
            ##clear unfinished stuff
            while not c_mission_queue.empty():
                print(f'skipped mission {model_name} : {c_mission_queue.get()}')

            while not done_mission_queue[model_name].empty():
                print(f'clearing done mission {model_name} : {done_mission_queue[model_name].get()}')


            c_mission_queue.put({'iminute':iminute})

        for model_name, c_done_mission_queue in done_mission_queue.items():
            mission = c_done_mission_queue.get()
            print(f'date: {datetime.now()} model {model_name} finished {mission["iminute"]}')




def main_live_run(prediction_queue,mission_queue,done_mission_queue,prediction_dir,checkpoint_path,dct,curr_date,chosen_objective='default',amount_minute = 20000,min_invest=18000,quantization=1,replace_dates=None):
    while True:
        mission = mission_queue.get()
        iminute = mission['iminute']
        os.makedirs(f'{prediction_dir}/{iminute}',exist_ok=True)
        model, invests = live_run(f'{prediction_dir}/{iminute}', dct['gui_dict'], dct['tickers'], checkpoint_path=checkpoint_path, curr_date=curr_date,replace_dates=replace_dates)
        last_date_invest = invests[chosen_objective]['invests'][-1]

        invests_dollar = np.round((last_date_invest[:,:,iminute - 1] * amount_minute)/quantization)*quantization # take the minute before last instead of current (current seems to be not full candle )
        II = (abs(invests_dollar) > min_invest)
        (iticker,istrategy) = np.where(II)
        invests_dollar = invests_dollar[II]
        tickers = np.array(model['data2net'].tickers)[iticker].tolist()
        strategies_params = np.array(model['data2net'].strategies_params)[istrategy].tolist()

        if invests_dollar.size!=0:
            II = np.where(np.array(tickers)!='cash')[0]
            invests_dollar = np.array(invests_dollar)[II]
            tickers = np.array(tickers)[II].tolist()
            strategies_params = np.array(strategies_params)[II].tolist()


        print(f'prediction : {list(zip(invests_dollar,tickers,strategies_params))}')


        assert len(tickers) == len(invests_dollar) == len(strategies_params), 'invests_dollar and rel_rows must have same size'
        prediction_queue.put({'tickers':tickers,'strategies_params':strategies_params,'invests_dollar':invests_dollar,'max_iminute':iminute})
        done_mission_queue.put(mission)

class PositionHandlerLogger():
    def __init__(self, path):
        self.path = path
        self.header = True
        os.makedirs(os.path.dirname(self.path),exist_ok=True)

    def print(self,action_str,ticker_handler):
        df = {}
        df['time'] = [datetime.now().isoformat()]
        df['sign'] = [ticker_handler.sign]
        df['iminute'] = [ticker_handler.iminute]
        df['invests_dollar'] = [ticker_handler.invests_dollar]
        df['action'] = [action_str]
        df['ticker'] = [ticker_handler.ticker]
        df['model_name'] = [ticker_handler.model_name]
        df['bid'] = [ticker_handler.bid]
        df['ask'] = [ticker_handler.ask]
        df['pips'] = [ticker_handler.pips]
        df['stop_price'] = [ticker_handler.stop_price]
        df['last'] = [ticker_handler.last]
        df['pos'] = [ticker_handler.pos]
        df['sign'] = [ticker_handler.sign]
        pd.DataFrame(df).to_csv(self.path, mode='a+', header=self.header)
        print(df)
        self.header = False

class TickDataHandler():
    def __init__(self):
        self.tick_data = {}

    def trace(self,broker_runner,ticker):
        contract = ib_insync.Stock(symbol=ticker, exchange='SMART', currency='USD')
        c_tick_data = {}
        c_tick_data['bid_ask'] = broker_runner.ib.reqTickByTickData(contract=contract, tickType='BidAsk', numberOfTicks=1, ignoreSize=True)
        c_tick_data['last'] = broker_runner.ib.reqTickByTickData(contract=contract, tickType='Last', numberOfTicks=1, ignoreSize=True)

        for k_time in range(60):
            if np.isnan(c_tick_data['bid_ask'].ask)|np.isnan(c_tick_data['last'].last):
                broker_runner.ib.sleep(1)
            else:
                break

        self.tick_data[ticker] = c_tick_data


    def is_traced(self,ticker):
        return ticker in self.tick_data

class TickerPositionHandler():
    def __init__(self,iminute,invests_dollar,ticker,model_name,SL_percent,sign,tick_price_handler):
        self.iminute = iminute
        self.invests_dollar = invests_dollar
        self.pos  = 0
        self.pos_dollar  = 0
        self.prev_pos_dollar = 0
        self.prev_pos = 0
        self.price_bought = 0
        self.stop_price = None
        self.SL_percent = SL_percent
        self.sign = sign
        self.buy_orders = []
        self.SL_orders  = []
        self.pips_ratio = None
        self.ticker = ticker
        self.model_name = model_name
        self.tick_price_handler = tick_price_handler

    @property
    def bid(self):
        return  self.tick_price_handler.tick_data[self.ticker]['bid_ask'].bid

    @property
    def ask(self):
        return  self.tick_price_handler.tick_data[self.ticker]['bid_ask'].ask

    @property
    def last(self):
        return  self.tick_price_handler.tick_data[self.ticker]['last'].last

    @property
    def spread(self):
        return (self.tick_price_handler.tick_data[self.ticker]['bid_ask'].ask - self.tick_price_handler.tick_data[self.ticker]['bid_ask'].bid)

    @property
    def pips(self):
        return np.round(self.pips_ratio * self.spread * 100) / 100

    @property
    def is_traced(self):
        return self.tick_price_handler.is_traced(self.ticker)

    def trace(self,broker_runner):
        self.tick_price_handler.trace(broker_runner=broker_runner, ticker=self.ticker)
        broker_runner.ib.sleep(1)

    @property
    def lmtPrice_buy(self):
        if self.sign > 0:
            lmtPrice_buy = min(self.bid + self.pips, self.last)
        else:
            lmtPrice_buy = max(self.ask - self.pips, self.last)
        return lmtPrice_buy

    @property
    def lmtPrice_sell(self):
        if self.sign > 0:
            lmtPrice_sell = max(self.ask - self.pips, self.last)
        else:
            lmtPrice_sell = min(self.bid + self.pips, self.last)
        return  lmtPrice_sell

    @property
    def action_buy(self):
        if self.sign > 0:
            action_buy = 'BUY'
        else:
            action_buy = 'SELL'
        return  action_buy

    @property
    def action_sell(self):
        if self.sign > 0:
            action_sell = 'SELL'
        else:
            action_sell = 'BUY'
        return  action_sell


class PositionHandler():
    def __init__(self,main_dir,broker_runner,trail_stop_loss = False,handle_positions_separately=True):
        self.tickers_tick_data = TickDataHandler()
        self.broker_runner  = broker_runner
        self.bought_tickers = {}
        self.tickers_ticker_handlers = {}
        self.strategy_ticker_handlers = {}

        self.ticker_position_handler = []
        self.strategy_position_handler = []

        ti = datetime.now().strftime("%Y%m%d-%H%M%S")
        self.log = PositionHandlerLogger(rf"{main_dir}/logger/{ti}.csv")
        self.trail_stop_loss = trail_stop_loss
        self.handle_positions_separately = handle_positions_separately

    def buy_prediction(self,prediction_result,model_name):
        broker_runner = self.broker_runner
        broker_runner.ib.sleep(0)

        if not self.handle_positions_separately:
            self.ticker_position_handler = []


        ## BUY
        invest_per_ticker = OrderedDict()
        ratio = OrderedDict()
        sm_ratio = {}
        for ticker, params, invests_dollar in zip(prediction_result['tickers'],prediction_result['strategies_params'], prediction_result['invests_dollar']):
            params_str = json.dumps(params)
            max_iminute = prediction_result['max_iminute']
            if ticker not in invest_per_ticker:
                invest_per_ticker[ticker] = 0.0
            invest_per_ticker[ticker] += invests_dollar*params['sign']

            ratio[(ticker, params_str)] = invests_dollar
            if ticker not in sm_ratio:
                sm_ratio[ticker] = 0.0
            sm_ratio[ticker] += invests_dollar

        for (ticker,params_str) in ratio.keys():
            ratio[(ticker,params_str)] = ratio[(ticker,params_str)]/sm_ratio[ticker]


        for ticker,invests_dollar in invest_per_ticker.items():
            if model_name not in self.bought_tickers:
                self.bought_tickers[model_name] = []
            buy_sign = np.sign(invests_dollar)
            invests_dollar = abs(invests_dollar)
            if ((ticker,model_name) in self.tickers_ticker_handlers) and (not self.handle_positions_separately):
                ticker_handler = self.tickers_ticker_handlers[(ticker,model_name)]
            else:
                self.tickers_ticker_handlers[(ticker,model_name)] = ticker_handler = TickerPositionHandler(iminute=max_iminute,invests_dollar=invests_dollar,ticker=ticker,model_name=model_name,SL_percent=0,sign=buy_sign,tick_price_handler=self.tickers_tick_data)

            self.ticker_position_handler.append(ticker_handler)

            if not ticker_handler.is_traced:
                ticker_handler.trace(broker_runner)
            broker_runner.ib.sleep(0)

            filled = 0.0
            q_filled = 0.0 + 1e-10
            for order in ticker_handler.buy_orders:
                filled += order.filled()
                q_filled += order.order.totalQuantity
                if order.orderStatus.status!='Filled':
                    broker_runner.ib.cancelOrder(order.order)

            ticker_handler.buy_orders = []

            ticker_handler.pos_dollar = ticker_handler.prev_pos_dollar + filled*ticker_handler.price_bought
            ticker_handler.pos = ticker_handler.prev_pos + filled

            ticker_handler.prev_pos_dollar = ticker_handler.pos_dollar
            ticker_handler.prev_pos = ticker_handler.pos

            contract = BrokerRunner.get_contract(ticker)

            delta_invest = (invests_dollar*params['sign'] - ticker_handler.pos_dollar*ticker_handler.sign)/params['sign']
            ticker_handler.pos_dollar = invests_dollar
            ticker_handler.sign       = params['sign']
            ticker_handler.pips_ratio = 0.1

            if ticker_handler.price_bought == 0:
                ticker_handler.price_bought = ticker_handler.last

            if delta_invest>1000:
                action = ticker_handler.action_buy
                lmt_price = ticker_handler.lmtPrice_buy
            elif delta_invest<-1000:
                action = ticker_handler.action_sell
                lmt_price = ticker_handler.lmtPrice_sell
            else:
                continue


            full_quantity = int(abs(delta_invest/ticker_handler.price_bought))
            ticker_handler.pos+=full_quantity

            while full_quantity>0:
                quantity = min([99,full_quantity])
                full_quantity = full_quantity-quantity
                cur_order = ib_insync.LimitOrder(action=action, totalQuantity=quantity, lmtPrice=lmt_price, orderId=broker_runner.ib.client.getReqId())
                cur_order.faGroup = broker_runner.fa_group
                cur_order.faMethod = broker_runner.faMethod
                ticker_handler.buy_orders.append(broker_runner.ib.placeOrder(contract, cur_order))
                broker_runner.ib.sleep(0)
            self.log.print('buy_prediction',ticker_handler)
            self.bought_tickers[model_name].append(ticker)

            if ticker_handler.pos_dollar==0:
                ticker_handler.price_bought = 0

        if self.handle_positions_separately == False:
            for (ticker,model_name) in self.tickers_ticker_handlers:
                ticker_handler = self.tickers_ticker_handlers[(ticker,model_name)]

                if (ticker_handler.pos!=0) and (ticker not in prediction_result['tickers']):
                    filled = 0.0
                    q_filled = 0.0 + 1e-10
                    for order in ticker_handler.buy_orders:
                        filled += order.filled()
                        q_filled += order.order.totalQuantity
                        if order.orderStatus.status != 'Filled':
                            broker_runner.ib.cancelOrder(order.order)

                    ticker_handler.buy_orders = []

                    ticker_handler.pos_dollar = ticker_handler.prev_pos_dollar + filled * ticker_handler.price_bought
                    ticker_handler.pos = ticker_handler.prev_pos + filled

                    ticker_handler.prev_pos_dollar = ticker_handler.pos_dollar
                    ticker_handler.prev_pos = ticker_handler.pos

                    contract = BrokerRunner.get_contract(ticker)

                    delta_invest = 0 - ticker_handler.pos_dollar
                    ticker_handler.pos_dollar = 0.0

                    ticker_handler.pips_ratio = 0.1

                    if ticker_handler.price_bought == 0:
                        ticker_handler.price_bought = ticker_handler.last

                    if delta_invest > 0:
                        action = ticker_handler.action_buy
                        lmt_price = ticker_handler.lmtPrice_buy
                    elif delta_invest < 0:
                        action = ticker_handler.action_sell
                        lmt_price = ticker_handler.lmtPrice_sell
                    else:
                        continue

                    # ticker_handler.pos = ticker_handler.pos + full_quantity
                    #                      / ticker_handler.last)

                    full_quantity = int(abs(delta_invest / ticker_handler.price_bought))
                    ticker_handler.pos  = 0.0
                    ticker_handler.stop_price = 0.0

                    while full_quantity > 0:
                        quantity = min([99, full_quantity])
                        full_quantity = full_quantity - quantity
                        cur_order = ib_insync.LimitOrder(action=action, totalQuantity=quantity, lmtPrice=lmt_price, orderId=broker_runner.ib.client.getReqId())
                        cur_order.faGroup = broker_runner.fa_group
                        cur_order.faMethod = broker_runner.faMethod
                        ticker_handler.buy_orders.append(broker_runner.ib.placeOrder(contract, cur_order))
                        broker_runner.ib.sleep(0)

                    self.log.print('buy_prediction', ticker_handler)
                    self.bought_tickers[model_name].append(ticker)

                    if ticker_handler.pos_dollar == 0:
                        ticker_handler.price_bought = 0


        ## STOP LOSS
        if self.handle_positions_separately:
            self.strategy_position_handler = []


        for ticker, params, invests_dollar in zip(prediction_result['tickers'],prediction_result['strategies_params'], prediction_result['invests_dollar']):
            if model_name not in self.bought_tickers:
                self.bought_tickers[model_name] = []
            params_str = json.dumps(params)

            ticker_ticker_handler = self.tickers_ticker_handlers[(ticker, model_name)]
            if ((ticker,model_name,params_str) in self.strategy_ticker_handlers) and (not self.handle_positions_separately):
                ticker_handler = self.strategy_ticker_handlers[(ticker,model_name,params_str)]
            else:
                self.strategy_ticker_handlers[(ticker,model_name,params_str)] = ticker_handler = TickerPositionHandler(iminute=prediction_result['max_iminute'],invests_dollar=invests_dollar,ticker=ticker,model_name=model_name,SL_percent=params['trailing_sl']*100,sign=params['sign'],tick_price_handler=self.tickers_tick_data)

            SL_percent = ticker_handler.SL_percent

            ticker_handler.SL_orders = []
            self.strategy_position_handler.append(ticker_handler)
            if not ticker_handler.is_traced:
                ticker_handler.trace(broker_runner)
            broker_runner.ib.sleep(0)

            ticker_handler.pos_dollar = ratio[(ticker,params_str)]*ticker_ticker_handler.pos_dollar
            ticker_handler.pos = int(np.round(ratio[(ticker,params_str)]*ticker_ticker_handler.pos))

            ticker_handler.stop_price = ticker_handler.last * (1 - ticker_handler.sign*(SL_percent / 100))
            ticker_handler.pos_dollar = invests_dollar
            ticker_handler.sign       = params['sign']
            ticker_handler.pips_ratio = 0.1

        broker_runner.ib.sleep(0)

    def handle_position(self):
        broker_runner = self.broker_runner
        for ticker_handler in self.ticker_position_handler:
            ticker = ticker_handler.ticker
            SL_percent = ticker_handler.SL_percent

            if ticker_handler.pos != 0:
                for buy_order in ticker_handler.buy_orders:

                    if buy_order.orderStatus.status == 'Filled':
                        ticker_handler.buy_orders.remove(buy_order)

                    elif (abs(ticker_handler.lmtPrice_buy - buy_order.order.lmtPrice) > 0.015):
                        try:
                            ticker_handler.pips_ratio+= 0.01
                            buy_order.order.lmtPrice = ticker_handler.lmtPrice_buy
                            ticker_handler.buy_orders[ticker_handler.buy_orders.index(buy_order)] = broker_runner.ib.placeOrder(buy_order.contract, buy_order.order)
                            broker_runner.ib.sleep(0)
                            self.log.print('update_buy_prediction', ticker_handler)
                        except:
                            self.log.print('error_in_update_buy_prediction', ticker_handler)

        for ticker_handler in self.strategy_position_handler:
            if ticker_handler.pos != 0:
                if self.trail_stop_loss:
                    if ticker_handler.sign > 0:
                        if (ticker_handler.last * (1 - (SL_percent / 100))) > ticker_handler.stop_price:
                            try:
                                ticker_handler.stop_price = ticker_handler.last * (1 - (SL_percent / 100))
                                self.log.print('update_stop_price', ticker_handler)
                            except:
                                self.log.print('error_in_update_stop_price', ticker_handler)
                                broker_runner.ib.sleep(0)
                    else:
                        if (ticker_handler.last * (1 + (SL_percent / 100))) < ticker_handler.stop_price:
                            try:
                                ticker_handler.stop_price = ticker_handler.last * (1 + (SL_percent / 100))
                                self.log.print('update_stop_price', ticker_handler)
                            except:
                                self.log.print('error_in_update_stop_price', ticker_handler)
                                broker_runner.ib.sleep(0)

                if ((ticker_handler.last <= ticker_handler.stop_price) and (ticker_handler.sign>0)) or ((ticker_handler.last >= ticker_handler.stop_price) and (ticker_handler.sign<0)):
                    self.log.print('sell', ticker_handler)
                    contract = BrokerRunner.get_contract(ticker)
                    full_quantity = ticker_handler.pos
                    ticker_handler.pips_ratio = 0.1

                    while full_quantity>0:
                        quantity = min([99, full_quantity])
                        full_quantity = full_quantity - quantity
                        cur_order = ib_insync.LimitOrder(action=ticker_handler.action_sell, totalQuantity=quantity,lmtPrice=ticker_handler.lmtPrice_sell,orderId=broker_runner.ib.client.getReqId())
                        cur_order.faGroup  = broker_runner.fa_group
                        cur_order.faMethod = broker_runner.faMethod
                        ticker_handler.SL_orders.append(broker_runner.ib.placeOrder(contract, cur_order))
                        broker_runner.ib.sleep(0)
                        ticker_handler.stop_price = 0
                        ticker_handler.pos = 0
                        ticker_handler.pos_dollar = 0
                        ticker_handler.price_bought = 0



            for sell_order in ticker_handler.SL_orders:
                if sell_order.orderStatus.status == 'Filled':
                    ticker_handler.SL_orders.remove(sell_order)


                elif (abs(ticker_handler.lmtPrice_sell- sell_order.order.lmtPrice) > 0.015):
                    try:
                        ticker_handler.pips_ratio += 0.01
                        sell_order.order.lmtPrice = ticker_handler.lmtPrice_sell
                        ticker_handler.SL_orders[ticker_handler.SL_orders.index(sell_order)] = broker_runner.ib.placeOrder(sell_order.contract, sell_order.order)
                        broker_runner.ib.sleep(0)
                        self.log.print('update_sell', ticker_handler)
                    except:
                        self.log.print('error_in_update_sell', ticker_handler)
                        broker_runner.ib.sleep(0)

    def end_of_day_close(self):
        broker_runner = self.broker_runner
        broker_runner.ib.sleep(60)

        for ticker_handler in self.ticker_position_handler:
            for order in ticker_handler.buy_orders:
                broker_runner.ib.cancelOrder(order.order)
            for order in ticker_handler.SL_orders:
                broker_runner.ib.cancelOrder(order.order)
            ticker_handler.SL_orders  = []
            ticker_handler.buy_orders = []

        for ticker_handler in self.ticker_position_handler:
            ticker = ticker_handler.ticker
            if ticker_handler.pos != 0:

                self.log.print('sell_at_close', ticker_handler)

                contract = BrokerRunner.get_contract(ticker)
                full_quantity = ticker_handler.pos

                ticker_handler.pips_ratio = 0.1
                while full_quantity>0:
                    quantity = min([99, full_quantity])
                    full_quantity = full_quantity - quantity
                    cur_order = ib_insync.LimitOrder(action=ticker_handler.action_sell, totalQuantity=quantity,lmtPrice=ticker_handler.lmtPrice_sell,orderId=broker_runner.ib.client.getReqId())
                    cur_order.faGroup  = broker_runner.fa_group
                    cur_order.faMethod = broker_runner.faMethod
                    ticker_handler.SL_orders.append(broker_runner.ib.placeOrder(contract, cur_order))
                    broker_runner.ib.sleep(0)
                    ticker_handler.stop_price = 0
                    ticker_handler.pos = 0

        while any([ticker_handler.SL_orders!=[] for ticker_handler in self.ticker_position_handler]):
            for ticker_handler in self.ticker_position_handler:
                for sell_order in ticker_handler.SL_orders:
                    if sell_order.orderStatus.status == 'Filled':
                        ticker_handler.SL_orders.remove(sell_order)

                    elif (abs(ticker_handler.lmtPrice_sell - sell_order.order.lmtPrice) > 0.015):
                        try:
                            ticker_handler.pips_ratio += 0.01
                            sell_order.order.lmtPrice = ticker_handler.lmtPrice_sell
                            ticker_handler.SL_orders[ticker_handler.SL_orders.index(sell_order)] = broker_runner.ib.placeOrder(sell_order.contract, sell_order.order)
                            broker_runner.ib.sleep(0)
                            self.log.print('update_sell_at_close', ticker_handler)
                        except:
                            self.log.print('error_in_update_sell_at_close', ticker_handler)
                            broker_runner.ib.sleep(0)

    @staticmethod
    def load(json_path,main_dir,broker_runner):
        with open(json_path) as f:
            dct = json.load(f)
        position_handler = PositionHandler(main_dir,broker_runner)
        position_handler.bought_tickers = dct['bought_tickers']
        for ticker_dict in dct['ticker_position_handler']:
            ticker_handler = TickerPositionHandler(ticker=ticker_dict['ticker'], model_name=ticker_dict['model_name'], SL_percent=ticker_dict['SL_percent'], sign=ticker_dict['sign'], tick_price_handler=position_handler.tickers_tick_data)
            position_handler.ticker_position_handler.append(ticker_handler)
            if not ticker_handler.is_traced:
                ticker_handler.trace(broker_runner)
            broker_runner.ib.sleep(0)
            ticker_handler.stop_price = ticker_dict['stop_price']
            ticker_handler.pos        = ticker_dict['pos']
            ticker_handler.pips_ratio = ticker_dict['pips_ratio']
        return position_handler

    def save(self,json_path):
        dct = {}
        dct['bought_tickers'] = position_handler.bought_tickers
        dct['ticker_position_handler'] = []
        for ticker_cls in position_handler.ticker_position_handler:
            dct['ticker_position_handler'].append({'ticker':ticker_cls.ticker,'model_name':ticker_cls.model_name,
                                              'SL_percent':ticker_cls.SL_percent,'sign':ticker_cls.sign,
                                              'stop_price':ticker_cls.stop_price,'pos':ticker_cls.pos,
                                              'pips_ratio':ticker_cls.pips_ratio})

        with open(json_path,'w+') as f:
            json.dump(dct,f)


if __name__=='__main__':

    close_time = mcal.get_calendar('NYSE').schedule(datetime.now(), datetime.now(), 'UTC').market_close[0] - timedelta(minutes=15)

    main_dir = r'D:/test_live_int_multi_model'
    curr_date = datetime.now()

    download = False
    check_full_prediction = False
    start = time.time()

    models_params = {}

    trail_stop_loss = False
    handle_positions_separately = False
    trade_mod = 1
    model_params = {}
    model_params['chosen_objective'] = 'default'
    model_params['prefix'] = 'stay_on_invest_21_10_2021'
    model_params['amount_minute'] = amount_minute = 5000
    model_params['min_invest'] = 1
    model_params['quantization'] = amount_minute * 0.02
    models_params['stay_on_invest_21_10_2021'] = model_params

    position_handler_save_path = f'{main_dir}/position_handler/{curr_date.date()}.json'

    if download:
        for model_name,model_params in models_params.items():
            prefix = model_params['prefix']
            for b in storage.client.Client().get_bucket('free_market_intraday_live').list_blobs(prefix=prefix + '/'):
                filepath = fr'{main_dir}/{model_name}/{b.name.lstrip(prefix)}'
                os.makedirs(os.path.dirname(filepath), exist_ok=True)
                print(filepath)
                b.download_to_filename(filepath)

    tickers = []
    model_run_dicts = {}
    for model_name,model_params in models_params.items():
        with open(fr"{main_dir}/{model_name}/run.json") as f:
            model_run_dicts[model_name] = json.load(f)
        tickers+=model_run_dicts[model_name]['tickers']
    tickers = list(set(tickers))


    data_table_name = gen_curr_table_name(curr_date)
    broker_runner = BrokerRunner()

    if curr_date.date() == datetime.now().date():
        for ticker in tickers:
            contract = BrokerRunner.get_contract(ticker)
            broker_runner.ib.reqHistoricalData(contract, '', "1 D", "1 min", "TRADES", useRTH=1, keepUpToDate=True)

    os.makedirs(os.path.dirname(position_handler_save_path),exist_ok=True)

    position_handler = PositionHandler(main_dir=main_dir,broker_runner=broker_runner,trail_stop_loss=trail_stop_loss,handle_positions_separately=handle_positions_separately)


    prediction_queue = {}
    mission_queue    = {}
    done_mission_queue = {}
    max_iminute = {}
    for model_name,model_params in models_params.items():
        prediction_queue[model_name] = Queue(maxsize=1)
        mission_queue[model_name]    = Queue(maxsize=1)
        done_mission_queue[model_name] = Queue(maxsize=1)
        checkpoint_path = rf"{main_dir}/{model_name}/checkpoints/{model_params['chosen_objective']}/checkpoint"
        prediction_dir  = f'{main_dir}/{model_name}/{curr_date.date().isoformat()}'
        module_run_process = Process(target=main_live_run,kwargs=
        {'prediction_queue':prediction_queue[model_name],'mission_queue':mission_queue[model_name],'done_mission_queue':done_mission_queue[model_name],
         'prediction_dir':prediction_dir,'checkpoint_path':checkpoint_path,'dct':model_run_dicts[model_name],'curr_date':curr_date,
         'chosen_objective':model_params['chosen_objective'],'amount_minute':model_params['amount_minute'],'min_invest':model_params['min_invest']
         ,'quantization':model_params['quantization']})
        module_run_process.start()
        max_iminute[model_name] = 0

    data_queue = Queue(maxsize=1)
    data_run_process = Process(target=main_table_create, kwargs={'data_table_name':data_table_name,'mission_queue':mission_queue,'done_mission_queue':done_mission_queue,'data_queue':data_queue})
    data_run_process.start()

    while (datetime.now().time().hour>=16) and (datetime.utcnow().time()<=close_time.time()):
        broker_runner.ib.sleep(0)
        periodic_data_bars = OrderedDict()
        if curr_date.date() == datetime.now().date():
            lengths = []
            for ticker,bars in zip(tickers,broker_runner.ib.realtimeBars()):
                periodic_data_bars[ticker] = [b for b in bars if b.date.date() == curr_date.date()]
                lengths.append(len(periodic_data_bars[ticker]))
        else:
            lengths = []
            for ticker in tickers:
                contract = BrokerRunner.get_contract(ticker)
                periodic_data_bars[ticker] = [b for b in broker_runner.ib.reqHistoricalData(contract, curr_date.date(), "1 D", "1 min","TRADES", useRTH=1) if b.date.date()==curr_date.date()]
                lengths.append(len(periodic_data_bars[ticker]))

        if all([len(pdb) == 0 for ticker,pdb in periodic_data_bars.items()]):
            broker_runner.ib.sleep(0.5)
            if random.random()<0.1:
                print('trade didnt start')
            continue
        broker_runner.ib.sleep(0)


        if (all([l==lengths[0] for l in lengths])):
            last_price = {}
            last_date = {}
            bars = []
            iminute = 0
            for ticker, pdb in periodic_data_bars.items():
                tmp = [p.__dict__ for p in pdb]
                [t.update({'ticker': ticker}) for t in tmp]
                max_tmp = max(tmp, key=lambda x: x['date'])
                last_price[ticker] = max_tmp['close']
                last_date[ticker] = max_tmp['date']
                bars += tmp
                iminute = max([iminute, len(tmp) - 1])

            assert all([v == last_date[ticker] for v in last_date.values()]), 'all tickers dates should be same'

            if iminute > max(max_iminute.values()):
                for model_name in mission_queue.keys():
                    max_iminute[model_name] = iminute
                while not data_queue.empty():
                    print(f'skipped: {data_queue.get()}')

                data_queue.put({'bars': bars, 'iminute': iminute})

        for model_name in prediction_queue:
            if prediction_queue[model_name].qsize()>0:
                prediction_result = prediction_queue[model_name].get()
                max_iminute[model_name] = prediction_result['max_iminute']

                print(prediction_result['max_iminute'],prediction_result)
                # if prediction_result['max_iminute']%trade_mod==7:
                position_handler.buy_prediction(prediction_result,model_name)

        position_handler.handle_position()
        try:
            position_handler.save(position_handler_save_path)
        except:
            print('skipping save of json ... ')

    position_handler.end_of_day_close()