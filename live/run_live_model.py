import time,json,numpy as np
from datetime import datetime,timedelta
from model_builder import live_run,run_prediction
from broker.broker_runner import BrokerRunner
from collections import OrderedDict
import pandas as pd,os
from data.stock_data import gen_curr_table_name
from google.cloud import storage
import random
import ib_insync
import pandas_market_calendars as mcal
from broker import config as broker_config
from multiprocessing import Process,Queue

def main_live_run(prediction_queue,mission_queue,data_table_name,prediction_dir,checkpoint_path,dct,curr_date,chosen_objective='default',amount_minute = 20000,min_invest=18000):
    while True:
        if mission_queue.qsize() == 0:
            time.sleep(0.1)
        mission = mission_queue.get()
        bars = mission['bars']
        iminute = mission['iminute']
        print(datetime.now(),iminute)
        bars_df = pd.DataFrame(bars)
        bars_df.to_gbq(data_table_name, if_exists='replace')
        os.makedirs(f'{prediction_dir}/{iminute}',exist_ok=True)
        model, invests = live_run(f'{prediction_dir}/{iminute}', dct['gui_dict'], dct['tickers'], checkpoint_path=checkpoint_path, curr_date=curr_date)
        last_date_invest = invests[chosen_objective]['invests'][-1]

        smoothed_prediction = last_date_invest[:, (iminute-10):iminute].sum(-1)/10

        invests_dollar = smoothed_prediction * amount_minute#last_date_invest[:, iminute - 1] * amount_minute # take the minute before last instead of current (current seems to be not full candle )
        rel_rows = np.array(model['data2net'].rows)[abs(invests_dollar) > min_invest].tolist()
        invests_dollar = invests_dollar[abs(invests_dollar) > min_invest].tolist()
        assert len(rel_rows) == len(invests_dollar), 'invests_dollar and rel_rows must have same size'
        prediction_queue.put({'rel_rows':rel_rows,'invests_dollar':invests_dollar,'max_iminute':iminute})

class PositionHandlerLogger():
    def __init__(self, path):
        self.path = path
        self.header = True
        os.makedirs(os.path.dirname(self.path),exist_ok=True)

    def print(self,action_str,ticker_handler):
        df = {}
        df['time'] = [datetime.now().isoformat()]
        df['action'] = [action_str]
        df['ticker'] = [ticker_handler.ticker]
        df['bid'] = [ticker_handler.tick_data['bid_ask'].bid]
        df['ask'] = [ticker_handler.tick_data['bid_ask'].ask]
        df['pips'] = [ticker_handler.pips]
        df['stop_price'] = [ticker_handler.tick_data['stop_price']]
        df['last'] = [ticker_handler.tick_data['last'].last]
        df['pos'] = [ticker_handler.tick_data['pos']]
        pd.DataFrame(df).to_csv(self.path, mode='a+', header=self.header)
        print(df)
        self.header = False

class TickerPositionHandler():
    def __init__(self,ticker):
        self.tick_data  = {'bid_ask':None,'last':None,'pos':0,'stop_price':None}
        self.buy_orders = []
        self.SL_orders  = []
        self.pips_ratio = None
        self.is_traced  = False
        self.ticker = ticker

    @property
    def spread(self):
        return (self.tick_data['bid_ask'].ask - self.tick_data['bid_ask'].bid)

    @property
    def pips(self):
        return np.round(self.pips_ratio * self.spread * 100) / 100

    def trace(self,broker_runner):
        contract = ib_insync.Stock(symbol=self.ticker, exchange='SMART', currency='USD')
        self.tick_data['bid_ask'] = broker_runner.ib.reqTickByTickData(contract=contract, tickType='BidAsk', numberOfTicks=1, ignoreSize=True)
        self.tick_data['last'] = broker_runner.ib.reqTickByTickData(contract=contract, tickType='Last', numberOfTicks=1, ignoreSize=True)

        for k_time in range(60):
            if np.isnan(self.tick_data['bid_ask'].ask):
                broker_runner.ib.sleep(1)
            else:
                break
        self.is_traced = True

class PositionHandler():
    def __init__(self,main_dir,broker_runner,tickers,SL_percent=0.2):
        self.SL_percent=SL_percent
        self.broker_runner  = broker_runner
        self.bought_tickers = []
        self.ticker_position_handler = {t:TickerPositionHandler(t) for t in tickers}
        ti = datetime.now().strftime("%Y%m%d-%H%M%S")
        self.log = PositionHandlerLogger(rf"{main_dir}/logger/{ti}.csv")

    def buy_prediction(self,prediction_result):
        SL_percent = self.SL_percent
        broker_runner = self.broker_runner
        broker_runner.ib.sleep(0)
        for (ticker, strategy, name, params), invests_dollar in zip(prediction_result['rel_rows'], prediction_result['invests_dollar']):
            if ticker in self.bought_tickers:
                print(f'already bought ticker {ticker} skipping')
                continue
            ticker_handler = self.ticker_position_handler[ticker]
            if not ticker_handler.is_traced:
                ticker_handler.trace(broker_runner)
            broker_runner.ib.sleep(0)
            for order in ticker_handler.buy_orders:
                broker_runner.ib.cancelOrder(order.order)
            # for order in ticker_handler.SL_orders:
            #     broker_runner.ib.cancelOrder(order.order)
            ticker_handler.buy_orders = []
            ticker_handler.SL_orders  = []

            ticker_handler.tick_data['stop_price'] = ticker_handler.tick_data['last'].last * (1 - (SL_percent / 100))
            contract = BrokerRunner.get_contract(ticker)
            full_quantity = np.round(invests_dollar / ticker_handler.tick_data['last'].last)
            ticker_handler.tick_data['pos'] = ticker_handler.tick_data['pos'] + full_quantity
            ticker_handler.pips_ratio = 0.1

            lmtPrice = min(ticker_handler.tick_data['bid_ask'].bid + ticker_handler.pips, ticker_handler.tick_data['last'].last)

            while full_quantity>0:
                quantity = min([99,full_quantity])
                full_quantity = full_quantity-quantity
                cur_order = ib_insync.LimitOrder(action='BUY', totalQuantity=quantity, lmtPrice=lmtPrice, orderId=broker_runner.ib.client.getReqId())
                cur_order.faGroup = broker_runner.fa_group
                cur_order.faMethod = broker_runner.faMethod
                ticker_handler.buy_orders.append(broker_runner.ib.placeOrder(contract, cur_order))
                broker_runner.ib.sleep(0)
            self.log.print('buy_prediction',ticker_handler)
            self.bought_tickers.append(ticker)
        broker_runner.ib.sleep(0)

    def update_prediction(self,prediction_result):
        SL_percent = self.SL_percent
        broker_runner = self.broker_runner
        broker_runner.ib.sleep(0)
        for (ticker, strategy, name, params), invests_dollar in zip(prediction_result['rel_rows'], prediction_result['invests_dollar']):
            ticker_handler = self.ticker_position_handler[ticker]
            if ticker_handler.tick_data['pos']==0:
                continue

            if not ticker_handler.is_traced:
                ticker_handler.trace(broker_runner)
            broker_runner.ib.sleep(0)

            ticker_handler.tick_data['stop_price'] = ticker_handler.tick_data['last'].last * (1 - (SL_percent / 100))

            # if ticker_handler.SL_orders is not None:
            #     broker_runner.ib.cancelOrder(ticker_handler.SL_orders.order)

            broker_runner.ib.sleep(0)
            self.log.print('update_prediction', ticker_handler)
        broker_runner.ib.sleep(0)

    def handle_position(self):
        SL_percent = self.SL_percent
        broker_runner = self.broker_runner
        for ticker,ticker_handler in self.ticker_position_handler.items():
            if ticker_handler.tick_data['pos'] != 0:
                for buy_order in ticker_handler.buy_orders:
                    if buy_order.orderStatus.status == 'Filled':
                        ticker_handler.buy_orders.remove(buy_order)

                    elif (abs(min(ticker_handler.tick_data['bid_ask'].bid + ticker_handler.pips, ticker_handler.tick_data['last'].last) - buy_order.order.lmtPrice) > 0.015):
                        try:
                            ticker_handler.pips_ratio+= 0.01
                            lmtPrice = min(ticker_handler.tick_data['bid_ask'].bid + ticker_handler.pips, ticker_handler.tick_data['last'].last)
                            buy_order.order.lmtPrice = lmtPrice
                            ticker_handler.buy_orders[ticker_handler.buy_orders.index(buy_order)] = broker_runner.ib.placeOrder(buy_order.contract, buy_order.order)
                            broker_runner.ib.sleep(0)
                            self.log.print('update_buy_prediction', ticker_handler)
                        except:
                            self.log.print('error_in_update_buy_prediction', ticker_handler)

                if (ticker_handler.tick_data['last'].last * (1 - (SL_percent / 100))) > ticker_handler.tick_data['stop_price']:
                    try:
                        ticker_handler.tick_data['stop_price'] = ticker_handler.tick_data['last'].last * (1 - (SL_percent / 100))
                        self.log.print('update_stop_price', ticker_handler)
                    except:
                        self.log.print('error_in_update_buy_prediction', ticker_handler)
                        broker_runner.ib.sleep(0)

                if ticker_handler.tick_data['last'].last <= ticker_handler.tick_data['stop_price']:
                    self.log.print('sell', ticker_handler)

                    contract = BrokerRunner.get_contract(ticker)
                    # position = []
                    # while len(position) == 0: #todo:chk if updates automaticly
                    #     broker_runner.ib.sleep(0)
                    #     position = [pos for pos in broker_runner.ib.positions(broker_config.account) if pos.contract.symbol == ticker]
                    # position[0].position
                    full_quantity = ticker_handler.tick_data['pos']

                    ticker_handler.pips_ratio = 0.1
                    lmtPrice = max(ticker_handler.tick_data['bid_ask'].ask - ticker_handler.pips, ticker_handler.tick_data['last'].last)
                    while full_quantity>0:
                        quantity = min([99, full_quantity])
                        full_quantity = full_quantity - quantity
                        cur_order = ib_insync.LimitOrder(action='SELL', totalQuantity=quantity,lmtPrice=lmtPrice,orderId=broker_runner.ib.client.getReqId())
                        cur_order.faGroup  = broker_runner.fa_group
                        cur_order.faMethod = broker_runner.faMethod
                        ticker_handler.SL_orders.append(broker_runner.ib.placeOrder(contract, cur_order))
                        broker_runner.ib.sleep(0)
                        ticker_handler.tick_data['stop_price'] = 0
                        ticker_handler.tick_data['pos'] = 0

            for sell_order in ticker_handler.SL_orders:
                if sell_order.orderStatus.status == 'Filled':
                    ticker_handler.SL_orders.remove(sell_order)


                elif (abs(max(ticker_handler.tick_data['bid_ask'].ask - ticker_handler.pips, ticker_handler.tick_data['last'].last) - sell_order.order.lmtPrice) > 0.015):
                    try:
                        ticker_handler.pips_ratio += 0.01
                        lmtPrice = max(ticker_handler.tick_data['bid_ask'].ask - ticker_handler.pips, ticker_handler.tick_data['last'].last)
                        sell_order.order.lmtPrice = lmtPrice
                        ticker_handler.SL_orders[ticker_handler.SL_orders.index(sell_order)] = broker_runner.ib.placeOrder(sell_order.contract, sell_order.order)
                        broker_runner.ib.sleep(0)
                        self.log.print('update_sell', ticker_handler)
                    except:
                        self.log.print('error_in_update_sell', ticker_handler)
                        broker_runner.ib.sleep(0)

    def end_of_day_close(self):
        broker_runner = self.broker_runner
        broker_runner.ib.sleep(60)

        for ticker,ticker_handler in self.ticker_position_handler.items():
            for order in ticker_handler.buy_orders:
                broker_runner.ib.cancelOrder(order.order)
            for order in ticker_handler.SL_orders:
                broker_runner.ib.cancelOrder(order.order)
            ticker_handler.SL_orders  = []
            ticker_handler.buy_orders = []

        for ticker,ticker_handler in self.ticker_position_handler.items():
            if ticker_handler.tick_data['pos'] != 0:
                self.log.print('sell_at_close', ticker_handler)

                contract = BrokerRunner.get_contract(ticker)
                full_quantity = ticker_handler.tick_data['pos']

                ticker_handler.pips_ratio = 0.1
                lmtPrice = max(ticker_handler.tick_data['bid_ask'].ask - ticker_handler.pips, ticker_handler.tick_data['last'].last)
                while full_quantity>0:
                    quantity = min([99, full_quantity])
                    full_quantity = full_quantity - quantity
                    cur_order = ib_insync.LimitOrder(action='SELL', totalQuantity=quantity,lmtPrice=lmtPrice,orderId=broker_runner.ib.client.getReqId())
                    cur_order.faGroup  = broker_runner.fa_group
                    cur_order.faMethod = broker_runner.faMethod
                    ticker_handler.SL_orders.append(broker_runner.ib.placeOrder(contract, cur_order))
                    broker_runner.ib.sleep(0)
                    ticker_handler.tick_data['stop_price'] = 0
                    ticker_handler.tick_data['pos'] = 0

        while any([ticker_handler.SL_orders!=[] for ticker, ticker_handler in self.ticker_position_handler.items()]):
            for ticker, ticker_handler in self.ticker_position_handler.items():
                for sell_order in ticker_handler.SL_orders:
                    if sell_order.orderStatus.status == 'Filled':
                        ticker_handler.SL_orders.remove(sell_order)

                    elif (abs(max(ticker_handler.tick_data['bid_ask'].ask - ticker_handler.pips, ticker_handler.tick_data['last'].last) - sell_order.order.lmtPrice) > 0.015):
                        try:
                            ticker_handler.pips_ratio += 0.01
                            lmtPrice = max(ticker_handler.tick_data['bid_ask'].ask - ticker_handler.pips, ticker_handler.tick_data['last'].last)
                            sell_order.order.lmtPrice = lmtPrice
                            ticker_handler.SL_orders[ticker_handler.SL_orders.index(sell_order)] = broker_runner.ib.placeOrder(sell_order.contract, sell_order.order)
                            broker_runner.ib.sleep(0)
                            self.log.print('update_sell_at_close', ticker_handler)
                        except:
                            self.log.print('error_in_update_sell_at_close', ticker_handler)
                            broker_runner.ib.sleep(0)


def handle_simple_prediction(prediction_result,broker_runner,amount_minute=5000):
    iminute = prediction_result['max_iminute']
    last_price = {}
    trailing_sl = 0.002
    open_orders_ids = [order.orderId for order in broker_runner.ib.reqAllOpenOrders()]
    all_pos=broker_runner.ib.positions(broker_config.account)
    cur_pos={p.contract.symbol:p.position for p in all_pos}
    for (ticker, strategy, name, params), _ in zip(prediction_result['rel_rows'], prediction_result['invests_dollar']):
        rel_trades_to_update = [t for t in broker_runner.ib.trades() if t.order.orderType == 'TRAIL' and t.contract.symbol == ticker and t.order.orderId in open_orders_ids]
        orders = [t.order for t in rel_trades_to_update]
        contracts = [t.contract for t in rel_trades_to_update]

        if contracts!=[] and ticker in cur_pos.keys():
            mkt = broker_runner.ib.reqMktData(contract=BrokerRunner.get_contract(ticker), mktDataOptions=233)
            list(broker_runner.ib.loopUntil(lambda: np.isnan(mkt.marketPrice()) == False))
            last_price[ticker] = mkt.marketPrice()

            for trail in orders:
                parentId = trail.parentId
                broker_runner.ib.cancelOrder(trail)

            new_trail = ib_insync.MarketOrder(action=trail.action, totalQuantity=trail.totalQuantity)
            new_trail.orderId = broker_runner.ib.client.getReqId()
            new_trail.orderType = trail.orderType
            new_trail.totalQuantity =cur_pos[ticker]
            # new_trail.totalQuantity = sum([trail.totalQuantity for trail in orders])

            new_trail.trailingPercent = trailing_sl*100
            new_trail.trailStopPrice = np.round_(last_price[ticker] * (1 - trailing_sl), 2)
            new_trail.transmit = True
            trade = broker_runner.place_orders(contracts,[new_trail])

            broker_runner.ib.cancelMktData(contract=BrokerRunner.get_contract(ticker))
            broker_runner.ib.sleep(0.1)


    if ((iminute%10)==0) and (amount_minute>3333.33):
        contracts = []
        orders = []
        for (ticker, strategy, name, params), invests_dollar in zip(prediction_result['rel_rows'], prediction_result['invests_dollar']):
            mkt = broker_runner.ib.reqMktData(contract=BrokerRunner.get_contract(ticker), mktDataOptions=233)
            list(broker_runner.ib.loopUntil(lambda: np.isnan(mkt.marketPrice()) == False))
            last_price[ticker] = mkt.marketPrice()

            broker_runner.ib.cancelMktData(contract=BrokerRunner.get_contract(ticker))
            broker_runner.ib.sleep(0.1)

            print(iminute,ticker,invests_dollar,name,iminute,last_price[ticker])
            curr_orders = strategy.generate_orders(broker_runner=broker_runner,ticker=ticker, ticker_price=last_price[ticker], amount=invests_dollar, params=params)
            contracts+=[broker_runner.get_contract(ticker)]*len(curr_orders)
            orders+=curr_orders

        broker_runner.place_orders(contracts = contracts,orders=orders)
        print(time.time() - start)
    else:
        print('not this time bako !!!')
        for (ticker, strategy, name, params), invests_dollar in zip(prediction_result['rel_rows'], prediction_result['invests_dollar']):
            print(iminute,ticker,invests_dollar,name,iminute)

        try:
            order_keys = ['orderId', 'permId', 'action', 'totalQuantity', 'orderType', 'lmtPrice', 'auxPrice', 'tif', 'percentOffset', 'trailStopPrice', 'trailingPercent', 'faGroup']
            exc = pd.DataFrame([e.__dict__ for e in broker_runner.ib.executions()])
            exc['chosen_objective'] = chosen_objective
            exc['model_name'] = prefix
            exc.to_csv(rf'{prediction_dir}/{iminute}/exc.csv')#to_gbq('algo_dataset.intraday_executions', if_exists='append')

            trades = broker_runner.ib.trades()
            orders = pd.DataFrame([e.order.__dict__ for e in trades])[order_keys]
            orders['status'] = [e.orderStatus.status for e in trades]
            orders['ticker'] = [e.contract.symbol for e in trades]

            orders['chosen_objective'] = chosen_objective
            orders['model_name'] = prefix
            orders.to_csv(rf'{prediction_dir}/{iminute}/orders.csv')#.to_gbq('algo_dataset.intraday_orders', if_exists='append')

            pd.merge(exc, orders, on=['permId']).to_csv(rf'{prediction_dir}/{iminute}/merged_orders_exc.csv')

        except:
            pass


if __name__=='__main__':

    close_time = mcal.get_calendar('NYSE').schedule(datetime.now(), datetime.now(), 'UTC').market_close[0] - timedelta(minutes=15)

    main_dir = r'D:/test_live_int'

    chosen_objective = 'default'
    prefix = 'small_aum_simple_model'
    amount_minute = 20000
    min_invest = 18000

    curr_date = datetime.now()
    download = False
    check_full_prediction = False
    start = time.time()
    checkpoint_path = rf"{main_dir}/checkpoints/{chosen_objective}/checkpoint"
    prediction_dir = f'{main_dir}/{curr_date.date().isoformat()}'

    if download:
        for b in storage.client.Client().get_bucket('free_market_intraday').list_blobs(prefix=prefix + '/'):
            filepath = fr'{main_dir}/{b.name.lstrip(prefix)}'
            os.makedirs(os.path.dirname(filepath), exist_ok=True)
            print(filepath)
            b.download_to_filename(filepath)

    with open(fr"{main_dir}/run.json") as f:
        dct = json.load(f)

    if check_full_prediction:
        model, invests = run_prediction(main_dir, dct['gui_dict'], dct['tickers'], checkpoint_path=checkpoint_path)

    if False:
        model, invests = live_run(f'{prediction_dir}/390', dct['gui_dict'], dct['tickers'], checkpoint_path=checkpoint_path, curr_date=datetime(2021, 8, 6))
        exit(0)

    data_table_name = gen_curr_table_name(curr_date)
    broker_runner = BrokerRunner()
    tmp = broker_runner.ib.reqMktData(ib_insync.Forex('USDILS'))
    list(broker_runner.ib.loopUntil(lambda :np.isnan(tmp.marketPrice())==False))
    ils2dollar =  tmp.marketPrice()

    if curr_date.date() == datetime.now().date():
        for ticker in dct['tickers']:
            contract = BrokerRunner.get_contract(ticker)
            broker_runner.ib.reqHistoricalData(contract, '', "1 D", "1 min", "TRADES", useRTH=1, keepUpToDate=True)

    prediction_queue = Queue(maxsize=1)
    mission_queue = Queue(maxsize=1)
    position_handler = PositionHandler(main_dir=main_dir,broker_runner=broker_runner,tickers=dct['tickers'],SL_percent=0.2)
    module_run_process = Process(target=main_live_run,kwargs=
    {'prediction_queue':prediction_queue,'mission_queue':mission_queue,'data_table_name':data_table_name,'prediction_dir':prediction_dir,
     'checkpoint_path':checkpoint_path,'dct':dct,'curr_date':curr_date,'chosen_objective':chosen_objective,'amount_minute':amount_minute,'min_invest':min_invest})

    module_run_process.start()

    max_iminute = 0
    while (datetime.now().time().hour>=16) and (datetime.utcnow().time()<=close_time.time()):
        broker_runner.ib.sleep(0)
        periodic_data_bars = OrderedDict()
        if curr_date.date() == datetime.now().date():
            lengths = []
            for ticker,bars in zip(dct['tickers'],broker_runner.ib.realtimeBars()):
                periodic_data_bars[ticker] = [b for b in bars if b.date.date() == curr_date.date()]
                lengths.append(len(periodic_data_bars[ticker]))
        else:
            lengths = []
            for ticker in dct['tickers']:
                contract = BrokerRunner.get_contract(ticker)
                periodic_data_bars[ticker] = [b for b in broker_runner.ib.reqHistoricalData(contract, curr_date.date(), "1 D", "1 min","TRADES", useRTH=1) if b.date.date()==curr_date.date()]
                lengths.append(len(periodic_data_bars[ticker]))

        if all([len(pdb) == 0 for ticker,pdb in periodic_data_bars.items()]):
            broker_runner.ib.sleep(0.5)
            if random.random()<0.1:
                print('trade didnt start')
            continue
        broker_runner.ib.sleep(0)


        if (all([l==lengths[0] for l in lengths])):
            last_price = {}
            last_date = {}
            bars = []
            iminute = 0
            for ticker, pdb in periodic_data_bars.items():
                tmp = [p.__dict__ for p in pdb]
                [t.update({'ticker': ticker}) for t in tmp]
                max_tmp = max(tmp, key=lambda x: x['date'])
                last_price[ticker] = max_tmp['close']
                last_date[ticker] = max_tmp['date']
                bars += tmp
                iminute = max([iminute, len(tmp) - 1])

            assert all([v == last_date[ticker] for v in last_date.values()]), 'all tickers dates should be same'

            if iminute > max_iminute:
                max_iminute = iminute
                mission_queue.put({'bars': bars, 'iminute': iminute})

        if prediction_queue.qsize()>0:
            prediction_result = prediction_queue.get()
            max_iminute = prediction_result['max_iminute']
            # send_result = Thread(target=handle_simple_prediction,kwargs={'prediction_result':prediction_result,'broker_runner':broker_runner})
            # send_result.start()
            print(prediction_result['max_iminute'],prediction_result)

            position_handler.buy_prediction(prediction_result)
            # if max_iminute%10==0:
            #     position_handler.buy_prediction(prediction_result)
            # else:
            #     position_handler.update_prediction(prediction_result)

        position_handler.handle_position()

    position_handler.end_of_day_close()