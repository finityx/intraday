import time,json,numpy as np
from datetime import datetime,timedelta
from broker.broker_runner import BrokerRunner
import ib_insync

main_dir = r'D:/test_live_galileo'


curr_date = datetime.now()

start = time.time()

if (curr_date.hour + curr_date.minute/60)<=6.5:
    exit(0)

broker_runner = BrokerRunner()

pair = ['TQQQ','TECL']
prices = []

invest_aum = 1500
protfolio = {}
contract = BrokerRunner.get_contract(pair[0])
bars_long = broker_runner.ib.reqHistoricalData(contract, curr_date.date(), "1 D", "5 secs", "TRADES", useRTH=1, timeout=300)
bars_long = sorted([b for b in bars_long if (b.date.hour+b.date.minute/60)<18.5 and (b.date.hour+b.date.minute/60)>17.5],key=lambda x:x.date)
bars_long = np.array([b.open for b in bars_long])

protfolio['long'] = {'contract':contract,'start_price':bars_long[-1]}

contract = BrokerRunner.get_contract(pair[1])
bars_short  = broker_runner.ib.reqHistoricalData(contract, curr_date.date(), "1 D", "5 secs", "TRADES", useRTH=1, timeout=300)
bars_short = sorted([b for b in bars_short if (b.date.hour+b.date.minute/60)<18.5 and (b.date.hour+b.date.minute/60)>17.5],key=lambda x:x.date)
bars_short = np.array([b.open for b in bars_short])

protfolio['short'] = {'contract':contract,'start_price':bars_short[-1]}

bars = (bars_long/bars_long[0]-1) - (bars_short/bars_short[0]-1)
protfolio['std']   = bars[0::12].std()*100*((9.5)**0.5)
protfolio['curr_invest']   = 0




while True:
    long_price  = broker_runner.ib.reqMktData(protfolio['long']['contract'], 233)
    short_price = broker_runner.ib.reqMktData(protfolio['short']['contract'], 233)
    broker_runner.ib.sleep(1)
    long_price = broker_runner.ib.reqMktData(protfolio['long']['contract'], 233).last
    short_price = broker_runner.ib.reqMktData(protfolio['short']['contract'], 233).last

    long_percent  = 100 * (long_price / protfolio['long']['start_price'] - 1)
    short_percent = 100 * (short_price / protfolio['short']['start_price'] - 1)
    extra_invest = long_percent-short_percent - protfolio['curr_invest']
    extra_invest = np.sign(extra_invest)*np.floor(abs(extra_invest)/protfolio['std'])
    print(long_price,short_price,long_percent,short_percent,extra_invest)

    if abs(extra_invest)>0.1:
        protfolio['curr_invest'] = protfolio['curr_invest'] + extra_invest*protfolio['std']
        trades = []
        contracts = []
        orders    = []
        if protfolio['curr_invest']>0:
            contracts.append(protfolio['long']['contract'])
            contracts.append(protfolio['short']['contract'])

            orders.append(ib_insync.MarketOrder('BUY' , totalQuantity=abs(np.round_(invest_aum*extra_invest/long_price))))
            orders.append(ib_insync.MarketOrder('SELL', totalQuantity=abs(np.round_(invest_aum*extra_invest/short_price))))

        else:
            contracts.append(protfolio['long']['contract'])
            contracts.append(protfolio['short']['contract'])

            orders.append(ib_insync.MarketOrder('SELL', totalQuantity=abs(np.round_(invest_aum * extra_invest / long_price))))
            orders.append(ib_insync.MarketOrder('BUY', totalQuantity=abs(np.round_(invest_aum * extra_invest / short_price))))

        broker_runner.place_orders(contracts,orders)