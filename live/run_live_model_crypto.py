import time,json,numpy as np
from datetime import datetime,timedelta
from model_builder import live_run,run_prediction
from crypto_broker.broker_runner import BrokerRunner
from crypto_broker import config as broker_config

from collections import OrderedDict
import pandas as pd,os
from data.crypto_data import  gen_curr_table_name
from google.cloud import storage
import random
import ib_insync
import pandas_market_calendars as mcal
from threading import Thread
from queue import Queue
from binance import Client, DepthCacheManager, BinanceSocketManager

def download_data(tickers,data_table_name,iminute):
    client = Client()
    curr_date.strftime('%Y-%M-%d')
    dfs = []
    iminutes = []
    for ticker in tickers:
        print(ticker)
        klines = np.array(client.get_historical_klines(ticker.replace('-',''), client.KLINE_INTERVAL_1MINUTE, curr_date.strftime('%d %b,%Y')))
        df = pd.DataFrame(klines.reshape(-1, 12), dtype=float, columns=('open_time','open','high','low','close','volume','close_time','quote_asset_volume','number_of_trades','taker_buy_base_asset_volume','taker_buy_quote_asset_volume','ignore'))
        df['open_time'] = [pd.Timestamp(ot, unit='ms') for ot in df['open_time']]
        df['close_time'] = [pd.Timestamp(ot, unit='ms') for ot in df['close_time']]
        iminutes.append(len(df.open_time))
        dfs.append(df)
    df = pd.concat(dfs)

    if all([at == iminutes[0] for at in iminutes]) and ((prev_time is None) or (all([at>iminute for at in iminutes]))):
        df.to_gbq(data_table_name, if_exists='replace')
        return True,iminutes[0]
    else:
        return False,iminute

def main_live_run(iminute,amount_minute=100):
    print(iminute,datetime.now())

    os.makedirs(f'{prediction_dir}/{iminute}',exist_ok=True)
    model, invests = live_run(f'{prediction_dir}/{iminute}', dct['gui_dict'], dct['tickers'], checkpoint_path=checkpoint_path, curr_date=curr_date)

    last_date_invest = invests[chosen_objective]['invests'][-1]
    invests_dollar = last_date_invest[:, iminute - 1] * amount_minute # take the minute before last instead of current (current seems to be not full candle )

    rel_rows = np.array(model['data2net'].rows)[abs(invests_dollar) > min_invest].tolist()
    invests_dollar = invests_dollar[abs(invests_dollar) > min_invest].tolist()
    assert len(rel_rows) == len(invests_dollar), 'invests_dollar and rel_rows must have same size'
    prediction_queue.put({'rel_rows':rel_rows,'invests_dollar':invests_dollar,'max_iminute':max_iminute})

class PositionHandlerLogger():
    def __init__(self, path):
        self.path = path
        self.header = True

    def print(self,action_str,ticker_handler):
        df = {}
        df['time'] = [datetime.now().isoformat()]
        df['action'] = [action_str]
        df['ticker'] = [ticker_handler.ticker]
        df['bid'] = [ticker_handler.tick_data['bid_ask'].bid]
        df['ask'] = [ticker_handler.tick_data['bid_ask'].ask]
        df['pips'] = [ticker_handler.pips]
        df['stop_price'] = [ticker_handler.stop_price]
        df['last'] = [ticker_handler.tick_data['last'].last]
        pd.DataFrame(df).to_csv(self.path, mode='a+', header=self.header)
        self.header = False

class TickerPositionHandler():
    def __init__(self,ticker):
        self.tick_data  = {'bid_ask':None,'last':None,'pos':None,'stop_price':None}
        self.buy_orders = []
        self.SL_orders  = []
        self.pips_ratio = None
        self.is_traced  = False
        self.ticker = ticker

    @property
    def spread(self):
        return (self.tick_data['bid_ask'].ask - self.tick_data['bid_ask'].bid)

    @property
    def pips(self):
        return np.round(self.pips_ratio * self.spread * 100) / 100

    def trace(self,broker_runner):
        contract = ib_insync.Stock(symbol=self.ticker, exchange='SMART', currency='USD')
        self.tick_data['bid_ask'] = broker_runner.ib.reqTickByTickData(contract=contract, tickType='BidAsk', numberOfTicks=1, ignoreSize=True)
        self.tick_data['last'] = broker_runner.ib.reqTickByTickData(contract=contract, tickType='Last', numberOfTicks=1, ignoreSize=True)

        for k_time in range(60):
            if np.isnan(self.tick_data['bid_ask'].ask):
                broker_runner.ib.sleep(1)
            else:
                break
        self.is_traced = True

class PositionHandler():
    def __init__(self,main_dir,broker_runner,tickers,SL_percent=0.2):
        self.SL_percent=SL_percent
        self.broker_runner = broker_runner
        self.ticker_position_handler = {t:TickerPositionHandler(t) for t in tickers}
        ti = datetime.now().strftime("%Y%m%d-%H%M%S")
        self.log = PositionHandlerLogger(rf"{main_dir}/logger/{ti}.txt")

    def buy_prediction(self,prediction_result):
        SL_percent = self.SL_percent
        broker_runner = self.broker_runner
        broker_runner.ib.sleep(0)
        for (ticker, strategy, name, params), invests_dollar in zip(prediction_result['rel_rows'], prediction_result['invests_dollar']):
            ticker_handler = self.ticker_position_handler[ticker]
            if not ticker_handler.is_traced:
                ticker_handler.trace(broker_runner)
            broker_runner.ib.sleep(0)
            for order in ticker_handler.buy_orders:
                broker_runner.ib.cancelOrder(order.order)
            for order in ticker_handler.SL_orders:
                broker_runner.ib.cancelOrder(order.order)
            ticker_handler.buy_orders = []
            ticker_handler.SL_orders  = []

            ticker_handler.tick_data['stop_price'] = ticker_handler.tick_data['last'].last * (1 - (SL_percent / 100))
            contract = BrokerRunner.get_contract(ticker)
            full_quantity = np.round(invests_dollar / ticker_handler.tick_data['last'].last)
            ticker_handler.tick_data['pos'] = ticker_handler.tick_data['pos'] + full_quantity
            ticker_handler.pips_ratio = 0.1
            # ticker_handler.pips = np.round(ticker_handler.pips_ratio * ticker_handler.spread * 100) / 100

            lmtPrice = min(ticker_handler.tick_data['bid_ask'].bid + ticker_handler.pips, ticker_handler.tick_data['last'].last)

            while full_quantity>0:
                quantity = min([99,full_quantity])
                full_quantity = full_quantity-quantity
                cur_order = ib_insync.LimitOrder(action='BUY', totalQuantity=quantity, lmtPrice=lmtPrice, orderId=broker_runner.ib.client.getReqId())
                cur_order.faGroup = broker_runner.fa_group
                cur_order.faMethod = broker_runner.faMethod
                ticker_handler.buy_orders.append(broker_runner.ib.placeOrder(contract, cur_order))
                broker_runner.ib.sleep(0)
            self.log.print('buy_prediction',ticker_handler)
        broker_runner.ib.sleep(0)

    def handle_position(self):
        SL_percent = self.SL_percent
        broker_runner = self.broker_runner
        for ticker,ticker_handler in self.ticker_position_handler.items():
            if ticker_handler.tick_data['pos'] != 0:
                # ticker_handler.pips = np.round(ticker_handler.pips_ratio * ticker_handler.spread * 100) / 100

                for buy_order in ticker_handler.buy_orders:
                    if buy_order.orderStatus.status == 'Filled':
                        ticker_handler.buy_orders.remove(buy_order)

                    elif (abs(min(ticker_handler.tick_data['bid_ask'].bid + ticker_handler.pips, ticker_handler.tick_data['last'].last) - ticker_handler.buy_orders.order.lmtPrice) > 0.015):
                        ticker_handler.pips_ratio+= 0.01
                        # ticker_handler.pips = np.round(ticker_handler.pips_ratio * ticker_handler.spread * 100) / 100
                        lmtPrice = min(ticker_handler.tick_data['bid_ask'].bid + ticker_handler.pips, ticker_handler.tick_data['last'].last)
                        buy_order.order.lmtPrice = lmtPrice
                        ticker_handler.buy_orders[ticker_handler.buy_orders.index(buy_order)] = broker_runner.ib.placeOrder(buy_order.contract, buy_order.order)
                        broker_runner.ib.sleep(0)
                        self.log.print('update_buy_prediction', ticker_handler)

                if (ticker_handler.tick_data['last'].last * (1 - (SL_percent / 100))) > ticker_handler.tick_data['stop_price']:
                    ticker_handler.tick_data['stop_price'] = ticker_handler.tick_data['last'].last * (1 - (SL_percent / 100))
                    self.log.print('update_stop_price', ticker_handler)

                if ticker_handler.tick_data['last'].last <= ticker_handler.tick_data['stop_price']:
                    self.log.print('sell', ticker_handler)

                    contract = BrokerRunner.get_contract(ticker)
                    # position = []
                    # while len(position) == 0: #todo:chk if updates automaticly
                    #     broker_runner.ib.sleep(0)
                    #     position = [pos for pos in broker_runner.ib.positions(broker_config.account) if pos.contract.symbol == ticker]
                    # position[0].position
                    full_quantity = ticker_handler.tick_data['pos']

                    ticker_handler.pips_ratio = 0.1
                    # ticker_handler.pips = np.round(ticker_handler.pips_ratio * ticker_handler.spread * 100) / 100
                    lmtPrice = max(ticker_handler.tick_data['bid_ask'].ask - ticker_handler.pips, ticker_handler.tick_data['last'].last)
                    while full_quantity>0:
                        quantity = min([99, full_quantity])
                        full_quantity = full_quantity - quantity
                        cur_order = ib_insync.LimitOrder(action='SELL', totalQuantity=quantity,lmtPrice=lmtPrice,orderId=broker_runner.ib.client.getReqId())
                        cur_order.faGroup  = broker_runner.fa_group
                        cur_order.faMethod = broker_runner.faMethod
                        ticker_handler.SL_orders.append(broker_runner.ib.placeOrder(contract, cur_order))
                        broker_runner.ib.sleep(0)
                        ticker_handler.tick_data['stop_price'] = 0
                        ticker_handler.tick_data['pos'] = 0

            if ticker_handler.SL_orders is not None:
                if ticker_handler.SL_orders.orderStatus.status == 'Filled':
                    ticker_handler.SL_orders = None

                elif (abs(max(ticker_handler.tick_data['bid_ask'].ask - ticker_handler.pips, ticker_handler.tick_data['last'].last) - ticker_handler.SL_orders.order.lmtPrice) > 0.015):
                    ticker_handler.pips_ratio += 0.01
                    # ticker_handler.pips = np.round(ticker_handler.pips_ratio * ticker_handler.spread * 100) / 100
                    lmtPrice = max(ticker_handler.tick_data['bid_ask'].ask - ticker_handler.pips, ticker_handler.tick_data['last'].last)
                    ticker_handler.SL_orders.order.lmtPrice = lmtPrice
                    ticker_handler.SL_orders = broker_runner.ib.placeOrder(ticker_handler.SL_orders.contract, ticker_handler.SL_orders.order)
                    broker_runner.ib.sleep(0)
                    self.log.print('update_sell', ticker_handler)


main_dir = r'D:/test_crypto_live_int'

chosen_objective = 'default'
prefix = 'crypto_test'

min_invest = 100
liq_req_factor = 1
curr_invests = []
curr_date = datetime.now()
download = True
check_full_prediction = False

start = time.time()
checkpoint_path = rf"{main_dir}/checkpoints/{chosen_objective}/checkpoint"

prediction_dir = f'{main_dir}/{curr_date.date().isoformat()}'

if download:
    for b in storage.client.Client().get_bucket('free_market_intraday').list_blobs(prefix=prefix + '/'):
        filepath = fr'{main_dir}/{b.name.lstrip(prefix)}'
        os.makedirs(os.path.dirname(filepath),exist_ok=True)
        print(filepath)
        b.download_to_filename(filepath)

with open(fr"{main_dir}/run.json") as f:
    dct = json.load(f)

if check_full_prediction:
    model, invests = run_prediction(main_dir, dct['gui_dict'], dct['tickers'], checkpoint_path=checkpoint_path)

if False:
    model, invests = live_run(f'{main_dir}', dct['gui_dict'], dct['tickers'], checkpoint_path=checkpoint_path, curr_date=datetime(2021,8,6))
    exit(0)

data_table_name = gen_curr_table_name(curr_date)
prev_time = None

broker_runner = BrokerRunner()
prediction_queue = Queue(maxsize=1)

modl_run_thread = None
max_iminute = 0
position_handler = PositionHandler(main_dir=main_dir,broker_runner=broker_runner,tickers=dct['tickers'],SL_percent=0.2)

while True:
    periodic_data_bars = OrderedDict()
    is_down,prev_time = download_data(dct['tickers'],data_table_name,prev_time)

    if (modl_run_thread is not None) and (modl_run_thread.is_alive()==False):
        modl_run_thread = None

    if (modl_run_thread is None) and (all([l==lengths[0] for l in lengths])):
        modl_run_thread = Thread(target=main_live_run,kwargs={'prev_time':prev_time})
        modl_run_thread.start()

    prediction_result = None
    if prediction_queue.qsize()>0:
        prediction_result = prediction_queue.get()
        max_iminute = prediction_result['max_iminute']

    if prediction_result is not None:
        position_handler.buy_prediction(prediction_result)

    position_handler.handle_position()


