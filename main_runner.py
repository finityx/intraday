import os
os.environ['CUDA_VISIBLE_DEVICES'] = '-1'
from datetime import datetime,timedelta
import scipy.io,git
import tensorflow as tf
import numpy as np
import shutil,glob,time
from google.cloud import storage


def upload_local_directory_to_gcs(local_path, bucket, gcs_path):
    assert os.path.isdir(local_path)
    for local_file in glob.glob(local_path + '/**'):
        if not os.path.isfile(local_file):
            upload_local_directory_to_gcs(local_file, bucket, gcs_path + "/" + os.path.basename(local_file))
        else:
            remote_path = '/'.join([gcs_path, local_file[1 + len(local_path):]])
            remote_path = remote_path.lstrip('/')
            blob = bucket.blob(remote_path)
            blob.upload_from_filename(local_file)

class Runner():
    def __init__(self,net,name='default'):
        self.net  = net
        self.name = name

    def build_model(self):
        self.model    = self.net.build()


    def get_dataset_by_dates(self,ds_start_date,ds_end_date=None,to_shuffle=False,drop_remainder=False):
        data2net = self.net.data2net
        data_feature_description  =  {source.name: tf.io.FixedLenFeature(len(source.params) * len(data2net.tickers)*data2net.ndays*data2net.n_minutes_in_day, tf.float32) for source in data2net.get_datasources() if source in data2net.data_sources}
        label_feature_description =  {source.name: tf.io.FixedLenFeature(len(source.params) * len(data2net.tickers)*len(data2net.strategies_params)*data2net.n_minutes_in_day, tf.float32) for source in data2net.get_datasources() if source not in data2net.data_sources}

        feature_description  = {**data_feature_description, **label_feature_description}
        record_dir = f'{data2net.main_path}/transformed_data/tf_record'

        def reshape_dict(example):
            sources = data2net.data_sources
            data  = {source.name: tf.reshape(example[source.name], [len(data2net.tickers),data2net.ndays,data2net.n_minutes_in_day, len(source.params)]) for source in sources}
            data  = {source.name: tf.where(tf.math.is_nan(data[source.name]), tf.zeros_like(data[source.name]), data[source.name]) for source in sources}
            data  = {source.name: tf.where(tf.math.is_inf(data[source.name]), tf.zeros_like(data[source.name]), data[source.name]) for source in sources}


            labels = {}
            for obj in data2net.objectives:
                closs = [tf.reshape(example[ods.name], [len(data2net.tickers),len(data2net.strategies_params),data2net.n_minutes_in_day, len(ods.params)]) for ods in obj.get_data_sources()]
                labels[obj.name] = tf.concat(closs,-1)

            labels  = {obj.name: tf.where(tf.math.is_nan(labels[obj.name]), tf.zeros_like(labels[obj.name]), labels[obj.name]) for obj in data2net.objectives}
            labels  = {obj.name: tf.where(tf.math.is_inf(labels[obj.name]), tf.zeros_like(labels[obj.name]), labels[obj.name]) for obj in data2net.objectives}

            return data,labels

        def add_random_tickers(data,labels):
            ds_dict = {ds.name: ds for ds in data2net.data_sources}
            stock_data = {k: data['stock_data'][:, :, :, ik:(ik + 1)] for ik, k in enumerate(ds_dict['stock_data'].params)}
            sign = 2*tf.cast(tf.random.uniform(tf.shape(stock_data['open']))>0.5,'float32')-1
            prev_sign = 2 * tf.cast(tf.random.uniform(tf.shape(stock_data['open'][:, :, 0:1, :])) > 0.5, 'float32') - 1

            stock_data['open'] = stock_data['open'] * sign
            stock_data['close'] = stock_data['close'] * sign
            high = stock_data['high'] * sign
            low  = stock_data['low'] * sign
            stock_data['high'] = tf.where(sign > 0, high, low)
            stock_data['low']  = tf.where(sign < 0, high, low)
            stock_data['close2open_yesterday']  = stock_data['close2open_yesterday']*prev_sign

            for k in data:
                if k == 'stock_data':
                    tmp = tf.concat([stock_data[k] for k in ds_dict['stock_data'].params],-1)
                    data[k] = tf.concat([data[k], tmp], 0)
                else:
                    data[k] = tf.concat([data[k], tf.zeros_like(data[k])], 0)
            for obj in data2net.objectives:
                labels[obj.name] += tf.reshape(tf.constant([390 if lp =='duration' else 0.0 for lp in obj.label.params],'float32'),[1,1,1,-1])
                labels[obj.name] = tf.concat([labels[obj.name], tf.zeros_like(labels[obj.name])], 0)
            return data,labels

        filelist = [f'{record_dir}/{f}' for f in sorted(os.listdir(record_dir))]

        dates    = [f.split('.')[0] for f in sorted(os.listdir(record_dir))]
        if ds_end_date is None:
            II = [II for II,f in enumerate(filelist) if (datetime.fromisoformat(os.path.basename(f).split('.')[0])>=ds_start_date)]

        else:
            II = [II for II,f in enumerate(filelist) if (datetime.fromisoformat(os.path.basename(f).split('.')[0])<=ds_end_date) and (datetime.fromisoformat(os.path.basename(f).split('.')[0])>=ds_start_date)]

        II = list(set(II + list(range(max(II),int(np.ceil((max(II)+self.net.batch_size)/self.net.batch_size)*(self.net.batch_size))))))
        II = [i for i in II if i<len(filelist)]
        II = list(sorted(II))

        filelist = np.array(filelist)[II].tolist()
        dates = np.array(dates)[II].tolist()

        filelist = tf.data.Dataset.list_files(filelist, shuffle=to_shuffle)
        reader = filelist.interleave(tf.data.TFRecordDataset,cycle_length=self.net.batch_size*2)
        reader = reader.map(lambda example_proto: tf.io.parse_single_example(example_proto, feature_description))
        dataset = reader.map(reshape_dict)
        dataset = dataset.map(add_random_tickers)

        dataset = dataset.batch(self.net.batch_size, drop_remainder=drop_remainder)

        return {'dataset':dataset,'dates':dates}

    def create_datasets(self,start_date,end_date_train,end_date_val,shuffle):
        train_dataset_dates = self.get_dataset_by_dates(start_date , end_date_train,shuffle)
        sorted_train_dataset_dates = self.get_dataset_by_dates(start_date , end_date_train)
        test_dataset_dates = self.get_dataset_by_dates(datetime.fromisoformat(train_dataset_dates['dates'][-1])+timedelta(days=1), end_date_val)
        val_dataset_dates = self.get_dataset_by_dates(datetime.fromisoformat(test_dataset_dates['dates'][-1])+timedelta(days=1),drop_remainder=True)
        return train_dataset_dates,sorted_train_dataset_dates,test_dataset_dates,val_dataset_dates


    # @tf.function
    def train_step(self,train_dataset):
        self.model.reset_states()
        losses  = 0.0
        counter = 0.0
        for step, (x_batch_train, y_batch_train) in enumerate(train_dataset):
            with tf.GradientTape() as tape:
                ypred = self.model(x_batch_train, training=True)
                loss_value = 0.0
                for obj in self.net.data2net.objectives:
                    loss_value += obj.loss.calc_loss[self.net.name](ypred,y_batch_train[obj.name])

            grads = tape.gradient(loss_value, self.model.trainable_weights)
            self.net.optimizer.apply_gradients(zip(grads, self.model.trainable_weights))
            losses +=loss_value
            counter+=1.0
        return float(losses/counter)


    def train(self,save_dir,start_date,end_date_train,end_date_val,shuffle,max_epochs=500,n_tosave=20,bucket_name=None,prefix='',obj_checkpoint=None):
        os.makedirs(save_dir,exist_ok=True)
        os.makedirs(f'{save_dir}/mats/last', exist_ok=True)
        os.makedirs(f'{save_dir}/mats/best', exist_ok=True)
        os.makedirs(f'{save_dir}/checkpoints', exist_ok=True)

        train_ds,s_train_ds,test_ds,val_ds = self.create_datasets(start_date,end_date_train,end_date_val,shuffle)
        self.build_model()
        if obj_checkpoint is not None:
            self.model.load_weights(f'{save_dir}/checkpoints/{obj_checkpoint}/checkpoint')

        losses = {'train':[],'curr_save_losses': {}}
        t = time.time()
        best_loss = {}
        for epoch in range(max_epochs):
            lss = self.train_step(train_ds['dataset'])
            print(f'time:{int(time.time() - t)}, training epoch:{epoch+1} loss:{lss}')

            if epoch% n_tosave==0:
                test_loss = self.save(f'{save_dir}/mats/last',s_train_ds,test_ds,val_ds,losses)
                for obj_name in test_loss:
                    if obj_name not in best_loss:
                        best_loss[obj_name]=np.inf

                    if np.mean(test_loss[obj_name])<=best_loss[obj_name]:
                        best_loss[obj_name] = np.mean(test_loss[obj_name])
                        if os.path.exists(f'{save_dir}/mats/best/{obj_name}'):
                            shutil.rmtree(f'{save_dir}/mats/best/{obj_name}')
                        shutil.copytree(f'{save_dir}/mats/last', f'{save_dir}/mats/best/{obj_name}')

                        repo = git.Repo(search_parent_directories=True)
                        sha = repo.head.object.hexsha
                        with open(f'{save_dir}/checkpoints/commit_hash.txt', 'w+') as f:
                            f.write(str(sha))

                        self.model.save_weights(f'{save_dir}/checkpoints/{obj_name}/checkpoint')

                if bucket_name is not None:
                    bucket = storage.client.Client().get_bucket(bucket_name)
                    upload_local_directory_to_gcs(f'{save_dir}/mats', bucket, f'{prefix}/mats')
                    upload_local_directory_to_gcs(f'{save_dir}/checkpoints', bucket, f'{prefix}/checkpoints')

    def predict(self,save_dir,checkpoint_path,start_date,end_date_train,end_date_val,bucket_name=None,prefix=''):
        os.makedirs(save_dir,exist_ok=True)
        os.makedirs(f'{save_dir}', exist_ok=True)

        train_ds,s_train_ds,test_ds,val_ds = self.create_datasets(start_date,end_date_train,end_date_val,shuffle=False)
        self.build_model()
        self.model.load_weights(checkpoint_path)

        losses = {'train':[],'curr_save_losses': {}}
        self.save(save_dir, train_ds, test_ds, val_ds, losses)

    def save(self,save_dir,train_ds,test_ds,val_ds,losses):

        def save_ds_run(ds_name,ds,curr_save_losses):

            ypreds = []
            labels = []
            loss_value = {}
            for step, (x_batch, y_batch) in enumerate(ds['dataset']):
                ypred = self.model(x_batch, training=False)
                for obj in self.net.data2net.objectives:
                    if obj.name not in loss_value:
                        loss_value[obj.name] = []
                    loss_value[obj.name].append(obj.loss.calc_loss[self.net.name](ypred,y_batch[obj.name]).numpy())
                ypreds.append(ypred)
                labels.append(y_batch)

            raw_preds = np.concatenate(ypreds)
            label = {k: np.concatenate([l[k] for l in labels], 0) for k in labels[0]}
            invests = {}
            obj_dicts = {}
            for obj in self.net.data2net.objectives:
                inv_d = obj.loss.get_invest_dict[self.net.name](raw_preds, label[obj.name])
                invests[obj.name] = {k: v if (type(v)==np.ndarray) or (type(v)==float) or (type(v)==int) else v.numpy() for k, v in inv_d.items()}
                obj_dicts[obj.name] = obj.to_dict()
                if obj.name not in curr_save_losses:
                    curr_save_losses[obj.name] = []
                curr_save_losses[obj.name].append(np.mean(loss_value[obj.name]))

            label_param_names = {o.name: o.label.params for o in self.net.data2net.objectives}

            tickers = self.net.data2net.tickers
            strategy_params = self.net.data2net.strategies_params
            strategy_name = self.net.data2net.strategy_name

            # tickers_per_date = [[f'{r[0]}' for r in self.net.data2net.rows_per_date[datetime.fromisoformat(date).date()]] for date in ds['dates']  if self.net.data2net.rows_per_date !={}]
            # strategy_params_per_date = [[r[3] for r in self.net.data2net.rows_per_date[datetime.fromisoformat(date).date()]] for date in ds['dates'] if self.net.data2net.rows_per_date !={}]
            # 'tickers_per_date': tickers_per_date, 'strategy_params_per_date': strategy_params_per_date,

            scipy.io.savemat(f'{save_dir}/{ds_name}.mat', {'tickers':tickers,'strategy_params':strategy_params,'strategy_name':strategy_name, 'invests': invests, 'dates': ds['dates'],
                                                           'label_param_names': label_param_names, 'objectives': obj_dicts,'curr_save_losses':curr_save_losses})#,'raw_preds': raw_preds,'raw_labels':label

            return loss_value

        if losses['curr_save_losses'] == {}:
            losses['curr_save_losses']['train'] = {}
            losses['curr_save_losses']['test']  = {}
            losses['curr_save_losses']['val']   = {}

        self.model.reset_states()
        loss_train = save_ds_run('train', train_ds,losses['curr_save_losses']['train'])
        loss_test  = save_ds_run('test', test_ds,losses['curr_save_losses']['test'])
        loss_val   = save_ds_run('val', val_ds,losses['curr_save_losses']['val'])

        return loss_test

    def predict_live(self,save_dir,checkpoint_path,ds_start_date,ds_end_date=None):
        tf.keras.backend.clear_session()

        self.build_model()
        self.model.load_weights(checkpoint_path)

        self.model.reset_states()
        ds = self.get_dataset_by_dates(ds_start_date.replace(hour=0, minute=0, second=0, microsecond=0), ds_end_date=ds_end_date.replace(hour=23, minute=59, second=59, microsecond=999999), to_shuffle=False, drop_remainder=False)

        ypreds = []
        labels = []
        loss_value = {}
        for step, (x_batch, y_batch) in enumerate(ds['dataset']):
            ypred = self.model(x_batch, training=False)
            for obj in self.net.data2net.objectives:
                if obj.name not in loss_value:
                    loss_value[obj.name] = []
                loss_value[obj.name].append(obj.loss.calc_loss[self.net.name](ypred,y_batch[obj.name]).numpy())
            ypreds.append(ypred)
            labels.append(y_batch)

        raw_preds = np.concatenate(ypreds)
        label = {k: np.concatenate([l[k] for l in labels], 0) for k in labels[0]}
        invests = {}
        obj_dicts = {}
        for obj in self.net.data2net.objectives:
            inv_d = obj.loss.get_invest_dict[self.net.name](raw_preds, label[obj.name])
            invests[obj.name] = {k: v if (type(v)==np.ndarray)  or (type(v)==float) or (type(v)==int) else v.numpy() for k, v in inv_d.items()}
            obj_dicts[obj.name] = obj.to_dict()

        label_param_names = {o.name: o.label.params for o in self.net.data2net.objectives}

        scipy.io.savemat(f'{save_dir}/prediction.mat', {'strategy_params':self.net.data2net.strategies_params,'raw_preds': raw_preds,'raw_labels':label, 'invests': invests,'tickers':self.net.data2net.tickers, 'dates': ds['dates'], 'label_param_names': label_param_names, 'objectives': obj_dicts})
        return invests



# if __name__=='__main__':
#     from data.stock_data import *
#     import time
#     from objective import objective
#     from data2net.data2net import Data2Net
#     from core_architecture import core_archicture
#     from losses.profit import Profit
#     from losses.sortino import Sortino
#     from losses.exp_loss import ExpLoss
#
#     from net_creator.army import Army
#     qst = f""" select ticker,max_invest from algo_dataset.ticker_turnover order by max_invest desc """
#
#     from data.stock_data import *
#     import time
#     from objective import objective
#     from strategies import none_strategy,trailing_sl_strategy,limits_pairs_strategy
#
#     # qst = f""" select ticker,max_invest from algo_dataset.ticker_turnover order by max_invest desc """
#     qst = """
#     select * from (select ticker,min(date) as mn_date,max(date) as mx_date,AVG(volume*average) as turnover ,count(distinct(extract(date from date))) as c from (select * from `KIRA.intraday_1min_full_new` where date >'2013-01-01') group by(ticker) ) where mn_date< '2018-01-01' and c>1000 order by turnover desc
#     """
#     df = client.query(qst).result().to_dataframe()
#     tickers = df['ticker'].tolist()
#
#     profit_objective = objective.Objective(name='normal', label=StockLabel(delay=2))
#     profit_objective.aum=1e6
#
#     objectives = []
#     tsl = trailing_sl_strategy.TrailingSLStrategyGenerator(trailing_sl=[0.002,0.002,0.002,0.002],tp=[0.002,0.004,0.008,0.016])
#     # tsl = limits_pairs_strategy.LimitsPairsStrategy()
#     main_path=fr'd:\trailing_sl'
#     d2n = Data2Net(main_path=main_path,tickers=tickers[0:3],start_date='2015-12-01',end_date='2021-06-07',data_sources=[StockData()],
#                    objectives=[profit_objective],replace_dates=['2015-12-01'],
#                    strategy_generators= [tsl])
#
#     profit_objective.loss = Profit(d2n,profit_objective)
#
#     t  = time.time()
#
#     d2n.get()
#     print(time.time()-t)
#     d2n.transform()
#     print(time.time() - t)
#
#
#     lstm = core_archicture.BasicLstm(start_dense_size=10,ticker_embedding=20,lstm_size=10,mid_dense_size=[10,10],out_dense_size=10,stateful=True)
#
#     army = Army(d2n,lstm,batch_size=100)
#     runner = Runner(army)
#
#     runner.train(fr'{main_path}\run_saves',datetime(2015,1,1),datetime(2019,1,1),datetime(2020,4,1),shuffle=False,n_tosave=20,max_epochs=2000)