import os
os.environ['CUDA_VISIBLE_DEVICES'] = '-1'

import os,sys,scipy.io
from threading import Thread
from google.cloud import bigquery
import pandas_market_calendars as mcal
from datetime import datetime
import tensorflow as tf
from tensorflow import keras
from data2net.data2net import QueryHandler
import numpy as np
import pandas as pd
import time
from copy import deepcopy


def add_jobs(qhandler,main_path,tickers,dates,table_name):
        for date in dates:
                date = date.date()
                for ticker in tickers:
                        qstr = f"""
                        SELECT distinct * from 
                        (select * from `{table_name}`  where ticker = '{ticker}' and extract(date from date)='{date}' order by date ) as a
                        join 
                        (
                        (
                        SELECT close as last_close ,date as last_date FROM `{table_name}`  where ticker = '{ticker}' and extract(date from date)<'{date}' order by date desc limit 1
                        ) as b
                        join 
                        (
                        SELECT open as next_open ,date as next_date FROM `{table_name}`  where ticker = '{ticker}' and extract(date from date)>'{date}' order by date asc limit 1
                        ) as c
                        on True
                        )
                        on True
                        """

                        qhandler.append_qstr_path( qstr=qstr, path=f'{main_path}/{ticker}_{date}.csv', date=date, ticker=ticker, data_name='data_name')

qhandler = QueryHandler(replace_dates=[],asset='STOCK',live=True)

main_path = fr'D:/evolution'
os.makedirs(main_path,exist_ok=True)
# tickers = ['SPY','AAPL','QQQ','MSFT']
# start_date = datetime(2019,1,1)
# end_date = datetime.now()
#
# table_name = 'KIRA.intraday_1min_full_clean'
#
# nyse = mcal.get_calendar('NYSE')
#
# dates = nyse.schedule(start_date, end_date).market_open.to_list()
#
# add_jobs(qhandler,main_path,tickers,dates,table_name)
# qhandler.finalize()
stocks_data = np.load(f'{main_path}/stocks_data.npy')

NMINUTES = stocks_data.shape[-1]
data_dir = fr'D:\temp_test_23_new\data\stock_data'

def loss_fcn(y,th,ypred,is_exist):
    dist_from_th = tf.maximum(y - th,0.0)
    dist_from_y  = abs(tf.squeeze(ypred) - y)
    _loss = tf.where(y<th,dist_from_th,dist_from_y)
    return tf.reduce_sum(_loss*is_exist)/tf.reduce_sum(is_exist)

def build_model(niter,ndays,max_result,ntickers):
    sz = 10
    inputs  = keras.Input(batch_shape=(niter,ndays,max_result,NMINUTES,ntickers*2), dtype='float64')
    nminute = keras.Input(batch_shape=(niter,ndays,max_result), dtype='int32')

    r_inputs  = tf.reshape(inputs, [niter * ndays * max_result, NMINUTES, ntickers * 2])
    r_nminute = tf.reshape(nminute, [-1])

    layers = [keras.layers.Dense(sz, activation='elu') for _ in range(3)] + [keras.layers.LSTM(sz,stateful=False,return_sequences=True)] + [keras.layers.Dense(sz, activation='elu') for _ in range(2)] + [keras.layers.Dense(1)]

    out = r_inputs
    for l in layers:
        out = l(out)
    out = tf.reduce_sum(tf.one_hot(r_nminute, NMINUTES) * tf.squeeze(out,-1), -1, True)
    out = tf.reshape(out,[niter,ndays,max_result])
    model = keras.Model([inputs,nminute], out)
    return {'model':model,'optimizer':keras.optimizers.Adam()}

def train_step(model,x,y,th,is_exist):
    with tf.GradientTape() as tape:
        grades = model['model'](x, training=True)
        loss_value = loss_fcn(y,th,grades,is_exist)

    grads = tape.gradient(loss_value, model['model'].trainable_weights)
    model['optimizer'].apply_gradients(zip(grads, model['model'].trainable_weights))
    return float(loss_value),grades.numpy()


def create_stock_data(main_path,data_dir):
    tickers = list(set([f.split('_')[0] for f in os.listdir(data_dir)]))
    dates   = list(sorted(set([f.split('_')[1].split('.')[0] for f in os.listdir(data_dir)])))
    stocks_data = np.zeros((len(tickers),len(dates),NMINUTES))
    for it,t in enumerate(tickers):
        print(it,t)
        for id,d in enumerate(dates):
            if os.path.exists(f'{data_dir}/{t}_{d}.csv'):
                df = pd.read_csv(f'{data_dir}/{t}_{d}.csv')
                df = df.sort_values(by='date')
                v = (df.close.values[1::]/df.close.values[0:-1]-1)[0:390]
                stocks_data[it,id,0:v.size] = v
    np.save(f'{main_path}/stocks_data.npy',stocks_data)


class Allocation():
    def __init__(self,stocks_data,allocation,old_allocations):
        self.old_allocations = old_allocations
        self.stocks_data = stocks_data
        self.allocation = allocation
        self.real_grade = None# calc_grade()
        self.grade      = None# calc_grade()

    def calc_real_grade(self):
        self.real_grade =  (self.allocation*self.stocks_data).sum()

    def is_legal(self,alloc):
        is_legal = ((abs(alloc) <= 0.2).all()) & (abs(alloc).sum() <= 1)
        is_new   = tuple(alloc) not in self.old_allocations
        return is_legal&is_new

    def expand(self):
        all_new_alloc = []
        for k_ticker in range(self.stocks_data.size):
            new_alloc = deepcopy(self.allocation)
            new_alloc[k_ticker] += 0.05
            if self.is_legal(new_alloc):
                all_new_alloc.append(Allocation(self.stocks_data,new_alloc,self.old_allocations))
                self.old_allocations.add(tuple(new_alloc))

            new_alloc = deepcopy(self.allocation)
            new_alloc[k_ticker] -= 0.05
            if self.is_legal(new_alloc):
                all_new_alloc.append(Allocation(self.stocks_data,new_alloc,self.old_allocations))
                self.old_allocations.add(tuple(new_alloc))
        return all_new_alloc


class MinuteTree():
    def __init__(self,stocks_data,prev_alloc):
        self.old_allocations = set([tuple(prev_alloc)])
        self.allocation = [Allocation(stocks_data,prev_alloc,self.old_allocations)]
        for alloc in self.allocation:
            alloc.calc_real_grade()

    def expand(self):
        new_alloc = []
        for alloc in self.allocation:
            new_alloc = alloc.expand()

        for alloc in new_alloc:
            alloc.calc_real_grade()

        self.allocation+=new_alloc

    def to_net(self):
        x = np.array([a.allocation for a in self.allocation])
        y = np.array([a.real_grade for a in self.allocation])
        return x,y



def predict(stocks_data,model,max_result,niter,train = False):
    loss = np.zeros((stocks_data.shape[-1]))

    init_alloc = np.ones_like(stocks_data[:,:,0].T)*0.05
    base_net_input = np.transpose(stocks_data,[1,2,0])
    base_net_input = np.concatenate([base_net_input*0,base_net_input],-1)


    for kminute in range(stocks_data.shape[-1]):
        minute_trees = []
        for iday in range(stocks_data.shape[1]):
            minute_trees.append(MinuteTree(stocks_data[:,iday,kminute],init_alloc[iday]))

        all_iter_x = []
        all_iter_y = []
        all_is_exist = []
        start_time = time.time()


        for iter in range(niter):
            print(f'minute {kminute} iter {iter} : {time.time() - start_time}')
            for iday in range(len(minute_trees)):
                minute_trees[iday].expand()

            for iday in range(len(minute_trees)):
                minute_trees[iday].allocation = sorted(minute_trees[iday].allocation,key=lambda x:x.real_grade)[-max_result::]

            all_x = []
            all_y = []
            all_iday = []
            is_exist = []
            for iday in range(len(minute_trees)):
                x,y = minute_trees[iday].to_net()
                xt = base_net_input[iday:(iday+1),:,:]
                xtt = np.zeros((max_result,) + xt.shape[1::])
                exist = np.zeros((max_result))
                for ii in range(x.shape[0]):
                    xt[0,kminute,0:x.shape[-1]] = x[ii]
                    xtt[ii] = xt
                    exist[ii] = 1
                yy = np.zeros(max_result)
                yy[0:y.size] = y
                all_x.append(np.expand_dims(xtt,0))
                all_y.append(np.expand_dims(yy,0))
                is_exist.append(np.expand_dims(exist, 0))
                all_iday.append(np.ones_like(np.expand_dims(y,0))*iday)

            x = np.concatenate(all_x,axis=0)
            y = np.concatenate(all_y,axis=0)
            is_exist = np.concatenate(is_exist,axis=0)

            all_iter_x.append(np.expand_dims(x,0))
            all_iter_y.append(np.expand_dims(y,0))
            all_is_exist.append(np.expand_dims(is_exist, 0))

        x = np.concatenate(all_iter_x, axis=0).astype('float32')
        y = np.concatenate(all_iter_y, axis=0).astype('float32')
        is_exist = np.concatenate(all_is_exist, axis=0).astype('float32')
        nminute = (np.ones_like(y)*kminute).astype('float32')
        th = 0.0*y
        if train:
            loss[kminute],grades = train_step(model, [x, nminute], y,th, is_exist)
        else:
            grades = model['model']([x, nminute]).numpy()
        tmp_out = {'loss': loss.mean(),'th':th, 'loss_full': loss, 'base_net_input': base_net_input, 'stocks_data': np.transpose(stocks_data, [1, 2, 0]),'grades':grades,'y':y,'kminute':kminute}
        if train:
            scipy.io.savemat(f'{main_path}/mid_run_{kminute}.mat', tmp_out)
        else:
            scipy.io.savemat(f'{main_path}/test_{kminute}.mat', tmp_out)

    out = {'loss':loss.mean(),'loss_full': loss, 'base_net_input': base_net_input, 'stocks_data': np.transpose(stocks_data, [1, 2, 0])}

    return out







train_stocks_data = stocks_data[:,0:400]
test_stocks_data = stocks_data[:,400:800]

max_result = 10
niter = 10
model = build_model(ntickers=stocks_data.shape[0],niter=niter,ndays=train_stocks_data.shape[1],max_result=max_result)


start_time = time.time()

loss = []
loss_test = []
for epoch in range(100):
    out = predict(train_stocks_data,model,max_result,niter,train=True)
    loss+=out['loss']
    print(f'loss epoch {epoch+1} time elapsed {int(time.time() - start_time)}: {np.mean(loss)} , {np.mean(loss_test)}')
    scipy.io.savemat(f'{main_path}/out.mat', out)

    out = predict(test_stocks_data,model,max_result,niter)
    loss_test+=out['loss']

    scipy.io.savemat(f'{main_path}/test.mat', out)
    model['model'].save_weights(f'{main_path}/model.h5',save_format="h5")
