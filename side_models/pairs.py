from google.cloud import bigquery
import pandas as pd
import numpy as np
import sys,scipy.io

def pairs(ticker_a,ticker_b):
    qstr = f""" select date,yahoo_unadjusted_close*yahoo_adjustment_factor from `checked_data.2021-07-06_open_checked_stocks_csv` where ticker = '{ticker_a}'"""
    df_a = bigquery.Client().query(qstr).result().to_dataframe()

    qstr = f""" select date,yahoo_unadjusted_close*yahoo_adjustment_factor from `checked_data.2021-07-06_open_checked_stocks_csv` where ticker = '{ticker_b}'"""
    df_b = bigquery.Client().query(qstr).result().to_dataframe()

    close = pd.merge(df_a,df_b,on='date').sort_values(by='date')
    close = close.groupby('date').mean()
    data  = np.zeros(close.shape[0])
    label = np.zeros(close.shape[0])
    for k in range(7):
        tmp = close.shift(2**k)
        dff = close['f0__x']/tmp['f0__x']-close['f0__y']/tmp['f0__y']
        if k == 0:
            label[0:-1] = dff[1::]
        data+=dff.values/np.sqrt(2**k)
    dates = [d.isoformat() for d in close.index.date[np.isnan(data) == False]]
    label = label[np.isnan(data)==False]
    data = data[np.isnan(data)==False]
    label[np.isnan(label)] = 0
    return data,label,dates

if __name__=='__main__':
    main_path = r'D:/pairs'
    for ticker_a,ticker_b in [('XLV','IXJ'),('TQQQ','TECL')]:
        data,label,dates = pairs(ticker_a,ticker_b)
        scipy.io.savemat(f'{main_path}/{ticker_a}_{ticker_b}.mat', {'dates':dates,'label': label,'data':data})
