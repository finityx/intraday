import os
os.environ['CUDA_VISIBLE_DEVICES'] = '-1'

import scipy.io,scipy.special
import pandas_market_calendars as mcal
from datetime import datetime
import tensorflow as tf
from tensorflow import keras
from data2net.data2net import QueryHandler
import numpy as np
import pandas as pd,time
from tensorflow.keras.utils import to_categorical


def add_jobs(qhandler,main_path,tickers,dates,table_name):
        for date in dates:
            date = date.date()
            for ticker in tickers:
                    qstr = f"""
                    SELECT distinct * from 
                    (select * from `{table_name}`  where ticker = '{ticker}' and extract(date from date)='{date}' order by date ) as a
                    join 
                    (
                    (
                    SELECT close as last_close ,date as last_date FROM `{table_name}`  where ticker = '{ticker}' and extract(date from date)<'{date}' order by date desc limit 1
                    ) as b
                    join 
                    (
                    SELECT open as next_open ,date as next_date FROM `{table_name}`  where ticker = '{ticker}' and extract(date from date)>'{date}' order by date asc limit 1
                    ) as c
                    on True
                    )
                    on True
                    """

                    qhandler.append_qstr_path( qstr=qstr, path=f'{main_path}/{ticker}_{date}.csv', date=date, ticker=ticker, data_name='data_name')


qhandler = QueryHandler(replace_dates=[],asset='STOCK',live=True)
NSTEPS = 30
main_path = fr'D:/compare_to_random_improved_large'
os.makedirs(main_path,exist_ok=True)

def create_stock_data(main_path,data_dir):
    NMINUTES = 390
    tickers = list(set([f.split('_')[0] for f in os.listdir(data_dir)]))
    dates   = list(sorted(set([f.split('_')[1].split('.')[0] for f in os.listdir(data_dir)])))
    stocks_data = np.zeros((len(tickers),len(dates),NMINUTES))
    for it,t in enumerate(tickers):
        print(it,t)
        for id,d in enumerate(dates):
            if os.path.exists(f'{data_dir}/{t}_{d}.csv'):
                df = pd.read_csv(f'{data_dir}/{t}_{d}.csv')
                df = df.sort_values(by='date')
                v = (df.close.values[1::]/df.close.values[0:-1]-1)[0:390]
                stocks_data[it,id,0:v.size] = v
    np.save(f'{main_path}/stocks_data.npy',stocks_data)

def download_all(tickers,start_year = 2019):
    start_date = datetime(start_year,1,1)
    end_date = datetime.now()
    table_name = 'KIRA.intraday_1min_full_clean'
    nyse = mcal.get_calendar('NYSE')
    dates = nyse.schedule(start_date, end_date).market_open.to_list()
    os.makedirs(f'{main_path}/data',exist_ok=True)
    add_jobs(qhandler,f'{main_path}/data',tickers,dates,table_name)
    print('qhandler')
    qhandler.finalize()
    create_stock_data(main_path, f'{main_path}/data')

# download_all(tickers=pd.read_csv(f'{main_path}/tickers.csv').ticker.tolist(),start_year=2019)
stocks_data = np.load(f'{main_path}/stocks_data.npy')

data_dir = fr'D:\temp_test_23_new\data\stock_data'
data_options = np.linspace(-2,2, 21)/100

# def get_predictions(test_input,layers):
#     def step(lstm_input, states):
#         return layers['lstm'].cell(lstm_input,states)
#
#     out = test_input
#     for l in layers['pre_dense']:
#         out = l(out)
#
#     out_data_op = data_options.reshape(-1,1)
#     for l in layers['pre_dense']:
#         out_data_op = l(out_data_op)
#
#     start_states = layers['lstm'].get_initial_state(out[:,0,:]*0)
#     outs_lstm = []
#     ons = tf.ones([tf.shape(out)[0],out_data_op.shape[0],1])
#     flat_data_op = tf.expand_dims(out_data_op, 0)*ons
#     flat_data_op = tf.reshape(flat_data_op,[-1,flat_data_op.shape[-1]])
#     for kmin in range(test_input.shape[1]):
#         print(kmin)
#         states = start_states
#
#         out_lstm = 0.0
#         for klh in range(NSTEPS):
#             flat_states = [tf.reshape(tf.expand_dims(s, 1) * ons, [-1, s.shape[-1]]) for s in states]
#             c_out,states = step(flat_data_op, flat_states)
#             states = [tf.reduce_mean(tf.reshape(s,[-1,ons.shape[1],s.shape[-1]]),1) for s in states]
#             out_lstm+=c_out/NSTEPS
#
#         outs_lstm.append(tf.expand_dims(out_lstm,1))
#
#         start_states = step(out[:,kmin,:], start_states)[1]
#
#     outs_lstm = tf.concat(outs_lstm, 1)
#     outs_lstm =  tf.reshape(outs_lstm,[-1,ons.shape[1],outs_lstm.shape[1],outs_lstm.shape[2]])
#     for l in layers['out_denses']:
#         outs_lstm = l(outs_lstm)
#
#     outs_lstm = tf.transpose(tf.squeeze(outs_lstm, -1), [0, 2, 1])
#
#     outs_lstm =  tf.nn.softmax(outs_lstm,-1)
#     return outs_lstm

def get_predictions(test_input,layers):

    out = test_input
    for l in layers['pre_dense']:
        out = l(out)

    out_lstm = layers['lstm'](test_input)

    for l in layers['out_denses']:
        out_lstm = l(out_lstm)

    outs_lstm =  tf.nn.softmax(out_lstm,-1)
    outs_lstm = tf.concat([outs_lstm[:, 0:1, :] * 0 + 1 / data_options.size, outs_lstm[:, 0:-1]], 1)
    return outs_lstm


def build_model(NMINUTES):
    sz = 50
    inputs = keras.Input(shape=(NMINUTES,1),dtype='float32')
    pre_dense = [keras.layers.Dense(sz, activation='elu') for _ in range(3)]
    lstm  = keras.layers.LSTM(sz, return_sequences=True)
    out_denses = [keras.layers.Dense(sz, activation='elu') for _ in range(2)] + [keras.layers.Dense(data_options.size)]
    layers = {'pre_dense':pre_dense,'lstm':lstm,'out_denses':out_denses}
    output = get_predictions(inputs, layers)

    bce = tf.keras.losses.CategoricalCrossentropy()
    model = keras.Model(inputs, output)
    model.compile(optimizer="adam", loss=bce)

    return model,layers


lst_stocks_data = []
lngth = []


stocks_label = np.concatenate([stocks_data[:,:,k:(k+NSTEPS)].sum(-1,keepdims=True) for k in range(stocks_data.shape[-1]-NSTEPS)],-1)
stocks_data  = stocks_data[:,:,0:-NSTEPS]

train_stocks_data = stocks_data[:,0:-250]
train_stocks_data = train_stocks_data.reshape(train_stocks_data.shape[0]*train_stocks_data.shape[1],train_stocks_data.shape[2],1)

train_stocks_label = stocks_label[:,0:-250]
train_stocks_label = train_stocks_label.reshape(train_stocks_label.shape[0]*train_stocks_label.shape[1],train_stocks_label.shape[2],1)

test_stocks_data = stocks_data[:,-250::]
shape_test_stocks_data = list(test_stocks_data.shape)
test_stocks_data = test_stocks_data.reshape(test_stocks_data.shape[0]*test_stocks_data.shape[1],test_stocks_data.shape[2],1)

test_stocks_label = stocks_label[:,-250::]
test_stocks_label = test_stocks_label.reshape(test_stocks_label.shape[0]*test_stocks_label.shape[1],test_stocks_label.shape[2],1)


a = abs(train_stocks_label-data_options.reshape(1,1,-1)).argmin(-1)
ons_like = to_categorical(a,num_classes = data_options.size)

a = abs(test_stocks_label-data_options.reshape(1,1,-1)).argmin(-1)
ons_like_test = to_categorical(a,num_classes = data_options.size)

wtrain = abs((data_options.reshape(1,1,-1)*ons_like).sum(-1))
wtest  = abs((data_options.reshape(1,1,-1)*ons_like_test).sum(-1))

model,layers = build_model(train_stocks_data.shape[1])

best_loss = 1e8
loss = []
start_time = time.time()

for epoch in range(100):
    # model.load_weights(f'{main_path}/model.h5')

    hist = model.fit(train_stocks_data,ons_like,epochs=5)
    loss+=hist.history['loss']

    pred = model.predict(test_stocks_data)
    new_pred_shape = shape_test_stocks_data[0:2] + list(pred.shape[1::])

    scipy.io.savemat(f'{main_path}/save_mat.mat',{'options':data_options,'pred':pred.reshape(new_pred_shape),'test_stocks_data':test_stocks_data.reshape(shape_test_stocks_data),'test_stocks_label':test_stocks_label.reshape(shape_test_stocks_data),'loss':loss})

    curr_loss = model.evaluate(test_stocks_data,ons_like_test)
    print(f'saving mats... time elapsed {int(time.time() - start_time)} test loss {curr_loss} best loss {best_loss}')

    if curr_loss<=best_loss:
        best_loss = curr_loss
        scipy.io.savemat(f'{main_path}/best_mat.mat', {'options': data_options, 'pred': pred.reshape(new_pred_shape), 'test_stocks_data': test_stocks_data.reshape(shape_test_stocks_data),'test_stocks_label':test_stocks_label.reshape(shape_test_stocks_data), 'loss': loss})
        model.save_weights(f'{main_path}/model.h5',save_format="h5")
