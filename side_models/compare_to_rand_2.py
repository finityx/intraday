import os
os.environ['CUDA_VISIBLE_DEVICES'] = '-1'

import sys,scipy.io,scipy.special
from threading import Thread
from google.cloud import bigquery
import pandas_market_calendars as mcal
from datetime import datetime
import tensorflow as tf
from tensorflow import keras
from data2net.data2net import QueryHandler
import numpy as np
import pandas as pd
import time,copy



def add_jobs(qhandler,main_path,tickers,dates,table_name):
        for date in dates:
                date = date.date()
                for ticker in tickers:
                        qstr = f"""
                        SELECT distinct * from 
                        (select * from `{table_name}`  where ticker = '{ticker}' and extract(date from date)='{date}' order by date ) as a
                        join 
                        (
                        (
                        SELECT close as last_close ,date as last_date FROM `{table_name}`  where ticker = '{ticker}' and extract(date from date)<'{date}' order by date desc limit 1
                        ) as b
                        join 
                        (
                        SELECT open as next_open ,date as next_date FROM `{table_name}`  where ticker = '{ticker}' and extract(date from date)>'{date}' order by date asc limit 1
                        ) as c
                        on True
                        )
                        on True
                        """

                        qhandler.append_qstr_path( qstr=qstr, path=f'{main_path}/{ticker}_{date}.csv', date=date, ticker=ticker, data_name='data_name')


qhandler = QueryHandler(replace_dates=[],asset='STOCK',live=True)
#
main_path = fr'D:/compare_to_random_improved'
# tickers = ['SPY','AAPL','QQQ','MSFT']
# start_date = datetime(2019,1,1)
# end_date = datetime.now()
#
# table_name = 'KIRA.intraday_1min_full_clean'
#
# nyse = mcal.get_calendar('NYSE')
#
# dates = nyse.schedule(start_date, end_date).market_open.to_list()
#
# add_jobs(qhandler,main_path,tickers,dates,table_name)
# qhandler.finalize()
stocks_data = np.load(f'{main_path}/stocks_data.npy')

NMINUTES = stocks_data.shape[-1]
data_dir = fr'D:\temp_test_23_new\data\stock_data'

def predict(inputs,data_options,layers):
    def step(lstm_input, states):
        return layers['lstm'].cell(lstm_input, states)

    states =layers['lstm'].get_initial_state(tf.zeros((1,) + layers['lstm'].input_shape[1::]))
    out = inputs
    for l in layers['pre_dense']:
        out = l(out)
    out_data_op = data_options.reshape(-1,1)
    for l in layers['pre_dense']:
        out_data_op = l(out_data_op)

    neg_out_data_op = -data_options.reshape(-1,1)
    for l in layers['pre_dense']:
        neg_out_data_op = l(neg_out_data_op)

    outs_lstm = []
    for kmin in range(inputs.shape[1]):
        out_lstm = tf.concat([tf.expand_dims(step(np.ones_like(inputs[:, kmin, :]) * out_data_op[k:(k + 1)],states)[0],1) for k in range(data_options.size)],1)
        for l in layers['out_denses']:
            out_lstm = l(out_lstm)

        n_out_lstm = tf.concat([tf.expand_dims(step(np.ones_like(inputs[:, kmin, :]) * neg_out_data_op[k:(k + 1)],states)[0],1) for k in range(data_options.size)],1)
        for l in layers['out_denses']:
            n_out_lstm = l(n_out_lstm)

        outs_lstm.append(out_lstm-n_out_lstm)
        out_regular,states = step(out[:,kmin,:], states)
    outs_lstm =  tf.nn.sigmoid(tf.concat(outs_lstm,-1))
    return outs_lstm.numpy()

def build_model():
    sz = 20
    inputs = keras.Input(shape=(NMINUTES,1),dtype='float32')
    random_input_sign = keras.Input(shape=(NMINUTES,1),dtype='float32')
    random_input = random_input_sign*inputs
    pre_dense = [keras.layers.Dense(sz, activation='elu') for _ in range(3)]
    lstm  = keras.layers.LSTM(sz,return_sequences=True,stateful=False)
    out_denses = [keras.layers.Dense(sz, activation='elu') for _ in range(2)] + [keras.layers.Dense(1)]
    layers = pre_dense + [lstm] + out_denses
    out = inputs
    for l in layers:
        out = l(out)

    out_random = random_input
    for l in layers:
        out_random = l(out_random)

    bce = tf.keras.losses.BinaryCrossentropy(from_logits=True)
    model = keras.Model([inputs,random_input_sign], tf.nn.softmax(tf.concat([out,out_random],-1)))
    model.compile(optimizer="adam", loss=bce)

    return model,{'pre_dense':pre_dense,'lstm':lstm,'out_denses':out_denses}

def create_stock_data(main_path,data_dir):
    tickers = list(set([f.split('_')[0] for f in os.listdir(data_dir)]))
    dates   = list(sorted(set([f.split('_')[1].split('.')[0] for f in os.listdir(data_dir)])))
    stocks_data = np.zeros((len(tickers),len(dates),NMINUTES))
    for it,t in enumerate(tickers):
        print(it,t)
        for id,d in enumerate(dates):
            if os.path.exists(f'{data_dir}/{t}_{d}.csv'):
                df = pd.read_csv(f'{data_dir}/{t}_{d}.csv')
                df = df.sort_values(by='date')
                v = (df.close.values[1::]/df.close.values[0:-1]-1)[0:390]
                stocks_data[it,id,0:v.size] = v
    np.save(f'{main_path}/stocks_data.npy',stocks_data)

# create_stock_data(main_path,data_dir)

train_stocks_data = stocks_data[:,0:600]
train_stocks_data = train_stocks_data.reshape(train_stocks_data.shape[0]*train_stocks_data.shape[1],train_stocks_data.shape[2],1)


test_stocks_data = stocks_data[:,600::]
shape_test_stocks_data = list(test_stocks_data.shape)
test_stocks_data = test_stocks_data.reshape(test_stocks_data.shape[0]*test_stocks_data.shape[1],test_stocks_data.shape[2],1)
center = (abs(stocks_data).sum(-1)/(stocks_data!=0).sum(-1))[:,599:-1].reshape(-1)
center[np.isnan(center)] = 0.0
model,layers = build_model()
ons_like = np.concatenate([np.ones_like(train_stocks_data),np.zeros_like(train_stocks_data)],-1)
ons_like_test = np.concatenate([np.ones_like(test_stocks_data),np.zeros_like(test_stocks_data)],-1)

best_loss = 1e8
loss = []
start_time = time.time()
for epoch in range(100):
    model.load_weights(f'{main_path}/model.h5')

    max_epoch = 0
    for kk in range(max_epoch):
        hist = model.fit([train_stocks_data,2*(np.random.random(train_stocks_data.shape)>0.5)-1],ons_like,epochs=1)
        loss+=hist.history['loss']
        print(f'loss epoch {epoch+1} , mini epoch {kk+1}/{max_epoch} time elapsed {int(time.time() - start_time)}: {np.mean(loss):0.03}')

    x = test_stocks_data.reshape(-1)
    options = np.linspace(0,1,11)/100
    pred = predict(test_stocks_data,options,layers)
    scipy.io.savemat(f'{main_path}/save_mat.mat',{'options':options,'pred':pred.reshape(shape_test_stocks_data[0:2] + [-1] + shape_test_stocks_data[-1::]),'test_stocks_data':test_stocks_data.reshape(shape_test_stocks_data),'loss':loss})
    print(f'saving mats... time elapsed {int(time.time() - start_time)}')
    hist = model.fit([test_stocks_data, 2 * (np.random.random(test_stocks_data.shape) > 0.5) - 1], ons_like_test, epochs=1)
    exit(0)
    curr_loss =  hist.history['loss'][-1]
    if curr_loss<=best_loss:
        best_loss = curr_loss
        scipy.io.savemat(f'{main_path}/best_mat.mat', {'options': options, 'pred': pred.reshape(shape_test_stocks_data[0:2] + [-1] + shape_test_stocks_data[-1::]), 'test_stocks_data': test_stocks_data.reshape(shape_test_stocks_data), 'loss': loss})
        model.save_weights(f'{main_path}/model.h5',save_format="h5")
