import os
os.environ['CUDA_VISIBLE_DEVICES'] = '-1'

import sys,scipy.io
from threading import Thread
from google.cloud import bigquery
import pandas_market_calendars as mcal
from datetime import datetime
import tensorflow as tf
from tensorflow import keras
from data2net.data2net import QueryHandler
import numpy as np
import pandas as pd
import time,copy
from tensorflow.keras.utils import to_categorical
import numpy as np
data_dir = main_path = fr'D:/compare_to_random_daily'
os.makedirs(main_path,exist_ok=True)

def create_stock_data(main_path,data_dir,tickers):
    qstr = f""" select ticker,date,yahoo_unadjusted_close*yahoo_adjustment_factor from `checked_data.2021-07-06_open_checked_stocks_csv` where ticker in {tuple(tickers)}"""
    df = bigquery.Client().query(qstr).result().to_dataframe()

    close = df.groupby(['ticker','date']).mean()['f0_']
    stocks_data = np.zeros((len(df.ticker.unique()),len(df.date.unique())))
    date_df = pd.DataFrame({'date':sorted(df.date.unique())})
    for it,t in enumerate(tickers):
        tmp = pd.merge(date_df,close[t],left_on='date',right_on=close[t].index,how='left')
        stocks_data[it,:] = (tmp['f0_']/tmp['f0_'].shift(1)-1).values
    stocks_data = stocks_data[:,((np.isnan(stocks_data)==False).sum(0)>5)]

    np.save(f'{main_path}/stocks_data.npy',stocks_data)

tickers = ["XLE","XLF","XLI","XLY","XLV","XLU","VNQ","AAPL","CVX","EEM","WMT","TQQQ", "SOXL", "FAS", "TNA","NVDA",'NUGT','SPY']

# create_stock_data(main_path,data_dir,tickers)

stocks_data = np.load(f'{main_path}/stocks_data.npy')

NMINUTES = None
data_options = np.linspace(-10, 10, 41) / 100


def get_predictions(test_input,layers):
    def step(lstm_input, states):
        return layers['lstm'].cell(lstm_input, states)


    out = test_input
    for l in layers['pre_dense']:
        out = l(out)

    out_data_op = data_options.reshape(-1,1)
    for l in layers['pre_dense']:
        out_data_op = l(out_data_op)

    states = layers['lstm'].get_initial_state(out)
    outs_lstm = []
    for kmin in range(test_input.shape[1]):
        print(kmin)
        out_lstm = tf.expand_dims(tf.concat([tf.expand_dims(step(tf.ones_like(test_input[:, kmin, :]) * out_data_op[k:(k + 1)],states)[0],1) for k in range(data_options.size)],1),1)
        outs_lstm.append(out_lstm)
        states = step(out[:,kmin,:], states)[1]

    outs_lstm = tf.concat(outs_lstm, 1)
    for l in layers['out_denses']:
        outs_lstm = l(outs_lstm)

    outs_lstm =  tf.nn.softmax(tf.squeeze(outs_lstm,-1),-1)
    return outs_lstm

def build_model(bsize):
    sz = 20
    inputs = keras.Input(shape=(bsize,1),dtype='float32')
    pre_dense = [keras.layers.Dense(sz, activation='elu') for _ in range(3)]
    lstm  = keras.layers.LSTM(sz,return_sequences=True,stateful=False)
    out_denses = [keras.layers.Dense(sz, activation='elu') for _ in range(2)] + [keras.layers.Dense(1)]
    layers = {'pre_dense':pre_dense,'lstm':lstm,'out_denses':out_denses}
    output = get_predictions(inputs, layers)

    bce = tf.keras.losses.CategoricalCrossentropy()
    model = keras.Model(inputs, output)
    model.compile(optimizer="adam", loss=bce)

    return model,layers

stocks_data[np.isnan(stocks_data)]=0
bsize = 21
stock_data_batch = np.zeros([stocks_data.shape[0],stocks_data.shape[1]-bsize,bsize])
for k in range(stocks_data.shape[1]-bsize):
    stock_data_batch[:,k,:] = stocks_data[:,k:(k+bsize)]
train_stocks_data =  stock_data_batch[:,0:-500].reshape(-1,stock_data_batch.shape[2],1)
test_stocks_data =  stock_data_batch[:,-500::].reshape(-1,stock_data_batch.shape[2],1)


a = abs(train_stocks_data-data_options.reshape(1,1,-1)).argmin(-1)
ons_like = to_categorical(a,num_classes = data_options.size)

model,layers = build_model(bsize)

best_loss = 1e8
loss = []
start_time = time.time()
for epoch in range(1000):
    # model.load_weights(f'{main_path}/model.h5')

    hist = model.fit(train_stocks_data,ons_like,epochs=5)
    loss+=hist.history['loss']

    pred = model.predict(test_stocks_data)
    scipy.io.savemat(f'{main_path}/save_mat.mat',{'options':data_options,'pred':pred,'test_stocks_data':test_stocks_data,'loss':loss})
    print(f'saving mats... time elapsed {int(time.time() - start_time)}')

    model.save_weights(f'{main_path}/model.h5', save_format="h5")
    # hist = model.fit([test_stocks_data, 2 * (np.random.random(train_stocks_data.shape) > 0.5) - 1], ons_like, epochs=1)
    # curr_loss =  hist.history['loss'][-1]
    # if curr_loss<=best_loss:
    #     best_loss = curr_loss
    #     scipy.io.savemat(f'{main_path}/best_mat.mat', {'pred': pred, 'test_stocks_data': test_stocks_data, 'loss': loss})
    #     model.save_weights(f'{main_path}/model.h5',save_format="h5")
