import os
os.environ['CUDA_VISIBLE_DEVICES'] = '-1'

import scipy.io,scipy.special
import pandas_market_calendars as mcal
from datetime import datetime,timedelta
import tensorflow as tf
from tensorflow import keras
from data2net.data2net import QueryHandler
import numpy as np
import pandas as pd,time
from tensorflow.keras.utils import to_categorical
from collections import OrderedDict
from broker.broker_runner import BrokerRunner
import ib_insync,copy

def add_jobs(qhandler,main_path,tickers,dates,table_name):
        for date in dates:
            date = date.date()
            for ticker in tickers:
                    qstr = f"""
                    SELECT distinct * from 
                    (select * from `{table_name}`  where ticker = '{ticker}' and extract(date from date)='{date}' order by date ) as a
                    join 
                    (
                    (
                    SELECT close as last_close ,date as last_date FROM `{table_name}`  where ticker = '{ticker}' and extract(date from date)<'{date}' order by date desc limit 1
                    ) as b
                    join 
                    (
                    SELECT open as next_open ,date as next_date FROM `{table_name}`  where ticker = '{ticker}' and extract(date from date)>'{date}' order by date asc limit 1
                    ) as c
                    on True
                    )
                    on True
                    """

                    qhandler.append_qstr_path( qstr=qstr, path=f'{main_path}/{ticker}_{date}.csv', date=date, ticker=ticker, data_name='data_name')


qhandler = QueryHandler(replace_dates=[],asset='STOCK',live=True)

def create_stock_data(main_path,data_dir):
    NMINUTES = 390
    tickers = list(sorted(set([f.split('_')[0] for f in os.listdir(data_dir)])))
    dates   = list(sorted(set([f.split('_')[1].split('.')[0] for f in os.listdir(data_dir)])))
    fields = ['open','close','high','low']
    stocks_data = np.zeros((len(tickers),len(dates),NMINUTES,len(fields)))
    for it,t in enumerate(tickers):
        for id,d in enumerate(dates):
            if os.path.exists(f'{data_dir}/{t}_{d}.csv'):
                df = pd.read_csv(f'{data_dir}/{t}_{d}.csv')
                df = df.sort_values(by='date')

                for i,f in enumerate(fields):
                    v = df[f].values/df.open.values[0]-1
                    stocks_data[it,id,0:v.size,i] = v
    np.save(f'{main_path}/stocks_data.npy',{'stocksdata':stocks_data,'tickers':tickers})
    return {'stocksdata': stocks_data, 'tickers': tickers}

# def download_all(tickers,start_year = 2019):
#     start_date = datetime(start_year,1,1)
#     end_date = datetime.now()
#     table_name = 'KIRA.intraday_1min_full_clean'
#     nyse = mcal.get_calendar('NYSE')
#     dates = nyse.schedule(start_date, end_date).market_open.to_list()
#     os.makedirs(f'{main_path}/data',exist_ok=True)
#     add_jobs(qhandler,f'{main_path}/data',tickers,dates,table_name)
#     print('qhandler')
#     qhandler.max_threads = 20
#     qhandler.finalize()
#     create_stock_data(main_path, f'{main_path}/data')

# download_all(tickers=pd.read_csv(f'{main_path}/tickers.csv').ticker.tolist(),start_year=2019)


data_options = np.array([-1,0,1])
bce_keras = tf.keras.losses.CategoricalCrossentropy()

def bce(y_true,y_pred):
    oh = tf.argmin(abs(tf.expand_dims(tf.sign(y_true),-1)-data_options.reshape(1,1,1,-1)),-1)
    oh = tf.one_hot(oh,data_options.size)
    return bce_keras(oh,y_pred,sample_weight=tf.where(y_true!=0,1,0))

def profit(y_true,y_pred):

    sgn_do = np.sign(data_options.reshape(1,1,1,-1))
    y_pred = tf.reduce_sum(y_pred * sgn_do, -1)
    prof = tf.reduce_sum(y_pred * y_true,-1)
    pp = 100*tf.reduce_mean(prof,0)
    pp = tf.clip_by_value(pp,-100,5)
    # sharpe = tf.reduce_mean(pp)/(tf.math.reduce_std(tf.where(pp>0,0,pp))+1e-3)
    return -tf.reduce_mean(pp)
    # return -sharpe

# def get_predictions_serial(test_input,layers,loss):
#     def step(lstm_input, states):
#         return layers['lstm'].cell(lstm_input,states)
#
#     ndays = tf.shape(test_input)[1]
#
#     test_input = tf.reshape(test_input, [-1, test_input.shape[2],test_input.shape[3]])
#
#     out = test_input
#     for l in layers['pre_dense']:
#         out = l(out)
#
#     # out_data_op = data_options.reshape(-1,1)
#     out_data_op = tf.eye(data_options.size)
#     for l in layers['pre_dense']:
#         out_data_op = l(out_data_op)
#
#     states = layers['lstm'].get_initial_state(out[:,0,:]*0)
#     outs_lstm = []
#     ons = tf.ones([tf.shape(out)[0],out_data_op.shape[0],1])
#     flat_data_op = tf.expand_dims(out_data_op, 0)*ons
#     flat_data_op = tf.reshape(flat_data_op,[-1,flat_data_op.shape[-1]])
#     for kmin in range(test_input.shape[1]):
#         flat_states = [tf.reshape(tf.expand_dims(s, 1)*ons,[-1,s.shape[-1]]) for s in states]
#         outs_lstm.append(tf.expand_dims(step(flat_data_op,flat_states)[0],1))
#         states = step(out[:,kmin,:], states)[1]
#
#     outs_lstm = tf.concat(outs_lstm, 1)
#     outs_lstm =  tf.reshape(outs_lstm,[-1,ons.shape[1],outs_lstm.shape[1],outs_lstm.shape[2]])
#     for l in layers['out_denses']:
#         outs_lstm = l(outs_lstm)
#
#     outs_lstm = tf.transpose(tf.squeeze(outs_lstm, -1), [0, 2, 1])
#
#     if loss =='bce':
#         outs_lstm = tf.nn.softmax(outs_lstm, -1)
#         outs_lstm = tf.reshape(outs_lstm, [-1,ndays] + [s for s in outs_lstm.shape[1::]])
#
#     return outs_lstm

# def get_predictions(test_input,time_index,layers,loss):
#     ndays = tf.shape(test_input)[1]
#     time_index = tf.reshape(time_index,[-1,time_index.shape[-1]])
#
#     outs_lstm = get_predictions_serial(test_input,layers,loss)
#     outs_lstm = tf.reduce_sum(tf.expand_dims(tf.one_hot(time_index, 391),-1)*tf.expand_dims(outs_lstm,2),1)
#     outs_lstm = outs_lstm[:,0:390,:]
#
#     mn = tf.stop_gradient(tf.reduce_min(outs_lstm))
#     outs_lstm = tf.where(outs_lstm==0,mn-1000.0,outs_lstm)
#
#     outs_lstm = tf.reshape(outs_lstm,[-1,ndays,outs_lstm.shape[1],outs_lstm.shape[2]])
#     return outs_lstm

# def build_model(NMINUTES,loss):
#     sz = 10
#     inputs = keras.Input(shape=(None,NMINUTES,data_options.size),dtype='float32')
#
#     pre_dense = [keras.layers.Dense(sz, activation='elu',name=f'dense_in_{k}') for k in range(3)]
#     lstm  = keras.layers.LSTM(sz, return_sequences=True,name='lstm')
#     out_denses = [keras.layers.Dense(sz, activation='elu',name=f'dense_out_{k}') for k in range(2)] + [keras.layers.Dense(1,name=f'dense_out_final')]
#     layers = {'pre_dense':pre_dense,'lstm':lstm,'out_denses':out_denses}
#     if loss=='bce':
#         loss_fcn = profit
#         output = get_predictions_serial(inputs,layers, loss=loss)
#         model = keras.Model([inputs], output)
#
#     else:
#         assert False
#
#     model.compile(optimizer="adam", loss=loss_fcn)
#
#     return model,layers

def get_predictions_serial(test_input,layers,loss):
    out_data_op = layers['conv'](tf.concat([test_input * 0, test_input], 2))[:, :, 0:-1]
    for l in layers['dense']:
        out_data_op = l(out_data_op)
    return out_data_op

def build_model(NMINUTES,loss):
    inputs = keras.Input(shape=(None,NMINUTES,data_options.size),dtype='float32')
    sz = 100
    dense = [keras.layers.Dense(sz, activation='elu',name=f'dense1'),keras.layers.Dense(sz, activation='elu',name=f'dense2')] + [keras.layers.Dense(data_options.size,name=f'dense_out_final',activation='softmax')]
    conv = tf.keras.layers.Conv1D(filters=2*sz,kernel_size=(NMINUTES,))
    layers = {'conv':conv,'dense':dense}
    if loss=='bce':
        loss_fcn = profit
        output = get_predictions_serial(inputs,layers, loss=loss)
        model = keras.Model([inputs], output)

    else:
        assert False

    model.compile(optimizer="adam", loss=loss_fcn)

    return model,layers


def transform_data(stocks_data,IX_test=-100):
    new_stocks_data = stocks_data[:,:,0:60,0]*0
    time_index = stocks_data[:,:,0:60,0]*0+390
    in_counter = 0
    res = stocks_data[:,:,0,0]*0 + 0.5/100
    tmp = (stocks_data[:,:,:,2]-stocks_data[:,:,:,3])
    for k1 in range(tmp.shape[0]):
        for k2 in range(tmp.shape[1]-1):
            tt = tmp[k1,k2,tmp[k1,k2]!=0]
            if tt.size>0:
                res[k1,k2+1] = np.percentile(tt,99)*2


    for k1 in range(stocks_data.shape[0]):
        for k2 in range(stocks_data.shape[1]):
            tmp = stocks_data[k1,k2]
            curr_v = 0
            kstart = 0

            curr_vs  = []
            c_time_index = []

            for k in range(tmp.shape[0]):
                counter = 0
                tmp_curr_vs = []
                tmp_time_index = []
                open_price = tmp[k, 0]
                close_price = tmp[k, 1]
                high_price = tmp[k, 2]
                low_price = tmp[k, 3]
                prices = [open_price]
                p_change = False
                if (abs(open_price-high_price) + abs(close_price-low_price))<=(abs(open_price-low_price) + abs(close_price-high_price)):
                    prices+=[high_price,low_price,close_price]
                else:
                    prices+=[low_price,high_price,close_price]

                for price in prices:
                    while abs(curr_v - price) >= res[k1,k2]:
                        counter += 1
                        sgn = np.sign(price - curr_v)
                        curr_v += res[k1,k2] * sgn
                        tmp_curr_vs.append(res[k1,k2] * sgn)
                        tmp_time_index.append(kstart)
                        p_change = True

                if p_change:
                    kstart = k + 1

                if counter>1:
                    in_counter+=1
                    tmp_curr_vs = tmp_curr_vs[0:1]
                    tmp_time_index = tmp_time_index[0:1]

                curr_vs+=tmp_curr_vs
                c_time_index+=tmp_time_index

            curr_vs.append(close_price - curr_v)
            c_time_index.append(tmp.shape[0]-1)

            assert len(curr_vs)==len(c_time_index)
            curr_vs = curr_vs[0:new_stocks_data.shape[2]]
            c_time_index= c_time_index[0:new_stocks_data.shape[2]]
            new_stocks_data[k1,k2,0:len(curr_vs)] = np.array(curr_vs)
            time_index[k1, k2, 0:len(curr_vs)] = np.array(c_time_index)

    real_stock_data_train = np.expand_dims(stocks_data[:,0:IX_test],-1)
    train_stocks_data = np.expand_dims(new_stocks_data[:,0:IX_test],-1)
    time_index_train = time_index[:,0:IX_test]
    res_train = res[:,0:IX_test]

    test_stocks_data = np.expand_dims(new_stocks_data[:,IX_test::],-1)
    real_stock_data = np.expand_dims(stocks_data[:,IX_test::],-1)
    time_index_test = time_index[:,IX_test::]
    res_test = res[:,IX_test::]

    shape_test_stocks_data = list(test_stocks_data.shape)

    a = abs(train_stocks_data-np.expand_dims(np.expand_dims(res_train,-1),-1)*data_options.reshape(1,1,1,-1)).argmin(-1)
    ons_like = to_categorical(a,num_classes = data_options.size)*(train_stocks_data!=0)

    a = abs(test_stocks_data-np.expand_dims(np.expand_dims(res_test,-1),-1)*data_options.reshape(1,1,1,-1)).argmin(-1)
    ons_like_test = to_categorical(a,num_classes = data_options.size)*(test_stocks_data!=0)



    train_data = (tf.one_hot(time_index_train,390)*train_stocks_data).numpy().sum(2)
    test_data  = (tf.one_hot(time_index_test,390)*test_stocks_data).numpy().sum(2)

    return {'train_data':train_data,'test_data':test_data,'real_stock_data_train':real_stock_data_train,'real_stock_data':real_stock_data,
            'shape_test_stocks_data':shape_test_stocks_data,'ons_like':ons_like,'ons_like_test':ons_like_test,'time_index_train':time_index_train,
            'time_index_test':time_index_test,'train_stocks_data':train_stocks_data.squeeze(-1),'test_stocks_data':test_stocks_data.squeeze(-1),'res_test':res_test}

def transform_data_live(stocks_data,IX_test=-100,max_minute=390):
    new_stocks_data = stocks_data[:,:,0:60,0:2]*0
    acc_stocks_data = stocks_data[:,:,0:60,0:2]*0

    time_index = (stocks_data[:,:,0:60,0:2]*0+390).astype(int)
    in_counter = 0
    res = stocks_data[:,:,0,0]*0 + 0.5/100

    # tmp = (stocks_data[:,:,:,2]-stocks_data[:,:,:,3])
    # for k1 in range(tmp.shape[0]):
    #     for k2 in range(tmp.shape[1]-1):
    #         tt = tmp[k1,k2,tmp[k1,k2]!=0]
    #         if tt.size>0:
    #             res[k1,k2+1] = np.percentile(tt,99)*2
    #

    for k1 in range(stocks_data.shape[0]):
        for k2 in range(stocks_data.shape[1]):
            tmp = stocks_data[k1,k2]
            curr_v = 0
            kstart = 0

            curr_vs  = []
            curr_ac  = []
            c_time_index = []

            for k in range(min([tmp.shape[0],max_minute])):
                counter = 0
                tmp_curr_vs = []
                tmp_curr_ac = []
                tmp_time_index = []
                open_price = tmp[k, 0]
                close_price = tmp[k, 1]
                high_price = tmp[k, 2]
                low_price = tmp[k, 3]
                prices = [open_price]
                p_change = False
                if (abs(open_price-high_price) + abs(close_price-low_price))<=(abs(open_price-low_price) + abs(close_price-high_price)):
                    prices+=[high_price,low_price,close_price]
                else:
                    prices+=[low_price,high_price,close_price]

                for price in prices:
                    while abs(curr_v - price) >= res[k1,k2]:
                        counter += 1
                        sgn = np.sign(price - curr_v)
                        curr_v += res[k1,k2] * sgn
                        tmp_curr_ac.append(curr_v)
                        tmp_curr_vs.append(res[k1,k2] * sgn)
                        tmp_time_index.append(kstart)
                        p_change = True

                if p_change:
                    kstart = k + 1

                if counter>1:
                    in_counter+=1
                    tmp_curr_vs = tmp_curr_vs[0:1]
                    tmp_time_index = tmp_time_index[0:1]
                    tmp_curr_ac = tmp_curr_ac[-1::]
                curr_vs+=tmp_curr_vs
                curr_ac+= tmp_curr_ac
                c_time_index+=tmp_time_index

            # curr_vs.append(close_price - curr_v)
            # c_time_index.append(tmp.shape[0]-1)

            assert len(curr_vs)==len(c_time_index)
            if len(curr_vs)>(new_stocks_data.shape[2]-2):
                print(f'skipped {k1}')
                continue

            curr_vs = curr_vs[0:new_stocks_data.shape[2]]
            c_time_index= c_time_index[0:new_stocks_data.shape[2]]
            if len(curr_ac)==0:
                lac = 0
            else:
                lac = curr_ac[-1]
            acc_stocks_data[k1,k2,0:(len(curr_ac)+1),0] = np.array(curr_ac + [lac+res[k1,k2]])
            new_stocks_data[k1,k2,0:(len(curr_vs)+1),0] = np.array(curr_vs + [res[k1,k2]])
            time_index[k1, k2, 0:(len(curr_vs)+2),0] = np.array(c_time_index + [390,max_minute] )

            acc_stocks_data[k1,k2,0:(len(curr_ac)+1),1] = np.array(curr_ac + [lac-res[k1,k2]])
            new_stocks_data[k1,k2,0:(len(curr_vs)+1),1] = np.array(curr_vs + [-res[k1,k2]])
            time_index[k1, k2, 0:(len(curr_vs)+2),1] = np.array(c_time_index + [390,max_minute] )

    new_stocks_data = np.concatenate([new_stocks_data[:, :, :, 0], new_stocks_data[:, :, :, 1]], 0)
    acc_stocks_data = np.concatenate([acc_stocks_data[:, :, :, 0], acc_stocks_data[:, :, :, 1]], 0)

    time_index = np.concatenate([time_index[:, :, :, 0], time_index[:, :, :, 1]], 0)
    res = np.concatenate([res, res], 0)

    test_stocks_data = np.expand_dims(new_stocks_data[:,IX_test::],-1)
    real_stock_data = np.expand_dims(stocks_data[:,IX_test::],-1)
    time_index_test = time_index[:,IX_test::]
    acc_stocks_data_test = acc_stocks_data[:,IX_test::]
    res_test = res[:,IX_test::]

    shape_test_stocks_data = list(test_stocks_data.shape)

    a = abs(test_stocks_data-np.expand_dims(np.expand_dims(res_test,-1),-1)*data_options.reshape(1,1,1,-1)).argmin(-1)
    ons_like_test = to_categorical(a,num_classes = data_options.size)*(test_stocks_data!=0)

    test_data  = (tf.one_hot(time_index_test,390)*test_stocks_data).numpy().sum(2)

    return {'test_data':test_data,'real_stock_data':real_stock_data,'shape_test_stocks_data':shape_test_stocks_data,'ons_like_test':ons_like_test,
            'time_index_test':time_index_test,'test_stocks_data':test_stocks_data,'res_test':res_test,
            'acc_stocks_data_test':acc_stocks_data_test}

def train(main_path,loss_name='bce'):
    stocks_data_dict = np.load(f'{main_path}/stocks_data.npy', allow_pickle=True).item()
    data_dict = transform_data(stocks_data_dict['stocksdata'])

    model,layers = build_model(data_dict['train_stocks_data'].shape[2],loss=loss_name)
    optimizer = keras.optimizers.Adam()

    best_loss = 1e8
    loss = []
    val_loss = []
    loss_fcn = profit
    start_time = time.time()

    for epoch in range(10000):
        if loss_name == 'bce':
            for k in range(5):
                with tf.GradientTape() as tape:
                    logits = get_predictions_serial(data_dict['ons_like'],layers,loss_name)
                    loss_value = loss_fcn(data_dict['train_stocks_data'], logits)

                grads = tape.gradient(loss_value, model.trainable_weights)
                optimizer.apply_gradients(zip(grads, model.trainable_weights))
                loss.append(float(loss_value))

            pred = get_predictions_serial(data_dict['ons_like_test'],layers,loss_name).numpy()#model.predict(data_dict['ons_like_test'])
            curr_loss = float(loss_fcn(data_dict['test_stocks_data'], pred))

        else:
            hist = model.fit([data_dict['ons_like'], data_dict['time_index_train']], data_dict['train_data'], epochs=5)
            loss += hist.history['loss']
            pred = model.predict([data_dict['ons_like_test'],data_dict['time_index_test']])
            curr_loss = model.evaluate([data_dict['ons_like_test'],data_dict['time_index_test']], data_dict['test_data'])

        val_loss.append(curr_loss)

        mat_dict = {'res_test':data_dict['res_test'],'time_index_test':data_dict['time_index_test'],'tickers': stocks_data_dict['tickers'],
                    'options': data_options, 'real_stock_data': data_dict['real_stock_data'],'ons_like_test':data_dict['ons_like_test'],
                    'pred': pred,'test_stocks_data': data_dict['test_stocks_data'],'test_data':data_dict['test_data'],'loss': loss,'val_loss':val_loss}
        scipy.io.savemat(f'{main_path}/save_mat.mat',mat_dict)
        print(f'saving mats... time elapsed {int(time.time() - start_time)} test loss {curr_loss} best loss {best_loss}')
        # model.save_weights(f'{main_path}/last_checkpoint.h5', )

        if curr_loss<=best_loss:
            best_loss = curr_loss
            scipy.io.savemat(f'{main_path}/best_mat.mat',mat_dict)
            model.save_weights(f'{main_path}/checkpoint.h5',)

def live(tickers,main_path,NMINUTES=60,aum=1000,loss_name = 'bce',curr_date = datetime.now()):
    is_live = curr_date.date() == datetime.now().date()
    if len(mcal.get_calendar('NYSE').schedule(curr_date, curr_date, 'UTC').market_close)==0:
        return
    close_time = mcal.get_calendar('NYSE').schedule(curr_date, curr_date, 'UTC').market_close[0] - timedelta(minutes=15)

    broker_runner = BrokerRunner(clientId=1375)
    curr_main_path = f'{main_path}/{curr_date.date().isoformat()}/'
    curr_data_path = f'{curr_main_path}/data'
    curr_mats_path = f'{curr_main_path}/mats'
    os.makedirs(curr_mats_path,exist_ok=True)
    os.makedirs(curr_data_path,exist_ok=True)
    last_date = None
    for ticker in tickers:
        if is_live:
            tick_data = broker_runner.ib.reqHistoricalData(BrokerRunner.get_contract(ticker), '', "5 D", "1 min", "TRADES", useRTH=1)
        else:
            tick_data = broker_runner.ib.reqHistoricalData(BrokerRunner.get_contract(ticker), curr_date, "5 D", "1 min", "TRADES", useRTH=1)
        broker_runner.ib.sleep(0)
        tmp = [p.__dict__ for p in tick_data]
        [t.update({'ticker': ticker}) for t in tmp]
        tmp = pd.DataFrame(tmp)
        tmp_last_date = max(tmp.date.dt.date[(tmp.date.dt.date < curr_date.date())])
        if last_date is None:
            last_date = tmp_last_date
        else:
            assert last_date==tmp_last_date

        tmp[tmp.date.dt.date==last_date].to_csv(f'{curr_data_path}/{ticker}_{last_date}.csv')


    model, layers = build_model(NMINUTES, loss=loss_name)
    model.load_weights(f'{main_path}/last_checkpoint.h5', by_name=True)


    if is_live:
        for ticker in tickers:
            broker_runner.ib.reqHistoricalData(BrokerRunner.get_contract(ticker), '', "2 D", "1 min", "TRADES", useRTH=1, keepUpToDate=True)
            broker_runner.ib.sleep(0)

    last_minute = 0
    current_orders   = OrderedDict()
    open_price = {}
    while (datetime.utcnow().time()<=close_time.time()):
        broker_runner.ib.sleep(0)

        periodic_data_bars = OrderedDict()
        if is_live:
            lengths = []
            for ticker, bars in zip(tickers, broker_runner.ib.realtimeBars()):
                periodic_data_bars[ticker] = [b for b in bars if b.date.date() == curr_date.date()]
                lengths.append(len(periodic_data_bars[ticker]))
        else:
            lengths = []
            for ticker in tickers:
                periodic_data_bars[ticker] = [b for b in broker_runner.ib.reqHistoricalData(BrokerRunner.get_contract(ticker), curr_date + timedelta(1), "2 D", "1 min", "TRADES", useRTH=1) if b.date.date() == curr_date.date()]
                lengths.append(len(periodic_data_bars[ticker]))

        if all([l==lengths[0] for l in lengths]) and (lengths[0]>last_minute):
            last_minute = lengths[0]
            for ticker,tick_data in periodic_data_bars.items():
                tmp = [p.__dict__ for p in tick_data]
                open_price[ticker] = min(tmp, key=lambda x: x['date'])['open']
                [t.update({'ticker': ticker}) for t in tmp]
                tmp = pd.DataFrame(tmp)
                tmp[tmp.date.dt.date == curr_date.date()].to_csv(f'{curr_data_path}/{ticker}_{curr_date.date()}.csv')

            stocks_data_dict = create_stock_data(curr_mats_path, curr_data_path)
            data_dict = transform_data_live(stocks_data_dict['stocksdata'], IX_test=-1, max_minute=last_minute)
            prediction = pred = get_predictions_serial(data_dict['ons_like_test'], layers, loss_name).numpy()
            acc_stocks_data = data_dict['acc_stocks_data_test'].squeeze(1)
            pred = pred.squeeze(1)
            tp = []
            acc = []
            tsd = data_dict['test_stocks_data'].squeeze()
            for k in range(pred.shape[0]):
                ilast = np.where(tsd[k]!=0)[0]
                if ilast.size == 0:
                    print(f'skipped {k}')
                    tp.append(pred[k:(k + 1), 0] * 0)
                    acc.append(acc_stocks_data[k:(k + 1), 0] * 0)

                elif ilast[-1]>=(pred.shape[1]-1):
                    print(f'skipped {k}')
                    tp.append(pred[k:(k + 1), 0] * 0 )
                    acc.append(acc_stocks_data[k:(k + 1), 0] * 0)
                else:
                    ilast  = ilast[-1]
                    tp.append(pred[k:(k + 1), ilast+1])
                    acc.append(acc_stocks_data[k:(k + 1), ilast])

            tp = np.concatenate(tp, 0)
            acc = np.concatenate(acc, 0)
            acc = np.concatenate([np.expand_dims(acc[0:int(acc.shape[0]/2)],-1),np.expand_dims(acc[int(acc.shape[0]/2)::],-1)],-1)

            pred = np.concatenate([np.expand_dims(tp[0:int(tp.shape[0]/2)],-1),np.expand_dims(tp[int(tp.shape[0]/2)::],-1)],-1)
            pred = (pred[:,data_options>0,:]-pred[:,data_options<0,:]).squeeze()


            mat_dict = {'ons_like_test':data_dict['ons_like_test'],'res_test': data_dict['res_test'], 'time_index_test': data_dict['time_index_test'], 'tickers': stocks_data_dict['tickers'],
                        'options': data_options, 'real_stock_data': data_dict['real_stock_data'],'prediction':prediction,
                        'pred': pred, 'test_stocks_data': data_dict['test_stocks_data'], 'test_data': data_dict['test_data']}
            scipy.io.savemat(f'{curr_mats_path}/{last_minute}.mat',mat_dict)

            if not is_live:
                return

            broker_runner.ib.sleep(1)
            current_position = broker_runner.ib.positions()
            current_position = {c.contract.symbol: c.position for c in current_position}
            open_orders = broker_runner.ib.reqAllOpenOrders()
            broker_runner.ib.sleep(1)
            for order in open_orders:
                broker_runner.ib.sleep(0)
                broker_runner.ib.cancelOrder(order)
                broker_runner.ib.sleep(0)
            broker_runner.ib.sleep(2)
            for iticker,ticker in enumerate(stocks_data_dict['tickers']):
                if ticker not in current_position:
                    current_position[ticker] = 0

                price_up   = np.round_((1+acc[iticker,0]) * open_price[ticker],2)
                price_down = np.round_((1+acc[iticker,1]) * open_price[ticker],2)


                orders = []
                if (pred[iticker]==0).all():
                    totalQuantity =  - current_position[ticker]
                    action = 'BUY' if totalQuantity>0 else 'SELL'
                    totalQuantity = abs(totalQuantity)

                    mit = ib_insync.MarketOrder(action=action, totalQuantity=totalQuantity)
                    if totalQuantity>0:
                        orders.append(mit)

                if pred[iticker,0]>0:
                    price = price_up
                    totalQuantity = np.round(aum/price) - current_position[ticker]

                    action = 'BUY' if totalQuantity>0 else 'SELL'
                    totalQuantity = abs(totalQuantity)
                    mit = ib_insync.StopOrder(action=action, totalQuantity=totalQuantity,stopPrice=price)
                    # mit.OrderType = "MIT"
                    # mit.AuxPrice = price
                    if totalQuantity>0:
                        orders.append(mit)

                if pred[iticker,0]<0:
                    price = price_up
                    totalQuantity = -np.round(aum/price) - current_position[ticker]
                    action = 'BUY' if totalQuantity>0 else 'SELL'
                    totalQuantity = abs(totalQuantity)

                    lmt = ib_insync.LimitOrder(action, totalQuantity, lmtPrice=price)
                    if totalQuantity>0:
                        orders.append(lmt)

                if pred[iticker,1]<0:
                    price = price_down
                    totalQuantity = -np.round(aum/price) - current_position[ticker]
                    action = 'BUY' if totalQuantity>0 else 'SELL'
                    totalQuantity = abs(totalQuantity)

                    mit = ib_insync.StopOrder(action=action, totalQuantity=totalQuantity,stopPrice=price)
                    if totalQuantity>0:
                        orders.append(mit)

                if pred[iticker,0]>0:
                    price = price_down
                    totalQuantity = np.round(aum/price) - current_position[ticker]
                    action = 'BUY' if totalQuantity>0 else 'SELL'
                    totalQuantity = abs(totalQuantity)
                    lmt = ib_insync.LimitOrder(action, totalQuantity, lmtPrice=price)
                    if totalQuantity>0:
                        orders.append(lmt)

                if len(orders)==0:
                    continue

                for o in orders:
                    o.ocaType = 2
                    o.ocaGroup = f'{ticker}_{last_minute}'

                print(f'{ticker}: curr : {open_price[ticker]} ...  {price_down},{price_up} ')
                current_orders[ticker] = broker_runner.place_orders(contracts=[BrokerRunner.get_contract(ticker) for _ in orders],orders=orders)
                broker_runner.ib.sleep(0)
                # broker_runner.place_orders(contracts= contracts,orders=orders)


    ########################### end of day close ###########################
    current_position = broker_runner.ib.positions()
    broker_runner.ib.sleep(1)
    current_position = {c.contract.symbol: c.position for c in current_position}
    for ticker, trade in current_orders.items():
        try:
            broker_runner.ib.sleep(0)
            broker_runner.ib.cancelOrder(trade.order)
            broker_runner.ib.sleep(0)
        except:
            pass

    for ticker,totalQuantity in current_position.items():
        action = 'BUY' if totalQuantity < 0 else 'SELL' # opposite
        totalQuantity = abs(totalQuantity)
        mkt = ib_insync.MarketOrder(action=action, totalQuantity=totalQuantity)
        broker_runner.place_orders(contracts=[BrokerRunner.get_contract(ticker)], orders=[mkt])
        broker_runner.ib.sleep(0)


def predict_checkpoint(main_path):
    stocks_data_dict = np.load(f'{main_path}/stocks_data.npy', allow_pickle=True).item()
    data_dict = transform_data(stocks_data_dict['stocksdata'])

    model,layers = build_model(data_dict['train_stocks_data'].shape[2])
    model.load_weights(f'{main_path}/model.h5')

    pred = model.predict([data_dict['ons_like_test'], data_dict['time_index_test']])
    mat_dict = {'res_test': data_dict['res_test'], 'time_index_test': data_dict['time_index_test'], 'tickers': stocks_data_dict['tickers'],
                'options': data_options, 'real_stock_data': data_dict['real_stock_data'],
                'pred': pred, 'test_stocks_data': data_dict['test_stocks_data'], 'test_data': data_dict['test_data']}
    scipy.io.savemat(f'{main_path}/predictions.mat', mat_dict)


if __name__=='__main__':
    small = True
    if small:
        main_path = fr'D:/compare_to_random_improved_res'
    else:
        main_path = fr'D:/compare_to_random_improved_res_large'

    if small:
        data_dir = fr'D:\temp_test_23_new\data\stock_data'
    else:
        data_dir = fr'D:\compare_to_random_improved_large\data'

    #     create_stock_data(main_path, f'{main_path}/data')
    os.makedirs(main_path, exist_ok=True)

    train(main_path)
    # predict(main_path)
