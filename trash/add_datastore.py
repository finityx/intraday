import firebase_admin
from firebase_admin import credentials
from firebase_admin import firestore
from google.cloud import bigquery
import pandas as pd
import os
import numpy as np
from google.cloud import storage
client = bigquery.Client()
TABLE_NAME = f'checked_data.2021-09-30_merged_stocks'

import datetime
# Use the application default credentials
cred = credentials.Certificate(r"C:\Users\admin\Downloads\finityx-app-3d85502cd5c5.json")
firebase_admin.initialize_app(cred)

db = firestore.client()

'yahoo_unadjusted_close', 'yahoo_unadjusted_open',
'yahoo_unadjusted_high', 'yahoo_unadjusted_low',
'yahoo_unadjusted_volume', 'yahoo_adjustment_factor',

def get_ticker_data(table,ticker):
    res = client.query(f"""select ticker, date,yahoo_unadjusted_close*yahoo_adjustment_factor as close,yahoo_unadjusted_open*yahoo_adjustment_factor as open,
yahoo_unadjusted_high*yahoo_adjustment_factor as high,yahoo_unadjusted_low*yahoo_adjustment_factor as low,yahoo_unadjusted_volume*yahoo_unadjusted_close as turnover 
from (select distinct * from `{table}` where ticker='{ticker}')""").result().to_dataframe()
    return res



tickers=['AAPL','MSFT','SPY','AMZN','TSLA','GOOG']
for tick in tickers:
    tick_data = get_ticker_data(TABLE_NAME, tick)
    for gname,gg in tick_data.groupby(['ticker',tick_data.date.dt.year]):

        data = {}
        for k in gg.keys():
            if k=='ticker':
                continue
            if k=='date':
                data[k] = [d.isoformat() for d in gg[k].dt.date.to_list()]
                continue
            data[k] = gg[k].values.tolist()
        doc_ref = db.collection(u'stocks_daily').document(f'{gname[0]},{gname[1]}')
        doc_ref.set(data)
  # doc_ref = db.collection(u'stocks_daily').document(tick)
  # col=tick_data.columns
  # col=col.to_list()
  # col.remove('ticker')
  # doc_ref.set({t:tick_data[t].to_list()[-400::] for t in col})
  #

