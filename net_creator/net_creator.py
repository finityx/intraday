from tensorflow.keras.layers import Dense, LSTM, Input
from collections import OrderedDict
from tensorflow.keras.layers import Dense
from tensorflow import keras
import tensorflow as tf
import dill,copy

class NetCreator():
    def __init__(self,data2net,core_architecture,name,batch_size):
        self.name = name
        self.data2net = data2net
        self.batch_size = batch_size
        self.net_inputs = OrderedDict()
        self.core_architecture = core_architecture
        self.build_net_inputs()

    def calc_out_size_for_objective(self,objective):
        return 1

    def build_net_inputs(self):
        self.net_inputs = OrderedDict()
        for source in self.data2net.data_sources:
            self.net_inputs[source.name] = Input([len(self.data2net.tickers)*2,self.data2net.ndays,self.data2net.n_minutes_in_day,  len(source.params)],name=source.name,batch_size=self.batch_size)

    def build(self):
        assert False, "not implemented"

    def branch_to_objective(self,out):
        out_net = OrderedDict()
        losses_fcn  = OrderedDict()
        for obj in self.data2net.objectives:
            out_net[obj.name]     = Dense(self.calc_out_size_for_objective(obj),name=f'branch_to_objective_dense_{obj.name}')(out)
            out_net[obj.name]   =  tf.reshape(out_net[obj.name] ,[out_net[obj.name].shape[0],out_net[obj.name].shape[1],out_net[obj.name].shape[2],len(self.data2net.strategies_params),-1])
            out_net[obj.name]   = tf.transpose(out_net[obj.name],[0,1,3,2,4])
            losses_fcn[obj.name]  = obj.loss.calc_loss[self.name]

        model = keras.models.Model(inputs=self.net_inputs, outputs=list(out_net.values()))
        model.output_names = list(out_net.keys())
        # model.compile(loss=losses_fcn, optimizer='adam')
        self.optimizer = keras.optimizers.Adam()
        return model

    def concat_net_inputs(self):
        out = tf.concat([v for k, v in self.net_inputs.items()], -1)
        return out

