from net_creator import net_creator
import tensorflow as tf

class Army(net_creator.NetCreator):
    def __init__(self,data2net,core_architecture,batch_size=1):
        super().__init__(data2net,core_architecture,'Army',batch_size=batch_size)

    def calc_out_size_for_objective(self,objective):
        return 3*len(self.data2net.strategies_params)

    def build(self):
        self.core_architecture.build(self)
        concat_net_inputs =  self.concat_net_inputs()
        out=self.core_architecture.call(concat_net_inputs)
        return self.branch_to_objective(out)
