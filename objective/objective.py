import os

class Objective:
    def __init__(self,name,label):
        self.name = name

        self.comis_per_stock = 0.003
        self.min_comis = 0.5
        self.max_concentration = 1
        self.max_invest_to_ratio = 0.05
        self.aum = 1e5
        self.loss  = None
        self.label = label
        self.mode = 'open2close'
        self.spread = 0.01/100
        self.min_invest = 0.5
        self.max_invest_minute = 1

    def to_dict(self):
        obj = {f: self.__dict__[f] for f in ['name','comis_per_stock','spread','min_comis','max_concentration','max_invest_to_ratio','min_invest','max_invest_minute','aum','mode']}
        obj['loss'] = self.loss.name
        return obj

    def get_data_sources(self):
        return [self.label]

