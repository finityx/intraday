import json
import os
import service.selector as selector
from datetime import date, datetime
from finityx.common.utils.kafka_client import StreamTopic, Kafka
from com.finityx.protobuf.metrics.metrics_pb2 import MetricsData
from service.globals import GLOBALS
import hashlib
import tensorflow as tf
import time
import logging.config
import git
import finityx.common.utils.storage as storage
import sys

logging.config.fileConfig('logging.conf')
logger = logging.getLogger()

kafka: Kafka = None
entrance_timestamp = datetime.utcnow().isoformat()


def get_topic():
    return os.environ['TOPIC']


def get_metrics_topic():
    return os.environ['METRICS_TOPIC']


def init_kafka():
    stream_topics = [StreamTopic(get_topic())]
    global kafka
    kafka = Kafka(stream_topics, 'intraday')


def send_metric(key: str, data: str, ex: Exception = None):
    json_data = json.loads(data)
    timestamp: str = datetime.utcnow().isoformat()
    model_name = 'UNKNOWN'
    if json_data.get('spec') is not None and json_data.get('spec').get('modelName') is not None:
        model_name = json_data['spec']['modelName']
    metric = MetricsData(appName='intraday|model',
                         id=model_name,
                         entranceTimestamp=entrance_timestamp,
                         key=key,
                         value=str(ex) if ex else '',
                         data=data,
                         type='STRING',
                         timestamp=timestamp)
    logger.info(f'{key} metric sent')
    kafka.send(get_metrics_topic(), metric.SerializeToString())


def mode_listening():
    init_kafka()
    logger.info('LISTENING...')
    while True:
        for message in kafka.consumer:
            value = message.value.decode("utf-8")
            logger.info(f'GOT NEW MSG: {value}')
            try:
                process(value)
            except Exception:
                pass


def mode_storage():
    init_kafka()
    path = os.getenv('STORAGE_PATH')
    storage.download_file(storage.get_bucket_name(path), storage.get_bucket_blob(path), GLOBALS.msg_file_path)
    value = open(GLOBALS.msg_file_path, "r").read()
    logger.info(f'GOT MSG path: {path} value: {value}')
    process(value)


def process(value):
    send_metric('INCOMING', value)
    try:
        selector.main(value)
        send_metric('DONE', value)
    except Exception as ex:
        logger.exception(f"Processing model failed {value}")
        send_metric('FAILED', value, ex)
        raise ex


def mode_provided_value():
    value = get_value()
    with(open(GLOBALS.msg_file_path, 'w')) as file:
        file.write(value)
    selector.main(value)


def mode_provided_value_with_metrics():
    init_kafka()
    value = get_value()
    with(open(GLOBALS.msg_file_path, 'w')) as file:
        file.write(value)
    process(value)


def get_value():
    return os.getenv('VALUE')


def mode_sending():
    init_kafka()
    logger.info('SENDING...')
    # kafka.send(get_topic(), '{"alg_type": "run_commit", "COMMIT": "a2be9197e452e7248f5c2c4be5e0113f455d1b89", "use_latest_on_script_path": true, "script_path": "production_scripts/run_prediction_for_checkpoint.py", "script_params": {"check_point_bucket_name": "free_market_live_trading", "check_point_prefix": "amos_close2open_10000000_IterativeAllocation", "chosen_objective": "amos", "trading_aum": 10000000.0}, "spec": {"id": "model-run-0-test", "tradingEnv": "live"}}')
    kafka.send(get_topic(),
               '{"gui_dict": {"trading_mode": "close2close", "data_sets": ["event_data", "fundemental_quarterly_data", "index_data", "market_cap", "news_data", "sector_data", "stocks_data_1", "stocks_data_5", "stocks_data_21"], "data_sets_params": {"etf_weights_ticker": "SPY"}, "objectives": [{"name": "default", "main_dir": "", "time_interval_days": 1, "aum": 1000000.0, "limit_input": "NoLimit", "limit_action": "NoLimit", "loss": "profit", "comis_factor": 1.0, "max_concentration": 1.0, "max_inv": 1.0, "refrence": "None"}], "run_name": "default55"}, "tickers": ["None", "SPY", "IWM", "QQQ", "MSFT", "BAC", "GOOGL", "C", "JPM", "XLE", "XOM", "AMZN", "XLF", "WFC", "T", "INTC", "JNJ", "PFE", "GLD", "CSCO", "PG", "DIA", "GE", "VZ", "MRK", "BKNG", "ORCL", "GS", "QCOM", "KO", "IBM", "MCD", "XLI", "BIDU", "IYR", "CMCSA", "HD", "V", "DIS", "PEP", "NFLX", "PM", "F", "MS", "AMGN", "COP", "UNH", "BA", "RTX", "GILD", "BMY", "SLB", "AXP", "MO", "MDLZ", "OXY", "LOW", "TGT", "EBAY", "UPS", "USB", "SBUX", "UNP", "VLO", "MMM", "TXN", "XLY", "WBA", "XOP", "DE", "HAL", "XLU", "CRM", "FCX", "ABT", "XLB", "XLK", "EOG", "NKE", "XLV", "GM", "HON", "SPG", "ACN", "COF", "M", "XLP", "EMR", "COST", "BP", "MDT", "DUK", "XRT", "CMI", "FDX", "CL", "MU", "AIG", "EXC", "SO", "PRU", "TJX", "PBR", "KMB", "CSX", "HPQ", "AMT", "WYNN", "TRV", "MRO", "ETN", "NEM", "TSM", "LYB", "STT", "VIAC", "ANTM", "HYG", "AMAT", "GOLD", "GIS", "PXD", "YUM", "NSC", "MCK", "FITB", "GLW", "VALE", "DFS", "ALL", "EXPE", "STX", "WDC", "CCL", "BBY", "ITW", "AAL", "ADP", "ISRG", "BLK", "APA", "HES", "ITUB", "KR", "TT", "ADBE", "KSS", "HUM", "MGM", "ADM", "TFC", "TEVA", "DVN", "CB", "IP", "TMO", "DAL", "RIO", "VMW", "CI", "VNQ", "NVDA", "HST", "PPL", "GD", "GPS", "X", "NEE", "FE", "INTU", "COG", "AZO", "PPG", "VFC", "HIG", "KEY", "UAL", "RF", "LB", "PCG", "WY", "MYL", "NLOK", "SIRI", "CAH", "EQR", "GSK", "NOC", "XLNX", "WHR", "VTR", "ATVI", "WELL", "NBL", "PSA", "JCI", "NVS", "VRTX", "SYY", "SWK", "RL", "K", "BHP", "ABC", "BBD", "AVB", "AKAM", "NUE", "CAG", "ROST", "ADI", "NTAP", "SYK", "AON", "FFIV", "PEAK", "ED", "MAR", "SHW", "ECL", "SLV", "DOV", "MMC", "TROW", "AMP", "A", "APD", "ZBH", "JWN", "PEG", "MSI", "CF", "IWO", "OMC", "BXP", "EMN", "BEN", "EIX", "NLY", "LEN", "WM", "CCI", "GWW", "PGR", "DISCA", "JNPR", "ROK", "SPGI", "IVZ", "ORLY", "IJH", "PCAR", "CHRW", "UAA", "EQT", "ICE", "ALXN", "BUD", "HBAN", "PAYX", "MOS", "HFC", "DRI", "FAST", "EW", "BSX", "PVH", "ASML", "CERN", "ADS", "CMA", "SWKS", "ILMN", "FSLR", "TOT", "ETR", "AVGO", "HSY", "BBBY", "NOK", "CNP", "SAP", "CHKP", "MXIM", "MCHP", "MNST", "DHI", "VNO", "SRE", "CLR", "HSBC", "RCL", "EQIX", "NOV", "IPG", "DISH", "VRSN", "BWA", "CLX", "PHM", "UN", "AGNC", "MPC", "TEL", "XEL", "AZN", "XRX", "AAP", "NRG", "KMX", "IBN", "OKE", "MTB", "NI", "AES", "APH", "KDP", "TOL", "KRE", "EXPD", "AEO", "WSM", "DG", "CIEN", "XHB", "VOD", "SLG", "FL", "CMS", "NTRS", "ZION", "NWL", "HAS", "XEC", "OVV", "FTI", "SNY", "URI", "HLF", "VAR", "SMH", "TECK", "TMUS", "TCOM", "GPC", "FISV", "CE", "MRVL", "MUR", "RSG", "SBAC", "TAP", "HRB", "UNM", "CPB", "SJM", "IBB", "HP", "DTE", "DGX", "ROP", "RIG", "PRGO", "WEC", "RRC", "CCEP", "DISCK", "SWN", "REGN", "FIS", "TSCO", "ON", "ULTA", "HOLX", "URBN", "TPX", "PBCT", "PII", "IRM", "ETFC", "BX", "WAT", "ALB", "MELI", "IWB", "GT", "FTNT", "FRT", "JBHT", "HUN", "PWR", "RGLD", "STZ", "BLL", "L", "NDAQ", "EAT", "KBH", "KMI", "NYCB", "CPT", "ESS", "UL", "CIT", "LHX", "J", "STLD", "IYT", "HTZ", "GRMN", "SEE", "DECK", "RHI", "JBL", "AGCO", "NTES", "KBE", "UHS", "TER", "FLS", "AMG", "ALV", "LEA", "ASH", "NVO", "RE", "ABB", "CCK", "QRTEA", "NXPI", "UTHR", "BAP", "PNW", "IAC", "COO", "DRE", "CNX", "HSIC", "LBTYA", "FMX", "AIZ", "MLM", "XRAY", "CHD", "AIV", "LEG", "THC", "NUAN", "MHK", "CHL", "GL", "MKC", "SCCO", "SINA", "ANF", "MTD", "CLF", "MAN", "SRCL", "RMD", "FLR", "CDNS", "WYND", "AU", "AWK", "TSLA", "EFX", "VMC", "FNF", "NVR", "GPN", "EV", "CRUS", "FLEX", "MT", "RS", "R", "FDS", "CINF", "AVY", "RNR", "O", "ATI", "TS", "FHN", "DOX", "HBI", "GRA", "CS", "ARW", "AVT", "CAKE", "REG", "AXS", "VRSK", "ARE", "EDU", "NCR", "ALK", "CREE"], "alg_type": "gui"}')
    # kafka.send(get_topic(),
    #            '{"gui_dict": {"trading_mode": "close2close", "data_sets": ["event_data", "fundemental_quarterly_data", "index_data", "market_cap", "news_data", "sector_data", "stocks_data_1", "stocks_data_5", "stocks_data_21"], "data_sets_params": {"etf_weights_ticker": "SPY"}, "objectives": [{"name": "default", "main_dir": "", "time_interval_days": 1, "aum": 1000000.0, "limit_input": "NoLimit", "limit_action": "NoLimit", "loss": "profit", "comis_factor": 1.0, "max_concentration": 1.0, "max_inv": 1.0, "refrence": "None"}], "run_name": "default"}, "tickers": ["None", "SPY", "IWM", "QQQ", "MSFT", "BAC", "GOOGL", "C", "JPM", "XLE", "XOM", "AMZN", "XLF", "WFC", "T", "INTC", "JNJ", "PFE", "GLD", "CSCO", "PG", "DIA", "GE", "VZ", "MRK", "BKNG", "ORCL", "GS", "QCOM", "KO", "IBM", "MCD", "XLI", "BIDU", "IYR", "CMCSA", "HD", "V", "DIS", "PEP", "NFLX", "PM", "F", "MS", "AMGN", "COP", "UNH", "BA", "RTX", "GILD", "BMY", "SLB", "AXP", "MO", "MDLZ", "OXY", "LOW", "TGT", "EBAY", "UPS", "USB", "SBUX", "UNP", "VLO", "MMM", "TXN", "XLY", "WBA", "XOP", "DE", "HAL", "XLU", "CRM", "FCX", "ABT", "XLB", "XLK", "EOG", "NKE", "XLV", "GM", "HON", "SPG", "ACN", "COF", "M", "XLP", "EMR", "COST", "BP", "MDT", "DUK", "XRT", "CMI", "FDX", "CL", "MU", "AIG", "EXC", "SO", "PRU", "TJX", "PBR", "KMB", "CSX", "HPQ", "AMT", "WYNN", "TRV", "MRO", "ETN", "NEM", "TSM", "LYB", "STT", "VIAC", "ANTM", "HYG", "AMAT", "GOLD", "GIS", "PXD", "YUM", "NSC", "MCK", "FITB", "GLW", "VALE", "DFS", "ALL", "EXPE", "STX", "WDC", "CCL", "BBY", "ITW", "AAL", "ADP", "ISRG", "BLK", "APA", "HES", "ITUB", "KR", "TT", "ADBE", "KSS", "HUM", "MGM", "ADM", "TFC", "TEVA", "DVN", "CB", "IP", "TMO", "DAL", "RIO", "VMW", "CI", "VNQ", "NVDA", "HST", "PPL", "GD", "GPS", "X", "NEE", "FE", "INTU", "COG", "AZO", "PPG", "VFC", "HIG", "KEY", "UAL", "RF", "LB", "PCG", "WY", "MYL", "NLOK", "SIRI", "CAH", "EQR", "GSK", "NOC", "XLNX", "WHR", "VTR", "ATVI", "WELL", "NBL", "PSA", "JCI", "NVS", "VRTX", "SYY", "SWK", "RL", "K", "BHP", "ABC", "BBD", "AVB", "AKAM", "NUE", "CAG", "ROST", "ADI", "NTAP", "SYK", "AON", "FFIV", "PEAK", "ED", "MAR", "SHW", "ECL", "SLV", "DOV", "MMC", "TROW", "AMP", "A", "APD", "ZBH", "JWN", "PEG", "MSI", "CF", "IWO", "OMC", "BXP", "EMN", "BEN", "EIX", "NLY", "LEN", "WM", "CCI", "GWW", "PGR", "DISCA", "JNPR", "ROK", "SPGI", "IVZ", "ORLY", "IJH", "PCAR", "CHRW", "UAA", "EQT", "ICE", "ALXN", "BUD", "HBAN", "PAYX", "MOS", "HFC", "DRI", "FAST", "EW", "BSX", "PVH", "ASML", "CERN", "ADS", "CMA", "SWKS", "ILMN", "FSLR", "TOT", "ETR", "AVGO", "HSY", "BBBY", "NOK", "CNP", "SAP", "CHKP", "MXIM", "MCHP", "MNST", "DHI", "VNO", "SRE", "CLR", "HSBC", "RCL", "EQIX", "NOV", "IPG", "DISH", "VRSN", "BWA", "CLX", "PHM", "UN", "AGNC", "MPC", "TEL", "XEL", "AZN", "XRX", "AAP", "NRG", "KMX", "IBN", "OKE", "MTB", "NI", "AES", "APH", "KDP", "TOL", "KRE", "EXPD", "AEO", "WSM", "DG", "CIEN", "XHB", "VOD", "SLG", "FL", "CMS", "NTRS", "ZION", "NWL", "HAS", "XEC", "OVV", "FTI", "SNY", "URI", "HLF", "VAR", "SMH", "TECK", "TMUS", "TCOM", "GPC", "FISV", "CE", "MRVL", "MUR", "RSG", "SBAC", "TAP", "HRB", "UNM", "CPB", "SJM", "IBB", "HP", "DTE", "DGX", "ROP", "RIG", "PRGO", "WEC", "RRC", "CCEP", "DISCK", "SWN", "REGN", "FIS", "TSCO", "ON", "ULTA", "HOLX", "URBN", "TPX", "PBCT", "PII", "IRM", "ETFC", "BX", "WAT", "ALB", "MELI", "IWB", "GT", "FTNT", "FRT", "JBHT", "HUN", "PWR", "RGLD", "STZ", "BLL", "L", "NDAQ", "EAT", "KBH", "KMI", "NYCB", "CPT", "ESS", "UL", "CIT", "LHX", "J", "STLD", "IYT", "HTZ", "GRMN", "SEE", "DECK", "RHI", "JBL", "AGCO", "NTES", "KBE", "UHS", "TER", "FLS", "AMG", "ALV", "LEA", "ASH", "NVO", "RE", "ABB", "CCK", "QRTEA", "NXPI", "UTHR", "BAP", "PNW", "IAC", "COO", "DRE", "CNX", "HSIC", "LBTYA", "FMX", "AIZ", "MLM", "XRAY", "CHD", "AIV", "LEG", "THC", "NUAN", "MHK", "CHL", "GL", "MKC", "SCCO", "SINA", "ANF", "MTD", "CLF", "MAN", "SRCL", "RMD", "FLR", "CDNS", "WYND", "AU", "AWK", "TSLA", "EFX", "VMC", "FNF", "NVR", "GPN", "EV", "CRUS", "FLEX", "MT", "RS", "R", "FDS", "CINF", "AVY", "RNR", "O", "ATI", "TS", "FHN", "DOX", "HBI", "GRA", "CS", "ARW", "AVT", "CAKE", "REG", "AXS", "VRSK", "ARE", "EDU", "NCR", "ALK", "CREE"], "alg_type": "gui"}')
    logger.info('MSG SENT!')


def mode_loop():
    while True:
        time.sleep(60)


def mode_kafka_test():
    init_kafka()
    for message in kafka.consumer:
        value = message.value.decode("utf-8")
        logger.info(f'GOT NEW MSG: {value}')
        input()
        logger.info('WAITING FOR NEW MSG...')


def mode_git_test():
    local_repo = '/tmp/repo'
    if not os.path.exists(local_repo):
        repo = git.Repo.clone_from(os.getenv('GIT_REPO'), local_repo)
    else:
        repo = git.Repo(local_repo)
    repo.heads.master.checkout()
    repo.remotes.origin.pull()
    repo.head.reset(commit='4a408e5f92fe102df0aa59476636f8f392cc2dd8', working_tree=True)
    print(repo.head.commit)
    os.system(f'python3 {local_repo}/model_scripts/run_iterative.py')
    print('finished!')


def main(mode):
    d = {
        'LISTENING': mode_listening,
        'STORAGE': mode_storage,
        'PROVIDED_VALUE': mode_provided_value,
        'PROVIDED_VALUE_METRICS': mode_provided_value_with_metrics,
        'SENDING': mode_sending,
        'LOOP': mode_loop,
        'KAFKA_TEST': mode_kafka_test,
        'GIT_TEST': mode_git_test
    }
    if mode in d:
        logger.info(f'Mode {mode} selected')
        d[mode]()
    else:
        logger.error('NO MODE SELECTED!')


if __name__ == '__main__':
    logger.info(f"Python version: {sys.version}")
    logger.info(f"Num GPUs Available: {len(tf.config.experimental.list_physical_devices('GPU'))}")
    logger.info("~~=START=~~")
    selected_mode = os.getenv('MODE')
    main(selected_mode)
    logger.info("--=END=--")
    os._exit(0)
