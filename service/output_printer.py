from slack_logger import SlackHandler, SlackFormatter
import logging.config
import os

try:
    logging.config.fileConfig('logging.conf')
except Exception as ex:
    print("Can't open a logging.conf file")
logger = logging.getLogger('output')
sh = SlackHandler(
    url=os.getenv('SLACK_URL', 'https://hooks.slack.com/services/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'))
sh.setLevel(logging.INFO)
sh.setFormatter(SlackFormatter())
logger.addHandler(sh)
