import json
import os
import git
import logging
from datetime import datetime
from google.cloud import storage
import traceback
import model_builder
from typing import Optional

logger = logging.getLogger()


def main(message):
    try:
        main_dir = '/tmp/data'
        os.makedirs(main_dir, exist_ok=True)
        json_msg = json.loads(message)

        if json_msg['alg_type'] == 'run_models':
            model_builder.main(main_dir,gui_dict=json_msg['gui_dict'],tickers=json_msg['tickers'])

    except Exception as e:
        log_path = f'{main_dir}/error_log.json'
        bug_details = {}
        bug_details['date'] = datetime.now().isoformat()
        bug_details['json_msg'] = json_msg
        bug_details['Exception'] = str(e)
        bug_details['traceback'] = traceback.format_exc()

        with open(log_path, 'w+') as f:
            json.dump(bug_details, f)
        client = storage.Client()
        storage.bucket.Bucket(client, 'free_market_rnd').blob(
            f"bugs/{json_msg['alg_type']}_{bug_details['date']}.json").upload_from_filename(log_path)
        raise e


def get_failed_pod_url(cluster: Optional[str], location: Optional[str], namespace: Optional[str],
                       pod_name: Optional[str]):
    prefix = 'https://console.cloud.google.com/logs/query;query=resource.type'
    log_url_parts = ['="k8s_container"\n',
                     'resource.labels.project_id="finityx-app"\n',
                     f'resource.labels.location="{location}"\n' if location else '',
                     f'resource.labels.cluster_name="{cluster}"\n' if cluster else '',
                     f'resource.labels.namespace_name="{namespace}"\n' if namespace else '',
                     f'resource.labels.pod_name="{pod_name}"\n' if pod_name else '']
    log_url = ''.join(log_url_parts)
    suffix = ';timeRange=PT12H?supportedpurview=project&project=finityx-app'
    from urllib.parse import quote
    return prefix + quote(log_url, safe='') + suffix


def select_commit(local_repo_path, commit_hash):
    if not os.path.exists(local_repo_path):
        repo = git.Repo.clone_from(os.getenv('GIT_REPO'), local_repo_path)
    else:
        repo = git.Repo(local_repo_path)
    repo.heads.master.checkout()
    repo.head.reset(working_tree=True)  # Reset local changes
    repo.remotes.origin.pull()
    repo.head.reset(commit=commit_hash, working_tree=True)
    logger.debug(f'Commit selected: {repo.head.commit}')


if __name__ == '__main__':
    msg = """
    {"alg_type": "alligator", "alligator_configurations": {"models_parameters": {"10M_iter_exp_c2o": "10M_iter_exp_c2o"}}, "spec": {"id": "alligator", "tradingEnv": "test"}}
    """
    from datetime import timedelta

    NDAYS = 0
    curr_date = datetime.now().date()  -timedelta(NDAYS)
    for k in range(1):
        # curr_date = curr_date + timedelta(1)
        for mode in ['open']:  # 'open',
            print('************************************************************', curr_date, mode,
                  '******************************************************************')
        for mode in ['open']:#'open',
            print('************************************************************',curr_date,mode,'******************************************************************')
            os.environ['CURRENT_DATE'] = curr_date.isoformat()
            os.environ['TRADING_MODE_FOR_DEBUG'] = mode
            main(msg)
