import pytz
from google.cloud import bigquery
import pandas as pd
import numpy as np
import scipy.io

bigqueryclient = bigquery.Client()
local = pytz.timezone("UTC")

class StatisticalArbitrageCalculator():
    def __init__(self,res,smooth=5):
        self.smooth = smooth
        self.res  = res
        self.rows = None
        self.pre_inv = 0.0

    @staticmethod
    def calc_inv(x, y):
        tmp1 = abs(x - y)
        tmp2 = tmp1
        tmp3 = np.sign(x - y)
        return -tmp3 * tmp2

    def add_row(self,row_x,row_y):
        assert row_x.date == row_y.date

        current_row = {'time':row_x.date,'close_x':row_x.close,'close_y':row_y.close,'inv':np.nan,'past_rev_x':np.nan,'past_rev_y':np.nan,'smoothed_inv':np.nan,'rounded_inv':np.nan,'coefx':np.nan,'coefy':np.nan,'invests':np.nan,'res':self.res}
        if self.rows is None:
            self.rows = pd.DataFrame([current_row])
            return

        self.rows = pd.concat([self.rows, pd.DataFrame([current_row])])


        if len(self.rows)<16:
            return

        tmpx = 0
        tmpy = 0
        for pp in range(4, 16):
            tmpx = tmpx + (((self.rows['close_x'].iloc[-1] / self.rows['close_x'].iloc[-2 ^ pp] - 1) / np.sqrt(2 ^ pp)) * 100)
            tmpy = tmpy + (((self.rows['close_y'].iloc[-1] / self.rows['close_y'].iloc[-2 ^ pp] - 1) / np.sqrt(2 ^ pp)) * 100)


        self.rows['past_rev_x'].iloc[-1] = tmpx
        self.rows['past_rev_y'].iloc[-1] = tmpy

        coefx = 0
        coefy = 0
        for pp in range(4, 16):
            coefx = coefx + (((1 / self.rows['close_x'].iloc[-2 ^ pp]) / np.sqrt(2 ^ pp))) * 100
            coefy = coefy + (((1 / self.rows['close_y'].iloc[-2 ^ pp]) / np.sqrt(2 ^ pp))) * 100

        self.rows['coefx'].iloc[-1] = coefx
        self.rows['coefy'].iloc[-1] = coefy


        self.rows['inv'].iloc[-1] = StatisticalArbitrageCalculator.calc_inv(self.rows['past_rev_x'].iloc[-1], self.rows['past_rev_y'].iloc[-1])

        self.rows['smoothed_inv'].iloc[-1] = np.mean(self.rows['inv'][-self.smooth::])
        self.rows['rounded_inv'].iloc[-1] = self.res*np.floor(abs(self.rows['smoothed_inv'].iloc[-1])/self.res)*np.sign(self.rows['smoothed_inv'].iloc[-1])

        self.rows['invests'].iloc[-1] = self.rows['rounded_inv'].iloc[-1]

        if self.rows['invests'].iloc[-1]!=self.pre_inv:
            if abs(self.rows['inv'].iloc[-1]-self.pre_inv)<(self.res/2):
                self.rows['invests'].iloc[-1]=self.pre_inv

        self.pre_inv = self.rows['invests'].iloc[-1]


    def save(self,mat_file):
        mat_dict = {k: self.rows[k].tolist() for k in self.rows.keys()}
        mat_dict['time'] = [t.isoformat() for t in mat_dict['time']]
        scipy.io.savemat(mat_file, mat_dict)


