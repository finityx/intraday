from broker.broker_runner import BrokerRunner
from datetime import datetime,timedelta
from collections import OrderedDict
from google.cloud import bigquery
from broker.broker_missions import get_df_from_bars
from statistical_arbitrage.statstical_arbitrage import StatisticalArbitrageCalculator
import pandas_market_calendars as mcal
import random,pandas as pd
import ib_insync,os,numpy as np
import time
import pickle


class PositionHandlerLogger():
    def __init__(self, path):
        self.path = path
        self.header = True
        os.makedirs(os.path.dirname(self.path),exist_ok=True)

    def print(self,action_str,ticker_handler):
        df = {}
        df['time'] = [datetime.now().isoformat()]
        df['action'] = [action_str]
        df['ticker'] = [ticker_handler.ticker]
        df['model_name'] = [ticker_handler.model_name]
        df['bid'] = [ticker_handler.bid]
        df['ask'] = [ticker_handler.ask]
        df['pips'] = [ticker_handler.pips]
        df['last'] = [ticker_handler.last]
        df['pos'] = [ticker_handler.pos]
        pd.DataFrame(df).to_csv(self.path, mode='a+', header=self.header)
        print(df)
        self.header = False

class TickDataHandler():
    def __init__(self):
        self.tick_data = {}

    def trace(self,broker_runner,ticker):
        contract = ib_insync.Stock(symbol=ticker, exchange='SMART', currency='USD')
        c_tick_data = {}
        c_tick_data['bid_ask'] = broker_runner.ib.reqTickByTickData(contract=contract, tickType='BidAsk', numberOfTicks=1, ignoreSize=True)
        c_tick_data['last'] = broker_runner.ib.reqTickByTickData(contract=contract, tickType='Last', numberOfTicks=1, ignoreSize=True)

        for k_time in range(60):
            if np.isnan(c_tick_data['bid_ask'].ask) | np.isnan(c_tick_data['last'].last):
                broker_runner.ib.sleep(1)
            else:
                break

        self.tick_data[ticker] = c_tick_data


    def is_traced(self,ticker):
        return ticker in self.tick_data

class PairPositionHandler():
    def __init__(self,tickers,res,tick_price_handler):
        self.res = res
        self.model_name = ','.join(tickers)
        self.tickers = tickers

        self.coeff_x = None
        self.coeff_y = None

        self.pos_x  = 0
        self.pos_y  = 0

        self.position  = 0
        self.base_price = 0

        self.limit_orders = {'long_x':None,'long_y':None,'short_x':None,'short_y':None,}
        self.buy_orders = []
        self.pips_ratio = None
        self.tick_price_handler = tick_price_handler

        self.histe_long_x  = 0.0
        self.histe_short_x = 0.0

    @property
    def bid_x(self):
        return  self.tick_price_handler.tick_data[self.tickers[0]]['bid_ask'].bid

    @property
    def bid_y(self):
        return  self.tick_price_handler.tick_data[self.tickers[1]]['bid_ask'].bid


    @property
    def ask_x(self):
        return  self.tick_price_handler.tick_data[self.tickers[0]]['bid_ask'].ask

    @property
    def ask_y(self):
        return  self.tick_price_handler.tick_data[self.tickers[1]]['bid_ask'].ask


    @property
    def last_x(self):
        return  0.5*(self.ask_x+self.bid_x)#self.tick_price_handler.tick_data[self.tickers[0]]['last'].last

    @property
    def last_y(self):
        return  0.5*(self.ask_y+self.bid_y)#self.tick_price_handler.tick_data[self.tickers[1]]['last'].last

    @property
    def spread_x(self):
        return (self.tick_price_handler.tick_data[self.tickers[0]]['bid_ask'].ask - self.tick_price_handler.tick_data[self.tickers[0]]['bid_ask'].bid)

    @property
    def spread_y(self):
        return (self.tick_price_handler.tick_data[self.tickers[1]]['bid_ask'].ask - self.tick_price_handler.tick_data[self.tickers[1]]['bid_ask'].bid)


    @property
    def pips_x(self):
        return np.round(self.pips_ratio * self.spread_x * 100) / 100

    @property
    def pips_y(self):
        return np.round(self.pips_ratio * self.spread_y * 100) / 100

    @property
    def is_traced(self):
        return all([self.tick_price_handler.is_traced(ticker) for ticker in self.tickers])

    def trace(self,broker_runner):
        for ticker in self.tickers:
            self.tick_price_handler.trace(broker_runner=broker_runner, ticker=ticker)
            broker_runner.ib.sleep(1)

    @property
    def limit_price_x(self):
        limit_long  = (-0.5 * self.res - self.position)/100*self.last_x + (self.coeff_y * self.last_y) / self.coeff_x - self.histe_long_x/100*self.last_x
        limit_short = (0.5 * self.res - self.position )/100*self.last_x + (self.coeff_y * self.last_y) / self.coeff_x + self.histe_short_x/100*self.last_x
        return limit_long,limit_short

    @property
    def limit_price_y(self):
        limit_long  = (-0.5 * self.res + self.position)/100*self.last_y  + (self.coeff_x * self.last_x) / self.coeff_y - self.histe_short_x/100*self.last_y
        limit_short = (0.5 * self.res + self.position)/100*self.last_y  + (self.coeff_x * self.last_x) / self.coeff_y  + self.histe_long_x/100*self.last_y
        return limit_long,limit_short


class PositionHandler():
    def __init__(self,main_dir,broker_runner,pairs_list,aum,res=0.25,handle_positions_separately=False):
        self.aum = aum
        self.broker_runner  = broker_runner
        self.started = False
        self.handle_positions_separately = handle_positions_separately
        self.tickers_tick_data = TickDataHandler()
        for tickers in pairs_list:
            for ticker in tickers:
                self.tickers_tick_data.trace(broker_runner, ticker)

        self.res = res
        self.pair_position_handelers = OrderedDict()
        for tickers in pairs_list:
            self.pair_position_handelers[tuple(tickers)] = PairPositionHandler(tickers,self.res, self.tickers_tick_data)

        ti = datetime.now().strftime("%Y%m%d-%H%M%S")
        self.log = PositionHandlerLogger(rf"{main_dir}/logger/{ti}.csv")

    def update_coefficients(self,row,tickers):
        self.pair_position_handelers[tuple(tickers)].coeff_x = row.coefx
        self.pair_position_handelers[tuple(tickers)].coeff_y = row.coefy
        self.started = True

    def buy_now(self,trade,lmtPrice):
        self.broker_runner.ib.sleep(0)
        [self.broker_runner.ib.cancelOrder(order) for order in self.broker_runner.ib.openOrders()]
        order = ib_insync.LimitOrder(action = trade.order.action,totalQuantity = trade.order.totalQuantity-trade.filled(),lmtPrice=lmtPrice())
        order.faGroup = self.broker_runner.fa_group
        order.faMethod = self.broker_runner.faMethod

        trade =  self.broker_runner.ib.placeOrder(trade.contract, order)
        while trade.orderStatus.status != 'Filled' :
            trade.order.lmtPrice =  lmtPrice()
            trade = self.broker_runner.ib.placeOrder(trade.contract, trade.order)
            self.broker_runner.ib.sleep(0.1)

        return trade

    def handle_position(self):
        broker_runner = self.broker_runner
        for tickers,position_handler in self.pair_position_handelers.items():
            if position_handler.limit_orders['long_x'] is not None:

                if not ((position_handler.limit_orders['long_x'].orderStatus.status != 'Filled') and (position_handler.limit_orders['short_y'].orderStatus.status != 'Filled')):
                    if (position_handler.limit_orders['long_x'].orderStatus.status == 'Filled') and (position_handler.limit_orders['short_y'].orderStatus.status != 'Filled'):
                        self.buy_now(position_handler.limit_orders['short_y'],lmtPrice=lambda :(position_handler.bid_y-0.01))
                    elif (position_handler.limit_orders['long_x'].orderStatus.status != 'Filled') and (position_handler.limit_orders['short_y'].orderStatus.status == 'Filled'):
                        self.buy_now(position_handler.limit_orders['long_x'],lmtPrice=lambda :(position_handler.ask_x+0.01))

                    # for limit_type,cur_order in position_handler.limit_orders.items():
                    #     if cur_order is not None:
                            #self.broker_runner.ib.cancelOrder(cur_order.order)

                    [self.broker_runner.ib.cancelOrder(order) for order in self.broker_runner.ib.openOrders()]


                    position_handler.limit_orders['short_x'] = None
                    position_handler.limit_orders['long_y'] = None
                    position_handler.limit_orders['short_y'] = None
                    position_handler.limit_orders['long_x'] = None
                    position_handler.position += self.res
                    position_handler.histe_short_x = self.res/4
                    position_handler.histe_long_x = 0.0


            if position_handler.limit_orders['short_x'] is not None:
                if not ((position_handler.limit_orders['short_x'].orderStatus.status != 'Filled') and (position_handler.limit_orders['long_y'].orderStatus.status != 'Filled')):
                    if (position_handler.limit_orders['short_x'].orderStatus.status == 'Filled') and (position_handler.limit_orders['long_y'].orderStatus.status != 'Filled'):
                        self.buy_now(position_handler.limit_orders['long_y'],lmtPrice=lambda :(position_handler.ask_y+0.01))
                    elif (position_handler.limit_orders['short_x'].orderStatus.status != 'Filled') and (position_handler.limit_orders['long_y'].orderStatus.status == 'Filled'):
                        self.buy_now(position_handler.limit_orders['short_x'],lmtPrice=lambda :(position_handler.bid_x-0.01))

                    # for limit_type,cur_order in position_handler.limit_orders.items():
                    #     if cur_order is not None:
                    #         self.broker_runner.ib.cancelOrder(cur_order.order)
                    #         self.broker_runner.ib
                    [self.broker_runner.ib.cancelOrder(order) for order in self.broker_runner.ib.openOrders()]


                    position_handler.limit_orders['short_x'] = None
                    position_handler.limit_orders['long_y'] = None
                    position_handler.limit_orders['short_y'] = None
                    position_handler.limit_orders['long_x'] = None

                    position_handler.position -= self.res
                    position_handler.histe_short_x = 0.0
                    position_handler.histe_long_x = self.res/4


            limits = {}
            broker_runner.ib.sleep(0)
            limits['long_x'], limits['short_x']  = position_handler.limit_price_x
            limits['long_y'], limits['short_y']  = position_handler.limit_price_y

            for limit_type in limits.keys():
                if '_x' in limit_type:
                    ticker = position_handler.tickers[0]
                    last = position_handler.last_x
                else:
                    ticker = position_handler.tickers[1]
                    last = position_handler.last_y

                if position_handler.limit_orders[limit_type] is None:
                    if 'long' in limit_type:
                        action = 'BUY'
                    else:
                        action = 'SELL'

                    totalQuantity = int(np.round(self.res*self.aum/last))
                    cur_order = ib_insync.LimitOrder(action=action, totalQuantity=totalQuantity, lmtPrice=np.round(100*limits[limit_type])/100, orderId=broker_runner.ib.client.getReqId())
                    cur_order.faGroup = broker_runner.fa_group
                    cur_order.faMethod = broker_runner.faMethod
                    position_handler.limit_orders[limit_type] = broker_runner.ib.placeOrder(broker_runner.get_contract(ticker), cur_order)
                elif position_handler.limit_orders[limit_type].orderStatus.status not in position_handler.limit_orders[limit_type].orderStatus.DoneStates:

                    position_handler.limit_orders[limit_type].order.lmtPrice = np.round(100*limits[limit_type])/100
                    position_handler.limit_orders[limit_type] = broker_runner.ib.placeOrder(position_handler.limit_orders[limit_type].contract, position_handler.limit_orders[limit_type].order)

                broker_runner.ib.sleep(0.1)


def init_live(main_dir,broker_runner,tickers,res,intraday_table):
    bq_client = bigquery.Client()

    while True:
        dates_max = bq_client.query(f'select ticker,max(date) as max_date from (select * from {intraday_table} where ticker in {str(tuple(tickers))}) group by ticker').result().to_dataframe()
        tickers_max_date = dict(zip(dates_max.ticker,dates_max.max_date))

        to_break = True
        now = datetime.utcnow()
        now =  now - timedelta(hours=now.hour,minutes=now.hour,seconds=now.hour)
        for ticker,max_date in tickers_max_date.items():

            max_date = max_date.tz_convert(None)
            print(ticker,max_date)
            df = get_df_from_bars(broker_runner.ib.reqHistoricalData(BrokerRunner.get_contract(ticker),max_date+timedelta(20), "1 M", "1 min", "TRADES", useRTH=1),ticker)
            df = df[(df.date>max_date)&(df.date<now)]
            if df.size>0:
                df.to_gbq(intraday_table,if_exists='append')
                to_break = False

        if to_break:
            break


    dfs = OrderedDict()
    for ticker in tickers:
        dfs[ticker] = bq_client.query(f"select distinct * from {intraday_table} where ticker = '{ticker}' and date>'2021-01-01' order by date").result().to_dataframe()

    dates = set(dfs[list(dfs.keys())[0]].date)
    for ticker in tickers:
        dates  = dates.intersection(dfs[ticker].date)

    statistical_calc = StatisticalArbitrageCalculator(res=res)
    start_time = time.time()
    for idate,date in enumerate(sorted(list(dates))):
        if any([dfs[ticker][dfs[ticker].date==date].size==0 for ticker in tickers]):
            continue

        tickers_df = [dfs[ticker][dfs[ticker].date == date] for ticker in tickers]
        statistical_calc.add_row(tickers_df[0].iloc[0],tickers_df[1].iloc[0])

        if idate%5000==0:
            print((idate+1)/len(dates),time.time()-start_time,date)

    statistical_calc.save(rf'{main_dir}\save.mat')
    statistical_calc.rows.to_csv(rf'{main_dir}\save.csv')

    with open(rf'{main_dir}\statistical_calc.pkl','wb+') as file:
        pickle.dump(statistical_calc,file)

    return statistical_calc


def run_live(tickers,res=0.25,aum=1000,intraday_table='KIRA.intraday_1min_full_clean',main_dir='D:/statistical_arb_live',model_name='statistical_arbitrage',load_pkl = False):
    close_time = mcal.get_calendar('NYSE').schedule(datetime.now(), datetime.now(), 'UTC').market_close[0] - timedelta(minutes=15)

    os.makedirs(main_dir,exist_ok=True)

    curr_date = datetime.now()
    broker_runner = BrokerRunner()

    if load_pkl:
        with open(rf'{main_dir}/statistical_calc.pkl','rb') as file:
            statistical_calc = pickle.load(file)
    else:
        statistical_calc = init_live(main_dir,broker_runner,tickers,res, intraday_table)
        exit(0)

    position_handler = PositionHandler(main_dir, broker_runner=broker_runner, pairs_list=[tickers],res=res,aum=1000)

    for ticker in tickers:
        contract = BrokerRunner.get_contract(ticker)
        broker_runner.ib.reqHistoricalData(contract, '', "1 D", "1 min", "TRADES", useRTH=1, keepUpToDate=True)

    while (datetime.utcnow().time() <= close_time.time()):
        broker_runner.ib.sleep(0)
        periodic_data_bars = OrderedDict()
        lengths = []
        for ticker, bars in zip(tickers, broker_runner.ib.realtimeBars()):
            periodic_data_bars[ticker] = [b for b in bars if b.date.date() == curr_date.date()]
            lengths.append(len(periodic_data_bars[ticker]))

        if all([len(pdb) == 0 for ticker, pdb in periodic_data_bars.items()]):
            broker_runner.ib.sleep(0.5)
            if random.random() < 0.1:
                print('trade didnt start')
            continue

        broker_runner.ib.sleep(0)

        if (all([l == lengths[0] for l in lengths])):
            if not position_handler.started:
                max_bar = OrderedDict()

                for k in range(0,len(list(periodic_data_bars.values())[0])-1):
                    for ticker, pdb in periodic_data_bars.items():
                        max_bar[ticker] = get_df_from_bars([pdb[k]], ticker)
                    statistical_calc.add_row(row_x=max_bar[tickers[0]].iloc[-1],row_y=max_bar[tickers[1]].iloc[-1])

            max_bar = OrderedDict()
            for ticker, pdb in periodic_data_bars.items():
                max_bar[ticker] = get_df_from_bars([max(pdb, key=lambda x: x.date)],ticker)


            statistical_calc.add_row(row_x=max_bar[tickers[0]].iloc[-1],row_y=max_bar[tickers[1]].iloc[-1])
            position_handler.update_coefficients(statistical_calc.rows.iloc[-1],tickers)

        if position_handler.started:
            position_handler.handle_position()
            broker_runner.ib.sleep(1)



if __name__ == '__main__':
    run_live(['TQQQ','TECL'],load_pkl=False,res=0.25,aum=1000)