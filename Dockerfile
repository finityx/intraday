FROM tensorflow/tensorflow:2.4.0
#-gpu

WORKDIR /opt/model
ENV PYTHONPATH=/opt/model

COPY . .

# DEFAULT RUN (PYTHON 3.6)
#RUN apt-get -y update && apt-get install -y git libpq-dev \
# && pip install -r requirements.txt \
# && chmod +x ./run.sh

# PYTHON 3.7
RUN apt-get -y update && apt-get install -y git python3.7 python3-pip \
 && update-alternatives --install /usr/bin/python3 python3 /usr/bin/python3.6 1 &&  update-alternatives --install /usr/bin/python3 python3 /usr/bin/python3.7 2 \
 && python3 -m pip --no-cache-dir install --upgrade  six setuptools wheel pip \
 && pip install -r requirements.txt \
 && chmod +x ./run.sh

CMD [ "./run.sh"]
