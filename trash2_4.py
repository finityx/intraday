from  model_builder import *
tickers = []

client = storage.Client()

# main_dir = 'd:/ProfitPerMinuteInvestChange_no_army_core_arch_diffrent'
# os.makedirs(main_dir, exist_ok=True)

tickers = ["XLE","XLF","XLI","XLY","XLV","XLU","VNQ","AAPL","CVX","EEM","WMT","TQQQ", "SOXL", "FAS", "TNA","NVDA",'NUGT','cash']

sz=50
run_parameters = RunParameters()
run_parameters.core_arch = ('BasicLstm', {'start_dense_size': [sz], 'ticker_embedding': sz, 'lstm_size': sz, 'out_dense_size': [sz,sz], 'stateful': False})
run_parameters.shuffle=True
run_parameters.batch_size = 50

obj = RunObjective()

obj['aum'] = 10
obj['loss'] = 'ShiloInvestChanger'
obj['comis_per_stock'] = 0.0
obj['spread'] = 0.01 / 100

run_parameters.objectives.append(obj)

run_parameters.run_name = 'until_close'

main_dir = r'D:\temp_test_24_new'
os.makedirs(main_dir, exist_ok=True)

run_parameters.strategy = ('TrailingSLStrategy', {'trailing_sl': [1], 'tp': [1]})
main(main_dir=main_dir, gui_dict=run_parameters.__dict__, tickers=tickers,bucket_name=None)
