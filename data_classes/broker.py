import json
import logging
import os
import pytz
from enum import Enum
from datetime import date, datetime, timedelta, timezone
from dataclasses import dataclass
from com.finityx.protobuf.broker.broker_request_pb2 import BrokerRequestData as ProtoBrokerRequest
from com.finityx.protobuf.broker.order_pb2 import Order as ProtoOrder
from com.finityx.protobuf.broker.order_action_pb2 import OrderAction as ProtoOrderAction
from com.finityx.protobuf.broker.enums_pb2 import OrderActionType as ProtoOrderActionType
from com.finityx.protobuf.broker.account_pb2 import Account as ProtoAccount
from com.finityx.protobuf.broker.enums_pb2 import TIFType, SourceType
from com.finityx.protobuf.broker.ordertype.order_type_pb2 import OrderType

logging.basicConfig(level=logging.NOTSET)


@dataclass
class Prediction:
    ticker: str
    invest_ratio: float
    open_mode: str
    close_mode: str
    open_order_start_time:datetime
    open_order_end_time:datetime
    close_order_start_time:datetime
    close_order_end_time:datetime
    days_interval: int
    mra: float
    quantity_stocks: int
    stock_price:float
    account: str
    subaccount: str
    mode: str
    model_name: str
    exchange: str = 'SMART'
    source: SourceType = SourceType.ALGO


@dataclass
class BrokerRequest:
    predictions: list = ()

    def to_json(self):
        return json.dumps(self, default=lambda o: o.__dict__,
                          sort_keys=True, indent=4)

    def to_proto(self):
        start_ts = datetime.now(tz=pytz.timezone('America/New_York'))
        start_date = datetime.date(start_ts)

        def create_order_action(mode: str, order_start_ts: datetime, order_end_ts: datetime):
            order_action = ProtoOrderAction()
            result = [x.strip() for x in mode.split(',')]
            if len(result) == 1:
                order_action.order_type = OrderType.Value(result[0])
            elif len(result) == 2:
                order_action.order_type = OrderType.Value(result[0])
                order_action.tifs.extend([TIFType.Value(t) for t in result[1].split(' ')])
            order_action.start_ts = order_start_ts.isoformat()
            order_action.transmit_ts = order_start_ts.isoformat()
            order_action.end_ts = order_end_ts.isoformat()
            return order_action

        def get_orders(prediction: Prediction):

            if all(mode in prediction.open_mode for mode in ['MOC']) or all(mode in prediction.open_mode for mode in ['MKT', 'GTD']):
                open_order_action = create_order_action(
                    prediction.open_mode,
                    prediction.open_order_start_time.astimezone(pytz.timezone('America/New_York')),
                    prediction.open_order_end_time.astimezone(pytz.timezone('America/New_York')),
                )
                close_order_action = create_order_action(
                    prediction.close_mode,
                    prediction.close_order_start_time.astimezone(pytz.timezone('America/New_York')),
                    prediction.close_order_end_time.astimezone(pytz.timezone('America/New_York')),
                )

            else:
                raise Exception(f'Unsupported mode {prediction.open_mode}')
            window = f'1 day|{open_order_action.start_ts}|{close_order_action.start_ts}'

            def create_order(position: str, order_action: ProtoOrderAction, order_action_type: int):
                order = ProtoOrder()
                order.window = window
                order.ticker = prediction.ticker
                order.position = position
                order.quantity_ratio = abs(prediction.invest_ratio)
                order.exchange = prediction.exchange
                order.source = prediction.source
                order.action.CopyFrom(order_action)
                order.action_type = order_action_type
                order.mra = prediction.mra
                order.account.CopyFrom(
                    ProtoAccount(account=prediction.account, subaccount=prediction.subaccount, mode=prediction.mode))
                order.model_name = prediction.model_name
                return order

            positions = ('BUY', 'SELL') if prediction.invest_ratio > 0 else ('SELL', 'BUY')
            return [create_order(positions[0], open_order_action, 0), create_order(positions[1], close_order_action, 1)]

        req = ProtoBrokerRequest()
        req.timestamp = start_ts.isoformat()

        orders = []
        for p in self.predictions:
            orders.extend(get_orders(p))
        req.orders.extend(orders)
        return req.SerializeToString()
