from enum import Enum


class TradingEnv(Enum):
    TEST = 'test'
    PRODUCTION = 'production'
    PAPER = 'paper'
    LIVE = 'live'


def validate_name(id: str, app_name: str, trading_env: TradingEnv):
    full_name = f'{app_name}-{trading_env.value}-{id}'
    if len(full_name) > 63:
        print(f'Validation failed! Name longer then 63 chars')
        return False
    for c in full_name:
        if not (c in ['-', '.'] or c.isalnum()):
            print(f'Validation failed! Char {c} is not allowed')
            return False
    return True
