from crypto_broker import config as broker_config
from binance import Client, ThreadedWebsocketManager, ThreadedDepthCacheManager

class BrokerRunner():
    def __init__(self,api_key=broker_config.api_key,secret_key=broker_config.secret_key):
        self.api_key = api_key
        self.secret_key = secret_key

    def is_live(self):
        return self.ib.isConnected()

    def place_orders(self,contracts,orders,end_time=None):
        if end_time is None:
            end_time = datetime.now()+timedelta(days=1)

        for contract, cur_order in zip(contracts,orders):
            if not self.is_live():
                self.connect()
            if datetime.now()<end_time:
                if type(cur_order)==ib_insync.order.BracketOrder:
                    trd = []
                    for cur_cur_order in cur_order:
                        cur_cur_order.faGroup = self.fa_group
                        cur_cur_order.faMethod = self.faMethod
                        trd.append(self.ib.placeOrder(contract, cur_cur_order))
                    self.trades.append(trd)
                else:
                    cur_order.faGroup = self.fa_group
                    cur_order.faMethod = self.faMethod
                    self.trades.append(self.ib.placeOrder(contract, cur_order))
            else:
                self.trades.append(None)

    @staticmethod
    def get_contract(ticker):
        contract = ib_insync.Stock(symbol=get_ib_clean_ticker(ticker), exchange='SMART', currency='USD')
        contract.primaryExchange = "ISLAND"
        return contract
