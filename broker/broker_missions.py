from datetime import datetime
from broker.broker_runner import BrokerRunner
import pandas as pd
import ib_insync,time,inspect,json
from google.cloud import bigquery


def get_df_from_bars(bars: ib_insync.BarDataList, ticker: str = None) -> pd.DataFrame:
    data_dict = {'date': [b.date for b in bars], 'open': [b.open for b in bars],
                 'close': [b.close for b in bars],
                 'high': [b.high for b in bars], 'low': [b.low for b in bars],
                 'volume': [b.volume for b in bars],
                 'average': [b.average for b in bars], 'barCount': [b.barCount for b in bars]}
    data_df = pd.DataFrame.from_dict(data_dict)
    if ticker is not None:
        data_df['ticker'] = ticker
    return data_df


def download_historical_minute(broker_runner,ticker=[],year=[],month=[]):
    dfs = []
    table_name_add = 'KIRA.intraday_1min_full_new2'
    for t,y,m in zip(ticker,year,month):
        print(t,y,m)
        contract = broker_runner.get_contract(t)
        bars = broker_runner.ib.reqHistoricalData(contract, datetime(y,m,1).date(), "1 M", "1 min", "TRADES",useRTH=1, timeout=300)
        dfs.append(get_df_from_bars(bars, t))
    pd.concat(dfs).to_gbq(table_name_add, if_exists='append')
    return ''


def download_tick_data(broker_runner,ticker=[]):
    run_time = 3600*4
    table_name_add = 'KIRA.tick_by_tick_data'
    ticker_req = {}
    for t in ticker:
        contract = broker_runner.get_contract(t)
        ticker_req[t] =  broker_runner.ib.reqTickByTickData(contract, 'BidAsk')

    start_time = time.time()
    ticker_bars = {t: [] for t in ticker}
    while (time.time()-start_time) <= run_time:
        for t in ticker:
            ticker_bars[t] += [tuple(tic._asdict().values())[0:-1] for tic in ticker_req[t].tickByTicks]
            if len(ticker_req[t].tickByTicks)>0:
                keys = list(ticker_req[t].tickByTicks[0]._asdict().keys())[0:-1]
            ticker_bars[t] = list(set(ticker_bars[t]))
        broker_runner.ib.sleep(1)
    dfs = []
    for t in ticker:
        cdf = pd.DataFrame([dict(zip(keys,b)) for b in ticker_bars[t]])
        cdf['ticker'] = t
        dfs.append(cdf)
    pd.concat(dfs).to_gbq(table_name_add, if_exists='append')
    return ''


class BrokerMissionHandler():
    def __init__(self):
        self.Missions = {}
        self.Missions['download_historical_minute'] = download_historical_minute
        self.bq_missions_table =  'KIRA.broker_missions'
        self.batch_size = 20
        self.broker_runner = BrokerRunner()
        self.bq_client = bigquery.Client()

    def run_missions(self):
        curr_missions = self.bq_client.query(f'select * from {self.bq_missions_table} where done=false ').result().to_dataframe()
        start_time = time.time()
        for mission_name,curr_missions_group in curr_missions.groupby('name'):
            curr_missions_group = curr_missions_group.to_dict(orient='rows')

            if not self.broker_runner.is_live():
                self.broker_runner.connect()

            for k in range(0,len(curr_missions_group),self.batch_size):
                c_batch_missions = curr_missions_group[k:(k+self.batch_size)]
                mission_params = [json.loads(bm['mission_params']) for bm in c_batch_missions]
                inp_dict = {k:[ms[k] for ms in mission_params] for k in mission_params[0].keys()}

                try:
                    comment = self.Missions[mission_name](self.broker_runner,**inp_dict)
                except:
                    print('skipp!!!!')
                    continue

                for mis in c_batch_missions:
                    print(time.time()-start_time,mission_name,mis['mission_params'])
                    mis['done']=True
                    mis['comment'] = comment
                    mis['run_date'] = datetime.now()

                self.add_missions(c_batch_missions,override=True)

    def get_mission(self,mission_name,mission_params):
        arg_spec = inspect.getfullargspec(self.Missions[mission_name])
        assert (set(arg_spec.args)-set(['broker_runner'])) ==set(list(mission_params.keys())), 'must contain all function params'
        mission = {'name':mission_name,'mission_params':json.dumps(mission_params),'insert_date':datetime.now(),'run_date':datetime.now(),'done':False,'comment':''}
        return mission

    def add_missions(self,mission_list,override=False):
        curr_missions = self.bq_client.query(f'select * from {self.bq_missions_table}').result().to_dataframe().to_dict(orient='rows')

        if override:
            missions_ids = [(mis['name'],mis['mission_params']) for mis in mission_list]
            mission_list = mission_list + [mis for mis in curr_missions if (mis['name'],mis['mission_params']) not in missions_ids]
        else:
            missions_ids = [(mis['name'],mis['mission_params']) for mis in curr_missions]
            mission_list = curr_missions + [mis for mis in mission_list if (mis['name'],mis['mission_params']) not in missions_ids]

        pd.DataFrame(mission_list).to_gbq(self.bq_missions_table,if_exists='replace')

if __name__ =='__main__':
    import os,pandas as pd
    from google.cloud import storage
    from google.cloud import bigquery
    from datetime import timedelta

    main_dir = fr'D:\tick_by_tick_26_10_2021'
    os.makedirs(main_dir,exist_ok=True)
    stocks = ['TQQQ','TECL']
    client = bigquery.Client()
    storage_client = storage.Client()
    broker_runner = BrokerRunner()
    startDateTime = datetime(2021, 10, 17)
    endDateTime = datetime(2021, 10, 26)
    for stock in stocks:

        get_df_from_bars(broker_runner.ib.reqHistoricalData(broker_runner.get_contract(stock), endDateTime, "1 M", "1 min", "TRADES", useRTH=1, timeout=300)).to_csv(rf"{main_dir}\{stock}.CSV")
        counter = 0
        his = broker_runner.ib.reqHistoricalTicks(contract=broker_runner.get_contract(stock), startDateTime=startDateTime, endDateTime=None, whatToShow='Trades', ignoreSize=True, numberOfTicks=1000, useRth=True)
        last_time = his[-1].time + timedelta(seconds=1)

        while True:
            counter+=1
            print(counter,his[-1].time)
            tmp = broker_runner.ib.reqHistoricalTicks(contract=broker_runner.get_contract(stock), startDateTime=last_time, endDateTime=None, whatToShow='Trades', ignoreSize=True, numberOfTicks=1000, useRth=True)
            his +=  tmp

            if last_time.date()>endDateTime.date():
                break
            if tmp==[]:
                last_time += timedelta(days=1) - timedelta(hours=last_time.hour)- timedelta(minutes=last_time.minute)- timedelta(seconds=last_time.second)
            else:
                last_time = his[-1].time + timedelta(seconds=1)


        pd.DataFrame([h._asdict() for h in his]).to_csv(rf"{main_dir}\Trades_{stock}.CSV")

        his = broker_runner.ib.reqHistoricalTicks(contract=broker_runner.get_contract(stock), startDateTime=startDateTime, endDateTime=None, whatToShow='Bid_Ask', ignoreSize=True, numberOfTicks=1000, useRth=True)
        while True:
            counter+=1
            print(counter,his[-1].time)
            tmp = broker_runner.ib.reqHistoricalTicks(contract=broker_runner.get_contract(stock), startDateTime=his[-1].time+timedelta(seconds=1), endDateTime=None, whatToShow='Bid_Ask', ignoreSize=True, numberOfTicks=1000, useRth=True)
            his +=  tmp
            if tmp==[]:
                break

        pd.DataFrame([h._asdict() for h in his]).to_csv(rf"{main_dir}\Bid_Ask_{stock}.CSV")

