import ib_insync,os,time,subprocess
from broker import config as broker_config
from datetime import datetime,timedelta
import pandas as pd

ib_ticker_mappings = pd.read_csv(f'{os.path.dirname(__file__)}/ib_ticker_mappings.csv')
ib_ticker_mappings = dict(zip(ib_ticker_mappings.ticker,ib_ticker_mappings.ib_ticker))


def get_ib_clean_ticker(ticker):
    if ib_ticker_mappings is not None and ticker in ib_ticker_mappings:
        return ib_ticker_mappings.get(ticker)
    return ticker.replace('-', ' ').replace('.', ' ')

def kill_broker():
    if os.name == 'nt':
        os.system('taskkill /im java.exe')
    else:
        os.system('killall -9 java')
    time.sleep(5)

def start_broker(host, port, clientId):
    try:
        ib = ib_insync.IB()
        while not ib.isConnected():
            ib.connect(host=host, port=port, clientId=clientId)
            ib.sleep(1)
        ib.disconnect()
    except:
        kill_broker()
        subprocess.call(broker_config.start_tws_path)

    while True:
        try:
            ib = ib_insync.IB()
            while not ib.isConnected():
                ib.connect(host=host, port=port, clientId=clientId)
                ib.sleep(0.1)
            return ib
        except:
            time.sleep(5)


class BrokerRunner():
    def __init__(self,host=broker_config.host,port=broker_config.port,clientId=broker_config.clientId):
        self.host = host
        self.port = port
        self.clientId = clientId
        self.connect()
        self.trades = []
        self.fa_group = 'amos'
        self.faMethod = 'Equal'

    def is_live(self):
        return self.ib.isConnected()

    def connect(self):
        self.ib = start_broker(host=self.host, port=self.port, clientId=self.clientId)

    def place_orders(self,contracts,orders,end_time=None):
        trades = []
        if end_time is None:
            end_time = datetime.now()+timedelta(days=1)

        for contract, cur_order in zip(contracts,orders):
            if not self.is_live():
                self.connect()
            if datetime.now()<end_time:
                if type(cur_order)==ib_insync.order.BracketOrder:
                    trd = []
                    for cur_cur_order in cur_order:
                        cur_cur_order.faGroup = self.fa_group
                        cur_cur_order.faMethod = self.faMethod
                        trd.append(self.ib.placeOrder(contract, cur_cur_order))
                    trades.append(trd)
                else:
                    cur_order.faGroup = self.fa_group
                    cur_order.faMethod = self.faMethod
                    trades.append(self.ib.placeOrder(contract, cur_order))
            else:
                trades.append(None)
        self.trades+=trades
        return trades

    @staticmethod
    def get_contract(ticker):
        contract = ib_insync.Stock(symbol=get_ib_clean_ticker(ticker), exchange='SMART', currency='USD')
        contract.primaryExchange = "ISLAND"
        return contract


if __name__ == '__main__':
    broker = BrokerRunner()
    broker.place_orders([],[])