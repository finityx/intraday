from collections import OrderedDict
from google.cloud import storage
import json,time

from data.stock_data import *
from data.crypto_data import *

from objective import objective
from data2net.data2net import Data2Net
from data2net.data2net_predictions_manager import Data2Net as Data2NetPredictionsManager
from net_creator.army import Army
from main_runner import Runner

from strategies.limits_pairs_strategy import LimitsPairsStrategy
from strategies.none_strategy import NoneStrategyGenerator
from strategies.trailing_sl_strategy import TrailingSLStrategyGenerator
from strategies.trailing_tp_strategy import TrailingTPStrategyGenerator
from strategies.limit_strategy import LimitGenerator
from strategies.limit_strategy_percentile import LimitStrategyPercentile

from losses.exp_loss import ExpLoss
from losses.profit   import Profit
from losses.profit_per_minute import ProfitPerMinute
from losses.profit_per_minute_invest_change import ProfitPerMinuteInvestChange
from losses.w_profit_per_minute_invest_change import WProfitPerMinuteInvestChange
from losses.sortino   import Sortino
from losses.shilo   import Shilo
from losses.shilo_invesst_changer import ShiloInvestChanger
from losses.ivar   import Ivar
from losses.limit_profit_per_minute_invest_change import LimitProfitPerMinuteInvestChange
from losses.limit_shilo_invesst_changer import LimitShiloInvestChanger
from core_architecture.core_archicture import BasicLstm

BUCKET_NAME = 'free_market_intraday'

def get_strategies():
    strategies = OrderedDict()
    strategies['LimitsPairsStrategy'] = LimitsPairsStrategy
    strategies['NoneStrategy']        = NoneStrategyGenerator
    strategies['TrailingSLStrategy']  = TrailingSLStrategyGenerator
    strategies['TrailingTPStrategy']  = TrailingTPStrategyGenerator
    strategies['LimitStrategy']  = LimitGenerator
    strategies['LimitStrategyPercentile'] = LimitStrategyPercentile


    return strategies

def get_losses():
    losses = OrderedDict()
    losses['ExpLoss'] = ExpLoss
    losses['Profit']  = Profit
    losses['Sortino'] = Sortino
    losses['Shilo'] = Shilo
    losses['Ivar'] = Ivar
    losses['ProfitPerMinute'] = ProfitPerMinute
    losses['ProfitPerMinuteInvestChange'] = ProfitPerMinuteInvestChange
    losses['ShiloInvestChanger'] = ShiloInvestChanger
    losses['LimitProfitPerMinuteInvestChange'] = LimitProfitPerMinuteInvestChange
    losses['LimitShiloInvestChanger'] = LimitShiloInvestChanger

    return losses

def get_core_architures():
    losses = OrderedDict()
    losses['BasicLstm'] = BasicLstm
    return losses


class RunParameters():
    def __init__(self,asset='STOCK'):
        if asset == 'STOCK':
            n_minutes_in_day = 390
        elif asset == 'CRYPTO':
            n_minutes_in_day = 1441
        else:
            assert False,'asset must be CRYPTO/STOCK'

        self.outer_day_predictions_bucket = None
        self.outer_day_predictions_prefix = None
        self.outer_day_trading_mode = 'open2close'
        self.n_minutes_in_day = n_minutes_in_day
        self.asset = asset
        self.run_name = 'default'
        self.objectives = []
        self.max_epochs = 5000
        self.mode_to_save=20
        self.ndays = 1
        self.strategy = ('TrailingSLStrategy',{'trailing_sl':[0.002,0.004,0.008,0.016],'tp':[1,1,1,1]})
        self.core_arch = ('BasicLstm',{'start_dense_size':10,'ticker_embedding':20,'lstm_size':10,'mid_dense_size':[10,10],'out_dense_size':10,'stateful':False})
        self.batch_size = 100
        self.shuffle = False
        self.obj_checkpoint = None

class RunObjective(dict):
    def __init__(obj):
        super(RunObjective, obj).__init__()
        obj['name'] = 'default'
        obj['aum'] = 10000
        obj['loss'] = 'Profit'
        obj['delay'] = 2
        obj['comis_per_stock'] = 0.003
        obj['min_comis']     = 1
        obj['max_concentration'] = 1
        obj['max_invest_to_ratio'] = 0.05
        obj['spread'] = 0.01/100
        obj['mode'] = 'open2close'

def get_model(main_dir,gui_dict,tickers,start_date=datetime(2018,1,1),end_date=datetime(2021,6,7),replace_dates=[],update_table_date=None,live=False):
    losses_dict = get_losses()
    objectives = []
    for obj in gui_dict['objectives']:
        if gui_dict['asset'] == 'CRYPTO':
            c_objective = objective.Objective(name=obj['name'], label=CryptoLabel(delay=obj['delay']))
        elif gui_dict['asset'] == 'STOCK':
            c_objective = objective.Objective(name=obj['name'], label=StockLabel(delay=obj['delay']))
        else:
            assert False,'asset must be CRYPTO/STOCK'

        c_objective.aum = obj['aum']
        c_objective.comis_per_stock = obj['comis_per_stock']
        c_objective.spread = obj['spread']
        c_objective.min_comis = obj['min_comis']
        c_objective.max_concentration = obj['max_concentration']
        c_objective.max_invest_to_ratio = obj['max_invest_to_ratio']
        c_objective.mode = obj['mode']
        c_objective.loss = losses_dict[obj['loss']]
        objectives.append(c_objective)


    strategies_dict = get_strategies()

    strategy_name,strategy_params =  gui_dict['strategy']
    strategy = strategies_dict[strategy_name](**strategy_params)


    if gui_dict['asset'] == 'CRYPTO':
        data_sources = [CryptoData()]
    elif gui_dict['asset'] == 'STOCK':
        data_sources = [StockData()]
    else:
        assert False,'asset must be CRYPTO/STOCK'

    if gui_dict['outer_day_predictions_bucket'] is None:
        d2n = Data2Net(main_path=main_dir,tickers=tickers,ndays=gui_dict['ndays'],start_date=start_date.date().isoformat(),end_date=end_date.date().isoformat(),data_sources=data_sources,
                       objectives=objectives,replace_dates=[r.date() for r in replace_dates],
                       strategy_generators= strategy,asset=gui_dict['asset'],live=live,n_minutes_in_day=gui_dict['n_minutes_in_day'])
    else:
        d2n = Data2NetPredictionsManager(main_path=main_dir,tickers=tickers,ndays=gui_dict['ndays'],start_date=start_date.date().isoformat(),end_date=end_date.date().isoformat(),data_sources=data_sources,
                       objectives=objectives,replace_dates=[r.date() for r in replace_dates],
                       strategy_generator= strategy,asset=gui_dict['asset'],live=live,n_minutes_in_day=gui_dict['n_minutes_in_day'],
                       outer_day_predictions_bucket=gui_dict['outer_day_predictions_bucket'],outer_day_predictions_prefix=gui_dict['outer_day_predictions_prefix'],
                       outer_day_trading_mode=gui_dict['outer_day_trading_mode'])

    if update_table_date is not None:
        d2n.update_table(update_table_date)

    for obj in objectives:
        obj.loss = obj.loss(d2n,obj)

    d2n.get()
    d2n.transform()

    core_arch = get_core_architures()[gui_dict['core_arch'][0]](**gui_dict['core_arch'][1])

    army = Army(d2n,core_arch,batch_size=gui_dict['batch_size'])
    runner = Runner(army)
    return {'runner':runner,'core_architure':core_arch,'data2net':d2n}


def main(main_dir,gui_dict,tickers,bucket_name = BUCKET_NAME):

    with open(f'{main_dir}/run.json', 'w+') as f:
        json.dump({'gui_dict':gui_dict,'tickers':tickers}, f)

    client = storage.Client()
    if bucket_name is not None:
        bucket = client.bucket(bucket_name)
        bucket.blob(f"{gui_dict['run_name']}/run.json").upload_from_filename(f'{main_dir}/run.json')
        if gui_dict['obj_checkpoint'] is not None:
            os.makedirs(fr"{main_dir}/run_saves/checkpoints/{gui_dict['obj_checkpoint']}",exist_ok=True)
            for blob in bucket.list_blobs(prefix=f"{gui_dict['run_name']}/checkpoints/{gui_dict['obj_checkpoint']}"):
                print(f"downloading {main_dir}/run_saves/checkpoints/{gui_dict['obj_checkpoint']}/{os.path.basename(blob.name)}")
                blob.download_to_filename(fr"{main_dir}/run_saves/checkpoints/{gui_dict['obj_checkpoint']}/{os.path.basename(blob.name)}")

    model = get_model(main_dir,gui_dict,tickers)

    model['runner'].train(fr'{main_dir}/run_saves',datetime(2015,1,1),datetime(2019,1,1),datetime(2020,1,1),shuffle=gui_dict['shuffle'],n_tosave=gui_dict['mode_to_save']
                 ,max_epochs=gui_dict['max_epochs'],bucket_name=bucket_name,prefix=gui_dict['run_name'],obj_checkpoint=gui_dict['obj_checkpoint'])

def live_run(main_dir,gui_dict,tickers,checkpoint_path,bucket_name = BUCKET_NAME,curr_date = datetime.now(),replace_dates=None):
    if replace_dates is None:
        replace_dates = [curr_date]
    gui_dict['batch_size'] = 1
    model = get_model(main_dir,gui_dict,tickers,start_date=min(replace_dates),end_date=max(replace_dates),replace_dates=replace_dates,update_table_date=curr_date,live=True)
    assert model['data2net'].dates[-1] == curr_date.date(),'curr_date not last'
    os.makedirs(f'{main_dir}/live_prediction',exist_ok=True)
    invests = model['runner'].predict_live(save_dir=f'{main_dir}/live_prediction',checkpoint_path=checkpoint_path,ds_start_date=curr_date,ds_end_date=curr_date)
    return model,invests

def run_prediction(main_dir,gui_dict,tickers,checkpoint_path,bucket_name = BUCKET_NAME):
    gui_dict['batch_size'] = 1
    model = get_model(main_dir,gui_dict,tickers,replace_dates=[datetime.now()])
    os.makedirs(f'{main_dir}/prediction',exist_ok=True)
    invests = model['runner'].predict(f'{main_dir}/prediction',checkpoint_path,datetime(2018,1,1),datetime(2019,1,1),datetime(2020,1,1))
    return model,invests

if __name__=='__main__':

    tickers = []

    client = storage.Client()

    main_dir = 'D:/intraday_spy'
    os.makedirs(main_dir, exist_ok=True)

    tickers = ["SPY",'cash']

    sz = 20
    run_parameters = RunParameters()
    run_parameters.core_arch = ('BasicLstm', {'start_dense_size': [sz], 'ticker_embedding': sz, 'lstm_size': sz, 'out_dense_size': [sz, sz], 'stateful': False})

    obj = RunObjective()

    obj['aum'] = 10
    obj['loss'] = 'Profit'
    obj['comis_per_stock'] = 0.0
    obj['spread'] = 0.01

    run_parameters.objectives.append(obj)
    run_parameters.strategy = ('TrailingSLStrategy',{'trailing_sl':[0.001,0.002,0.004],'tp':[1,1,1]})

    run_parameters.run_name = 'tmp'

    main(main_dir=main_dir, gui_dict=run_parameters.__dict__, tickers=tickers, bucket_name=None)
