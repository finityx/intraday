from __future__ import absolute_import

import copy
import os
import queue
import threading
import time
from distutils.util import strtobool
from kafka import KafkaConsumer, KafkaProducer
from kafka import TopicPartition
from kafka.structs import OffsetAndMetadata
import logging

logger = logging.getLogger("logger")


class KafkaMessage:
    def __init__(self, message):
        self.message = message


class StreamTopic:
    def __init__(self, topic, followup_topic=None):
        self.topic = topic
        self.followup_topic = followup_topic


class Kafka:
    def __init__(self, stream_topics, group_id, timeout=15000):
        self.timeout = timeout
        self.stream_topics = stream_topics
        topics = [st.topic for st in stream_topics if st.topic is not None]
        kafka_logger = logging.getLogger('kafka')
        kafka_logger.setLevel(logging.WARN)

        kafka_conf, producer_conf = self.get_kafka_config(group_id)

        def to_bytes(x):
            if type(x) is str:
                return x.encode('utf-8')
            else:
                return x

        producer_conf['value_serializer'] = to_bytes
        self.confs = {'consumer': kafka_conf, 'producer': producer_conf}

        self.producer = KafkaProducer(**self.confs['producer'])
        self.q = queue.Queue()
        self.tps = {}
        if topics:
            self.consumer = KafkaConsumer(*topics, **self.confs['consumer'])
            self.start()
            logger.info('consumers running')

    def get_kafka_config(self, group_id):
        logger.info("using kafka config with env variables")
        conf = {'bootstrap_servers': os.getenv('KAFKA_HOST'),
                'security_protocol': 'SASL_SSL',
                'sasl_mechanism': 'PLAIN',
                'sasl_plain_username': os.getenv('KAFKA_USERNAME'),
                'sasl_plain_password': os.getenv('KAFKA_PASSWORD'),
                'retry_backoff_ms': 500,
                'api_version': (0, 10, 2)}

        producer_conf = copy.copy(conf)

        conf['group_id'] = 'sourcerer.%s.%s' % (group_id, os.getenv('USER'))
        conf['enable_auto_commit'] = bool(strtobool(os.getenv('KAFKA_AUTO_COMMIT'))) if os.getenv(
            'KAFKA_AUTO_COMMIT') else True
        conf['session_timeout_ms'] = self.timeout
        conf['max_poll_interval_ms'] = self.timeout * 4
        conf['request_timeout_ms'] = self.timeout * 4 + 5000
        conf['connections_max_idle_ms'] = self.timeout * 4 + 10000
        conf['auto_offset_reset'] = os.getenv('KAFKA_OFFSET') if os.getenv('KAFKA_OFFSET') else 'latest'
        # logger.info(json.dumps(conf, indent=4))
        return conf, producer_conf

    def run(self):
        while True:
            message = self.consumer.next_v2()
            if message.partition not in self.tps:
                self.tps[message.topic + str(message.partition)] = TopicPartition(message.topic, message.partition)
            logger.info("putting message in queue")
            self.q.put(KafkaMessage(message))

    def start(self):
        t = threading.Thread(target=self.run)
        t.start()

    def get_data(self):
        msg = self.q.get()
        return msg

    def send(self, topic, value):
        retry = 0
        retries = 5
        while retry < retries:
            retry += 1
            try:
                logger.info("trying to send to topic %s" % topic)
                self.producer.send(topic, value)
                self.producer.flush()
                logger.info("message sent successfully")
                break
            except Exception as e:
                logger.exception("Error in kafka producer sending topic %s" % topic)
                time.sleep(1)

    def mark_done(self, msg: KafkaMessage, value=None):
        logger.info("committing partition %d, offset %d" % (msg.message.partition, msg.message.offset))
        tp = self.tps[msg.message.topic + str(msg.message.partition)]
        self.consumer.commit({
            tp: OffsetAndMetadata(msg.message.offset + 1, None)
        })
        followup_topic = self.get_followup_topic(msg.message.topic)
        if msg.message.topic is not None and value is not None and followup_topic is not None:
            self.producer.send(followup_topic, value)

    def get_followup_topic(self, topic):
        return [st.followup_topic for st in self.stream_topics if st.topic == topic][0]
