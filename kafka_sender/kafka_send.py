import os, json
from kafka_sender.kafka_client import StreamTopic, Kafka
from data_classes.message import validate_name, TradingEnv


def get_app_name():
    return 'intraday'

def get_topic():
    return 'pod-automation.main'


def send_dict(inp_dict, run_name, trading_env: TradingEnv = TradingEnv.TEST):
    stream_topics = [StreamTopic(get_topic())]
    kafka = Kafka(stream_topics, get_app_name())

    assert 'alg_type' in inp_dict, 'message must have alg_type'

    assert validate_name(run_name.lower().replace('_', ''), get_app_name(), trading_env)
    if 'check_point_prefix' not in inp_dict:
        model_name = 'UNKNOWN'
    else:
        model_name = (inp_dict['script_params']['check_point_prefix'] if 'check_point_prefix' in inp_dict['script_params'] else 'UNKNOWN')
    inp_dict['spec'] = {"id": run_name.lower().replace('_', ''), 'appName': get_app_name(), 'tradingEnv': trading_env.value, 'modelName': model_name}
    if os.getenv('ENABLE_ALERTS') is not None:
        inp_dict['spec']['alert'] = {'enabled': True, 'channel': '#free-market-alerts'}

    kafka.send(get_topic(), json.dumps(inp_dict))
    print("SENDING...")
    print(json.dumps(inp_dict))