from google.cloud import bigquery
import platform,os
if platform.system()=='Windows':
    pass
else:
    pass
from datetime import datetime

def gen_curr_table_name(curr_date_time):
    return f'algo_dataset.intraday_1min_crypto_binance_{curr_date_time.date().isoformat()}'

client = bigquery.Client()
TABLE_NAME = 'algo_dataset.intraday_1min_crypto_binance_usdt_new'

class CryptoData():
    def __init__(self,name='crypto_data',table_name=TABLE_NAME):
        self.table_name = table_name
        self.name = name
        self.params = ['open', 'close', 'high', 'low', 'average','turnover','div_price']

    def get(self,main_path,tickers,dates,query_handler = None):
        for t in tickers:
            for d in dates:
                qstr = f"select open_time as date,ticker,open,close,high,low,(open+close)/2 as average,volume/100 as volume, from (select distinct * from `{self.table_name}` where ticker = '{t}' and extract(date from open_time)='{d}' order by open_time) "
                query_handler.append_qstr_path(qstr=qstr, path=f'{main_path}/{t}_{d}.csv', date=d, ticker=t, data_name=self.name)

    def transform(self,data_dir,strategy_generators,tranformations_handler):
        strategy_keys = []
        for strategy_generator in strategy_generators:
            for strategy_name, strategy_params in strategy_generator.get_strategies_params():
                strategy_keys += list(strategy_params.keys())

        self.params+=sorted(list(set(strategy_keys)))

        for file in os.listdir(f'{data_dir}'):
            ticker = file.split('_')[0]
            date = datetime.fromisoformat(file.split('_')[1].split('.')[0]).date()
            for strategy_generator in strategy_generators:
                for strategy_name,strategy_params in strategy_generator.get_strategies_params():
                    tranformations_handler.add_job(ticker = ticker , strategy_name = f'{strategy_name}' ,date = date,data_name=self.name,fcn = strategy_generator.transform_stock_data,fcn_args={'params':self.params,'strategy_params':strategy_params,'filename':f'{data_dir}/{file}'})

    def update_table(self,curr_date_time):
        self.table_name = gen_curr_table_name(curr_date_time)

class CryptoLabel():
    def __init__(self,delay,name='crypto_label',table_name=TABLE_NAME):
        self.table_name = table_name
        self.name = name
        self.delay = delay
        self.params = ['open2close', 'close2close', 'close2open',
                       'change_open2close', 'change_close2close', 'change_close2open',
                       'div_price_open2close','div_price_close2close','div_price_close2open',
                       'turnover','duration']
    def get(self,main_path,tickers,dates,query_handler = None):
        for t in tickers:
            for d in dates:
                qstr = f"select open_time as date,ticker,open,close,high,low,(open+close)/2 as average,volume/100 as volume, from (select distinct * from `{self.table_name}` where ticker = '{t}' and extract(date from open_time)='{d}' order by open_time) "
                query_handler.append_qstr_path(qstr=qstr, path=f'{main_path}/{t}_{d}.csv', date=d, ticker=t, data_name=self.name)

    def transform(self,data_dir,strategy_generators,tranformations_handler):
        for file in os.listdir(f'{data_dir}'):
            ticker = file.split('_')[0]
            date = datetime.fromisoformat(file.split('_')[1].split('.')[0]).date()
            for strategy_generator in strategy_generators:
                for strategy_name, strategy_params in strategy_generator.get_strategies_params():
                    tranformations_handler.add_job(ticker=ticker, strategy_name=f'{strategy_name}', date=date, data_name=self.name, fcn=strategy_generator.transform_stock_label, fcn_args={'delay':self.delay,'params':self.params,'strategy_params':strategy_params,'filename':f'{data_dir}/{file}'})

    def update_table(self,curr_date_time):
        self.table_name = gen_curr_table_name(curr_date_time)
