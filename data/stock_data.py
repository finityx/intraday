from google.cloud import bigquery
import platform,os
if platform.system()=='Windows':
    pass
else:
    pass
from datetime import datetime
import numpy as np

def gen_curr_table_name(curr_date_time):
    return f'INTRADAY_LIVE.intraday_1min_{curr_date_time.date().isoformat()}'

client = bigquery.Client()
TABLE_NAME = 'KIRA.intraday_1min_full_clean'

class StockData():
    def __init__(self,name='stock_data',table_name=TABLE_NAME):
        self.table_name = table_name
        self.name = name
        self.params = ['open', 'close', 'high', 'low', 'average','turnover','div_price','close2open_yesterday']

    def get(self,main_path,tickers,dates,query_handler = None):
        for t in tickers:
            for d in dates:
                # qstr = f"select distinct * from `{self.table_name}` where ticker = '{t}' and extract(date from date)='{d}' order by date "
                qstr = f"""
                SELECT distinct * from 
                (select * from `{self.table_name}`  where ticker = '{t}' and extract(date from date)='{d}' order by date ) as a
                join 
                (
                (
                SELECT close as last_close ,date as last_date FROM `{self.table_name}`  where ticker = '{t}' and extract(date from date)<'{d}' order by date desc limit 1
                ) as b
                join 
                (
                SELECT open as next_open ,date as next_date FROM `{self.table_name}`  where ticker = '{t}' and extract(date from date)>'{d}' order by date asc limit 1
                ) as c
                on True
                )
                on True
                """

                query_handler.append_qstr_path(qstr=qstr, path=f'{main_path}/{t}_{d}.csv', date=d, ticker=t, data_name=self.name)

    def transform(self,data_dir,strategy_generator,tranformations_handler):

        for file in os.listdir(f'{data_dir}'):
            ticker = file.split('_')[0]
            date = datetime.fromisoformat(file.split('_')[1].split('.')[0]).date()
            tranformations_handler.add_job(ticker = ticker ,date = date,data_name=self.name,fcn = strategy_generator.transform_stock_data,fcn_args={'params':self.params,'filename':f'{data_dir}/{file}'})


    def transform_prediction_mgr(self,data_dir,strategy_generators,ticker_invests_per_date,tranformations_handler):
        strategy_keys = []
        for strategy_generator in strategy_generators:
            for strategy_name, strategy_params in strategy_generator.get_strategies_params():
                strategy_keys += list(strategy_params.keys())

        self.params+=sorted(list(set(strategy_keys)))

        for file in os.listdir(f'{data_dir}'):
            ticker = file.split('_')[0]
            date = datetime.fromisoformat(file.split('_')[1].split('.')[0]).date()

            for strategy_generator in strategy_generators:
                if (date in ticker_invests_per_date) and (ticker in ticker_invests_per_date[date]):
                    c_invest = ticker_invests_per_date[date][ticker]
                    strategy_generator.sign = [np.sign(c_invest)]
                    strategy_generator.weight = abs(c_invest) + 1e-10

                for strategy_name,strategy_params in strategy_generator.get_strategies_params():
                    tranformations_handler.add_job(ticker = ticker , strategy_name = f'{strategy_name}' ,date = date,data_name=self.name,fcn = strategy_generator.transform_stock_data,fcn_args={'params':self.params,'strategy_params':strategy_params,'filename':f'{data_dir}/{file}'})

    def update_table(self,curr_date_time):
        self.table_name = gen_curr_table_name(curr_date_time)

class StockLabel():
    def __init__(self,delay,name='stock_label',table_name=TABLE_NAME):
        self.table_name = table_name
        self.name = name
        self.delay = delay
        self.params = ['open2close', 'close2close', 'close2open','close2open_tomorrow',
                       'change_open2close', 'change_close2close', 'change_close2open',
                       'div_price_open2close','div_price_close2close','div_price_close2open',
                       'turnover','duration','weight']

    def get(self,main_path,tickers,dates,query_handler = None):
        for t in tickers:
            for d in dates:
                # qstr = f"select distinct * from `{self.table_name}` where ticker = '{t}' and extract(date from date)='{d}' order by date "
                qstr = f"""
                SELECT distinct * from 
                (select * from `{self.table_name}`  where ticker = '{t}' and extract(date from date)='{d}' order by date ) as a
                join 
                (
                (
                SELECT close as last_close ,date as last_date FROM `{self.table_name}`  where ticker = '{t}' and extract(date from date)<'{d}' order by date desc limit 1
                ) as b
                join 
                (
                SELECT open as next_open ,date as next_date FROM `{self.table_name}`  where ticker = '{t}' and extract(date from date)>'{d}' order by date asc limit 1
                ) as c
                on True
                )
                on True
                """
                query_handler.append_qstr_path(qstr=qstr, path=f'{main_path}/{t}_{d}.csv', date=d, ticker=t, data_name=self.name)

    def transform(self,data_dir,strategy_generator,tranformations_handler):
        for file in os.listdir(f'{data_dir}'):
            ticker = file.split('_')[0]
            date = datetime.fromisoformat(file.split('_')[1].split('.')[0]).date()
            tranformations_handler.add_job(ticker=ticker, date=date, data_name=self.name, fcn=strategy_generator.transform_stock_label, fcn_args={'delay':self.delay,'params':self.params,'filename':f'{data_dir}/{file}'})

    def transform_prediction_mgr(self, data_dir, strategy_generators, ticker_invests_per_date, tranformations_handler):
        for file in os.listdir(f'{data_dir}'):
            ticker = file.split('_')[0]
            date = datetime.fromisoformat(file.split('_')[1].split('.')[0]).date()
            for strategy_generator in strategy_generators:
                if (date in ticker_invests_per_date) and (ticker in ticker_invests_per_date[date]):
                    c_invest = ticker_invests_per_date[date][ticker]
                    strategy_generator.sign = [np.sign(c_invest)]
                    strategy_generator.weight = abs(c_invest) + 1e-10

                for strategy_name, strategy_params in strategy_generator.get_strategies_params():
                    tranformations_handler.add_job(ticker=ticker, strategy_name=f'{strategy_name}', date=date, data_name=self.name, fcn=strategy_generator.transform_stock_label, fcn_args={'delay':self.delay,'params':self.params,'strategy_params':strategy_params,'filename':f'{data_dir}/{file}'})

    def update_table(self,curr_date_time):
        self.table_name = gen_curr_table_name(curr_date_time)
