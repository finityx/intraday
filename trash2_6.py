from live import run_live_models
from broker import broker_runner
broker_runner = broker_runner.BrokerRunner()
position_handler = run_live_models.PositionHandler(main_dir=r'D:\tmp', broker_runner=broker_runner, trail_stop_loss=False, handle_positions_separately=False)

prdict = {'tickers':['AAPL','AAPL'],'strategies_params':[{'sign':1,'trailing_sl':0.002},{'sign':-1,'trailing_sl':0.002}],'invests_dollar':[10000,0],'max_iminute':1}
for k in range(10):
    position_handler.buy_prediction(prdict,'tmp')
    position_handler.handle_position()