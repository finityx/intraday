from losses.main_losses import Losses
import tensorflow as tf
class Profit(Losses):
    def __init__(self,data2net, objective):
        super().__init__(data2net, objective,'profit')
        self.calc_loss = {'Army':self.generate_army_loss,'AllocStockChanger':self.generate_alloc_stock_changer_loss}

    def generate_army_loss(self,y_pred, y_true):
        # def profit(y_pred, y_true):
        min_prf = self.get_profit_army(y_pred, y_true)
        return -tf.reduce_mean(tf.reduce_sum(min_prf,-1))*100

    def generate_alloc_stock_changer_loss(self, y_pred, y_true):
        min_prf = self.get_profit_alloc_changer(y_pred, y_true)
        return -tf.reduce_mean(tf.reduce_sum(min_prf, -1)) * 100
