import tensorflow as tf
import copy,numpy as np,scipy.special

class Losses():
    def __init__(self,data2net,objective,name):
        self.data2net = data2net
        self.objective = objective
        self.name = name
        self.calc_profit = {'Army':self.get_profit_army}
        self.get_invest_dict  = {'Army':self.get_invest_dict_army}


    def get_rel_label_comis(self,y_true,mode):
        label_params = self.objective.label.params
        div_price = y_true[:, :, :, :, label_params.index(f'div_price_{mode}')]
        label     = y_true[:, :, :, :, label_params.index(mode)]
        duration = y_true[:, :, :, :, label_params.index('duration')]
        change_label = y_true[:, :, :,:, label_params.index(f'change_{mode}')]
        if self.data2net.asset == 'STOCK':
            comis = self.objective.comis_per_stock*div_price
        elif self.data2net.asset == 'CRYPTO':
            # crypto comis is in percent of invest
            comis = self.objective.comis_per_stock*tf.ones_like(div_price)
        else:
            assert False, 'asset must be CRYPTO/STOCK'
        return label,comis,duration,change_label

    def get_invest_dict_army(self, y_pred, y_true):
        label_params = self.objective.label.params
        label, comis,duration,change_label = self.get_rel_label_comis(y_true, self.objective.mode)

        weight = y_true[:, :, :, label_params.index('weight')]
        turnover = tf.expand_dims(y_true[:, :, :, label_params.index('turnover')],-1)/(1e-10+tf.expand_dims(weight, -1))

        invest_dict = {}
        invest_dict['label']   = label
        invest_dict['comis']   = comis
        invest_dict['duration'] = duration
        invest_dict['change_label'] = change_label

        yp = y_pred[:, :, :, :, 1]
        ryp = tf.reshape(yp,[yp.shape[0],-1,yp.shape[-1]])
        invest_dict['invests'] = tf.reshape(tf.nn.softmax(ryp,1),[s for s in yp.shape])

        yp2 = y_pred[:, :, :, :, 2]
        invests_use_new_long  = tf.concat([yp2[:, :, isp:(isp+1)] for isp, sp in enumerate(self.data2net.strategies_params) if sp['sign'] == 1] , -2)
        invests_use_new_short = tf.concat([yp2[:, :, isp:(isp+1)] for isp, sp in enumerate(self.data2net.strategies_params) if sp['sign'] == -1] , -2)

        invests_use_new_long = tf.nn.softmax(invests_use_new_long, 2)
        invests_use_new_short = tf.nn.softmax(invests_use_new_short, 2)

        kl,ks = 0,0
        invests_use_new = []
        for isp, sp in enumerate(self.data2net.strategies_params):
            if sp['sign'] == 1:
                invests_use_new.append(invests_use_new_long[:,:,kl:(kl+1)])
                kl += 1

            else:
                invests_use_new.append(invests_use_new_short[:,:,ks:(ks+1)])
                ks += 1
        invests_use_new = tf.concat(invests_use_new,-2)


        invest_dict['invests_use_new'] = invests_use_new
        return invest_dict

    def get_profit_army(self,y_pred, y_true):
        invest_dict = self.get_invest_dict_army(y_pred, y_true)

        # clean_min_prof = tf.reduce_sum(tf.reduce_sum(invest_dict['invests']*(invest_dict['label']),2),1)
        ncash = [1.0 if ticker is not 'cash' else 0.0 for ticker in self.data2net.tickers] + [0.0 if ticker is not 'cash' else 0.0 for ticker in self.data2net.tickers]

        not_cash = (tf.reshape(tf.concat(ncash,0),[1,-1,1,1]))
        # aum_per_minute = 1/(1+tf.reduce_max(tf.reduce_sum(invest_dict['invests']*not_cash,[1,2,3])))
        aum_per_minute = 0.01
        #
        # yearly_prof = tf.maximum(100*aum_per_minute * tf.reduce_mean(tf.reduce_sum(clean_min_prof,1)) * 251,0.0)
        # yearly_prof = tf.clip_by_value(yearly_prof / 4 * 0.01 / 100, 0.0, 0.06 / 100)
        # slipage = tf.stop_gradient(not_cash*yearly_prof)
        slipage = self.objective.spread*not_cash
        #aum_per_minute = 0.01
        min_prof = aum_per_minute*tf.reduce_sum(tf.reduce_sum(invest_dict['invests']*(invest_dict['label']- 2*invest_dict['comis'] -slipage),2),1)

        return min_prof


    def get_profit_and_duration_army(self,y_pred, y_true):
        invest_dict = self.get_invest_dict_army(y_pred, y_true)
        min_prof     = tf.reduce_sum(invest_dict['invests']*(invest_dict['label']- 2*invest_dict['comis'] - self.objective.spread),1)
        min_duration = tf.reduce_sum(invest_dict['invests']*(invest_dict['duration']+1),1)
        return min_prof,min_duration



    def get_profit_army_invest_changer(self,y_pred, y_true):
        invest_dict = self.get_invest_dict_army(y_pred, y_true)
        labels = []
        ncash = [1.0 if ticker is not 'cash' else 0.0 for ticker in self.data2net.tickers] + [0.0 for ticker in self.data2net.tickers]
        not_cash = (tf.reshape(tf.concat(ncash,0),[1,-1,1]))

        prev_invests = 0.0*tf.stop_gradient(invest_dict['label'][:,:,:,0])
        for kmin in range(invest_dict['invests'].shape[-1]):
            invests = invest_dict['invests'][:,:,:,kmin]*not_cash
            duration = invest_dict['duration'][:,:,:,kmin]

            label = invest_dict['label'][:,:,:,kmin]
            change_label = invest_dict['change_label'][:,:,:,kmin]
            comis = invest_dict['comis'][:,:,:,kmin]

            change_profit = invests * tf.where(duration != 1,change_label,label)
            replace_cost = - (comis + self.objective.spread / 2) * abs(invests - prev_invests)

            delta = tf.reduce_sum(change_profit +  replace_cost,[1,2])
            labels.append(tf.expand_dims(delta,-1))
            prev_invests = tf.where(duration != 1, invests, 0.0)


        min_prof = tf.concat(labels, -1)
        return min_prof


    def get_profit_army_invest_changer_limit(self,y_pred, y_true):

        invest_dict = self.get_invest_dict_army(y_pred, y_true)
        labels = []
        not_cash = (tf.reshape(tf.concat([1.0 if ticker is not 'cash' else 0.0 for ticker in self.data2net.tickers],0),[1,-1,1]))
        prev_invests = 0.0*tf.stop_gradient(invest_dict['label'][:,:,:,0])
        prev_long_invests = 0.0*tf.stop_gradient(invest_dict['label'][:,:,0,0])
        prev_short_invests = 0.0*tf.stop_gradient(invest_dict['label'][:,:,0,0])

        curr_run = '2_1'

        for kmin in range(invest_dict['invests'].shape[-1]):
            invests = invest_dict['invests'][:,:,:,kmin]
            invests_use_new = invest_dict['invests_use_new'][:,:,:,kmin]
            duration = invest_dict['duration'][:,:,:,kmin]
            label = invest_dict['label'][:,:,:,kmin]
            change_label = invest_dict['change_label'][:,:,:,kmin]
            comis = invest_dict['comis'][:,:,:,kmin]

            if curr_run=='2_2':
                prev_keep = tf.where(invests<prev_invests,invests,prev_invests)
                extra_invest = invests - prev_keep
                change_profit = prev_keep*change_label + extra_invest*label
                replace_cost = - (comis + self.objective.spread / 2) * abs(invests - prev_invests)

                delta = tf.reduce_sum(change_profit +  replace_cost,[1,2])
                labels.append(tf.expand_dims(delta,-1))
                prev_invests = tf.where(duration != 1, extra_invest, 0.0) + prev_keep

            if curr_run=='2_4':
                extra_invest = invests*(1 - tf.reduce_sum(prev_invests,2,True))
                change_profit = prev_invests*change_label + extra_invest*label
                replace_cost = - (comis + self.objective.spread / 2) * abs(invests - prev_invests)

                delta = tf.reduce_sum(change_profit +  replace_cost,[1,2])
                labels.append(tf.expand_dims(delta,-1))
                prev_invests = tf.where(duration != 1, extra_invest, 0.0) + prev_invests

            elif curr_run=='2_3':
                invests = invests*not_cash

                change_profit = prev_invests*change_label + invests*label
                replace_cost = - (comis + self.objective.spread / 2) * abs(invests - prev_invests)

                delta = tf.reduce_sum(change_profit +  replace_cost,[1,2])
                labels.append(tf.expand_dims(delta,-1))
                prev_invests = tf.where(duration != 1, invests, 0.0) + prev_invests
                prev_invests = prev_invests/tf.maximum(1.0,tf.reduce_sum(prev_invests,[1,2],True))

            elif curr_run=='2_1':

                change_label_long  = [change_label[:, :, isp]  for isp, sp in enumerate(self.data2net.strategies_params) if sp['sign'] == 1][0]
                change_label_short = [change_label[:, :, isp] for isp, sp in enumerate(self.data2net.strategies_params) if sp['sign'] == -1][0]
                prev_invest_signed = prev_long_invests - prev_short_invests

                currently_invest_curr = tf.expand_dims(tf.reduce_sum(abs(prev_invest_signed), 1, True), 1)

                max_short = tf.where(prev_invest_signed > 0,  prev_invest_signed, 0.0)
                max_long  = tf.where(prev_invest_signed < 0, -prev_invest_signed, 0.0)

                free_invest = tf.concat([tf.expand_dims(max_long,-1) if sp['sign'] == 1 else tf.expand_dims(max_short,-1) for isp, sp in enumerate(self.data2net.strategies_params)],-1)
                invests = invests * not_cash

                invests = (1-currently_invest_curr)*invests + free_invest*invests_use_new

                change_profit = prev_long_invests*change_label_long + prev_long_invests*change_label_short + tf.reduce_sum(invests*label,-1)
                replace_cost = - tf.reduce_sum((comis + self.objective.spread / 2) * abs(invests - prev_invests),-1)

                delta = tf.reduce_sum(change_profit +  replace_cost,[1])
                labels.append(tf.expand_dims(delta,-1))
                i1 =  tf.where(duration > 1, invests, 0.0)
                prev_long_invests  += sum([i1[:, :, isp]  for isp, sp in enumerate(self.data2net.strategies_params) if sp['sign'] == 1])
                prev_short_invests += sum([i1[:, :, isp]  for isp, sp in enumerate(self.data2net.strategies_params) if sp['sign'] == -1])



        min_prof = tf.concat(labels, -1)
        return min_prof