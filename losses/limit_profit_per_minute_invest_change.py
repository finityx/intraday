import numpy as np
from losses.main_losses import Losses
import tensorflow as tf
class LimitProfitPerMinuteInvestChange(Losses):
    def __init__(self,data2net, objective):
        super().__init__(data2net, objective,'limit_profit_per_minute_invest_change')
        self.calc_loss = {'Army':self.generate_army_loss}

    def generate_army_loss(self,y_pred, y_true):
        min_prf = self.get_profit_army_invest_changer_limit(y_pred, y_true)*100
        day_prof = tf.reduce_sum(min_prf,-1)
        #
        #
        loss = -tf.reduce_mean(day_prof)
        #
        # # ## 2.4
        # # s_day_prof = tf.sort(day_prof)
        # # zero_like = tf.where(tf.math.cumsum(s_day_prof) <= 10,s_day_prof,0.0)
        # # loss = -tf.reduce_mean(zero_like)
        #
        # # 2.2
        # day_prof = day_prof-0.5
        # loss = -tf.reduce_mean(tf.where(day_prof<=0,day_prof,0.0))
        #
        return loss
