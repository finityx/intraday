from losses.main_losses import Losses
import tensorflow as tf
class Shilo(Losses):
    def __init__(self,data2net, objective):
        super().__init__(data2net, objective,'profit')
        self.prof_lim_2sharp = 0.25
        self.calc_loss = {'Army':self.generate_army_loss}

    def generate_army_loss(self,y_pred, y_true):
        min_prf = self.get_profit_army(y_pred, y_true)*100
        day_prf = tf.reduce_sum(min_prf,-1)
        prof = tf.reduce_mean(day_prf)
        std = tf.math.reduce_std(day_prf)
        sharp = tf.math.sqrt(tf.constant(251, dtype=tf.float32)) * (prof) / (1e-10 + std)
        shilo =  -((-tf.nn.elu(-(prof-self.prof_lim_2sharp)*10)/10)+self.prof_lim_2sharp + (tf.nn.sigmoid((prof-self.prof_lim_2sharp)*100)*sharp/20))
        return shilo
