from losses.main_losses import Losses
import tensorflow as tf
class Sortino(Losses):
    def __init__(self,data2net, objective):
        super().__init__(data2net, objective,'profit')
        self.calc_loss = {'Army':self.generate_army_loss}

    def generate_army_loss(self,y_pred, y_true):
        # def profit(y_pred, y_true):
        min_prf = self.get_profit_army_invest_changer_limit(y_pred, y_true)
        day_prof = tf.reduce_sum(min_prf, -1)*100

        mean_prf = tf.math.reduce_mean(day_prof)

        neg_min_prf     = tf.where(day_prof<0,day_prof,0)
        # neg_min_counter = tf.where(day_prof<0,1.0,0.0)
        # neg_min_counter = (tf.reduce_sum(neg_min_counter) + 1e-5)
        # neg_mean_min_prf = tf.reduce_sum(neg_min_prf)/neg_min_counter
        # neg_mean_sq_min_prf = tf.reduce_sum(neg_min_prf**2)/neg_min_counter
        # neg_min_std = tf.math.sqrt(neg_mean_sq_min_prf-neg_mean_min_prf**2)

        sortino = mean_prf/(1e-5+tf.math.reduce_std(neg_min_prf))

        return -sortino
