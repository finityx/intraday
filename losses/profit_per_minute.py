from losses.main_losses import Losses
import tensorflow as tf
class ProfitPerMinute(Losses):
    def __init__(self,data2net, objective):
        super().__init__(data2net, objective,'profit_per_minute')
        self.calc_loss = {'Army':self.generate_army_loss}

    # def generate_army_loss(self,y_pred, y_true):
    #     min_prf = self.get_profit_and_duration_army(y_pred, y_true)
    #     day_prof     = tf.reduce_sum(min_prf,-1) * 100
    #     # day_duration = tf.reduce_sum(min_duration,-1)
    #     # loss = -tf.reduce_mean(day_prof/day_duration)*390
    #     loss = -tf.reduce_mean(day_prof)
    #     return loss

    def generate_army_loss(self,y_pred, y_true):
        min_prf,min_duration = self.get_profit_and_duration_army(y_pred, y_true)
        day_prof     = tf.reduce_sum(min_prf,-1) * 100
        day_duration = tf.maximum(tf.reduce_sum(min_duration,-1),390.0)

        loss = -tf.reduce_mean(day_prof/day_duration)*390
        return loss