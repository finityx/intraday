from losses.main_losses import Losses
import tensorflow as tf
class Ivar(Losses):
    def __init__(self,data2net, objective):
        super().__init__(data2net, objective,'profit')
        self.calc_loss = {'Army':self.generate_army_loss}
        self.min_prof = 0.05

    def generate_army_loss(self,y_pred, y_true):
        min_prf = self.get_profit_army(y_pred, y_true)*100
        day_prf = tf.reduce_sum(min_prf,-1)

        ivar  = -tf.reduce_mean(tf.where(day_prf<self.min_prof,5*day_prf,day_prf))
        return ivar
