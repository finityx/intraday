from losses.main_losses import Losses
import tensorflow as tf
class ExpLoss(Losses):
    def __init__(self,data2net, objective):
        super().__init__(data2net, objective,'profit')
        self.calc_loss = {'Army':self.generate_army_loss}

    def generate_army_loss(self,y_pred, y_true):
        # def profit(y_pred, y_true):
        min_prf = self.get_profit_army_invest_changer(y_pred, y_true)*100
        day_prof = tf.reduce_sum(min_prf, -1)
        wprof = tf.exp(-day_prof/5) - 1

        return tf.reduce_mean(wprof)
