from tensorflow.keras import layers
import tensorflow as tf
import numpy as np

class BasicLstm():
    def __init__(self,start_dense_size,ticker_embedding,lstm_size,out_dense_size,stateful=False):
        self.name = 'basic_lstm'
        self.lstm_size = lstm_size
        self.start_dense_size = start_dense_size
        self.ticker_embedding = ticker_embedding
        self.out_dense_size = out_dense_size
        self.stateful = stateful

    def build(self,net):

        N = 1
        self.prev_days_lstm   = [layers.LSTM(self.lstm_size,return_sequences=False,stateful=False) for k in range(N)]
        self.prev_days_denses = [layers.Dense(n,activation='elu') for n in self.out_dense_size[0:-1]] + [layers.Dense(self.out_dense_size[-1])]

        self.avg_denses = [layers.Dense(n,activation='elu') for n in self.out_dense_size[0:-1]] + [layers.Dense(self.out_dense_size[-1])]

        self.lstm   = [layers.LSTM(self.lstm_size,return_sequences=True,stateful=self.stateful) for k in range(N)]

        if self.start_dense_size ==[]:
            self.start_dense=[]
        else:
            self.start_dense = [layers.Dense(n,activation='elu') for n in self.start_dense_size[0:-1]] + [layers.Dense(self.start_dense_size[-1])]
        self.denses = [layers.Dense(n,activation='elu') for n in self.out_dense_size]

    def call(self,inp):
        minutes = tf.tile(tf.constant((np.arange(inp.shape[3])/inp.shape[3]).reshape(1,1,1,-1,1),'float32'),[inp.shape[0],inp.shape[1],inp.shape[2],1,1])
        concat_net_inputs = tf.concat([inp,minutes],-1)
        flat_net = tf.reshape(concat_net_inputs,[-1,concat_net_inputs.shape[3],concat_net_inputs.shape[4]])
        # concat_net_inputs = inp
        out_pre = flat_net
        for prev_dense in self.prev_days_lstm:
            out_pre = prev_dense(out_pre)

        for prev_dense in self.prev_days_denses:
            out_pre = prev_dense(out_pre)

        out_pre = tf.reshape(out_pre,[concat_net_inputs.shape[0],concat_net_inputs.shape[1],concat_net_inputs.shape[2],-1])
        concat_net_inputs = concat_net_inputs[:,:,-1]

        if out_pre.shape[2]!=1:
            out_filter = tf.tile(tf.concat([out_pre[:, :, k:k + 1] for k in range(out_pre.shape[2] - 1)], -1), [1, 1, concat_net_inputs.shape[-2], 1])
            concat_net_inputs = tf.concat([out_filter,concat_net_inputs],-1)


        out_avg = concat_net_inputs
        for prev_dense in self.avg_denses:
            out_avg = prev_dense(out_avg)

        concat_net_inputs = tf.concat([concat_net_inputs,tf.tile(tf.reduce_mean(out_avg, 1, True), [1, concat_net_inputs.shape[1], 1, 1])],-1)

        out = tf.reshape(concat_net_inputs,[-1,concat_net_inputs.shape[-2],concat_net_inputs.shape[-1]])

        for l in self.start_dense:
            out = l(out)

        for l in self.lstm:
            out = l(out)

        for l in self.denses:
            out = l(out)

        out = tf.reshape(out,[concat_net_inputs.shape[0],concat_net_inputs.shape[1],concat_net_inputs.shape[2],-1])
        return out