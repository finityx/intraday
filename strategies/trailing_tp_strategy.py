from strategies.common import *
from strategies.none_strategy import NoneStrategyGenerator
import ib_insync
import pandas_market_calendars as mcal

class TrailingTPStrategyGenerator(NoneStrategyGenerator):
    def __init__(self,sign=[1,-1],trailing_tp=[0.005],sl=[0.02],weight=1.0):
        super().__init__(sign,weight)
        self.name = 'TrailingSL'
        self.trailing_tp = trailing_tp
        self.sl = sl

    def get_strategies_params(self):
        strategies_params = []
        for s in self.sign:
            for ttp,sl in zip(self.trailing_tp,self.sl):
                strategy_name = 'TrailingTP ' + ('long' if s > 0 else 'short')
                params = {}
                params['weight'] = self.weight
                params['sign'] = s
                strategy_name+=f' trailing_tp {ttp}'
                params['trailing_tp'] = ttp
                strategy_name += f' stop_loss {sl}'
                params['sl'] = sl
                strategies_params.append((strategy_name,params))
        return strategies_params

    def transform_stock_label(self,filename, params, delay, market_open, out_filename,n_minutes_in_day,asset):

        arrs = []
        for sname,strategy_params in self.get_strategies_params():
            arrs.append(self.transform_stock_label_per_strategy(filename, params, strategy_params, delay, market_open, out_filename, n_minutes_in_day, asset))
        np.save(out_filename, np.array(arrs))


    def transform_stock_label_per_strategy(self,filename, params,strategy_params, delay, market_open, out_filename,n_minutes_in_day,asset):

        odata    = get_data_dict(filename, market_open, delay=delay,nminutes=n_minutes_in_day,asset=asset)
        un_odata = get_data_dict(filename, market_open, delay=0,nminutes=n_minutes_in_day,asset=asset)
        label = OrderedDict()
        for p in params:
            label[p] = np.zeros_like(odata['close'])

        for mode in ['open2close','close2close','close2open']:
            for k in range(odata['close'].size-1):
                start_mode = 'open'
                if strategy_params['sign']>0:
                    best = odata['high'][k::] / odata[start_mode][k] - 1
                    worst = odata['low'][k::] / odata[start_mode][k] - 1
                    in_candle_best = abs(odata['high'][k::] / odata['open'][k::] - 1)
                else:
                    best  =  -(odata['low'][k::]/odata[start_mode][k]-1)
                    worst =  -(odata['high'][k::]/odata[start_mode][k]-1)
                    in_candle_best = abs(odata['low'][k::] / odata['open'][k::] - 1)

                cmin = np.append(0,np.minimum.accumulate(worst))[0:-1]

                stop_loss = worst <= -strategy_params['sl']
                tp = ((best - cmin) >= strategy_params['trailing_tp'])|(in_candle_best>strategy_params['trailing_tp'])

                itp = np.where(tp == True)[0]
                if itp.size == 0:
                    itp = cmin.size + 1
                else:
                    itp = itp[0]

                isl = np.where(stop_loss == True)[0]
                if isl.size == 0:
                    isl = cmin.size + 1
                else:
                    isl = isl[0]

                rel_ix = min([isl, itp])
                if rel_ix<best.size:
                    if itp<=isl:
                        label[mode][k] = cmin[itp]+strategy_params['trailing_tp']
                        nminutes = itp + 1
                    else:
                        label[mode][k] =  strategy_params['sl']
                        nminutes = isl + 1
                else:
                    isl = worst.size-1
                    label[mode][k] = worst[isl]
                    nminutes = isl + 1


                label[f'change_{mode}'][k] = strategy_params['sign']*(odata[start_mode][k+1]/odata[start_mode][k]-1)
                label[f'div_price_{mode}'][k] = odata['div_price'][k]
                label[f'duration'][k] = nminutes

        label['turnover']  = un_odata['turnover']
        label['weight'] = np.ones_like(label['weight']) * strategy_params['weight']
        arr = np.array(list(label.values())).T
        return arr

