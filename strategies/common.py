import pandas as pd
import numpy as np
from collections import OrderedDict
import pytz
from datetime import datetime,timedelta
import pytz

def get_data_dict(filename,market_open,delay,nminutes,asset):
    df = pd.read_csv(filename)
    df = df.sort_values(by='date')
    df.index = pd.to_datetime(df['date'])
    close2open_tomorrow  = df.next_open.iloc[-1]/df.close.iloc[-1] - 1
    close2open_yesterday = df.open.iloc[0] / df.last_close.iloc[-1] - 1

    if asset=='STOCK':
        if df.index.min().time().hour<15:
            eastern = pytz.timezone('UTC')
        else:
            eastern = pytz.timezone('Asia/Jerusalem')
        df.date = df.index.tz_convert(None).tz_localize(eastern).tz_convert(pytz.utc)
    elif asset == 'CRYPTO':
        df.date = df.index
    else:
        assert False, 'asset must be CRYPTO/STOCK'

    data = OrderedDict()
    for key in ['open', 'close', 'high', 'low', 'average','turnover']:
        data[key] = np.ones(nminutes) * np.nan

    data['close2open_tomorrow']  = np.ones(nminutes) * close2open_tomorrow
    data['close2open_yesterday'] = np.ones(nminutes) * close2open_yesterday

    i_pred_minute = np.array([(tm.hour - market_open[dt].hour) * 60 + (tm.minute - market_open[dt].minute) for dt, tm in zip(df.date.dt.date, df.date.dt.time)])

    i_pred_minute = i_pred_minute - delay
    II = (i_pred_minute < nminutes) & (i_pred_minute >= 0)

    keys_to_dalay = ['open', 'close', 'high', 'low', 'average']
    i_pred_minute = i_pred_minute[II]
    df = {k: np.array(df[k].tolist())[II] for k in df}
    for key in keys_to_dalay:
        data[key][i_pred_minute] = df[key]

    data['turnover'][i_pred_minute] = df['average'] * df['volume']*100


    df = pd.DataFrame(data)
    data['close'] = df['close'].fillna(method='ffill', axis=0).to_numpy()
    for key in ['open', 'close', 'high', 'low', 'average']:
        data[key][np.isnan(data[key])] = data['close'][np.isnan(data[key])]
    data['turnover'][np.isnan(data['turnover'])] = 0

    data['turnover'] = 1000+data['turnover']

    data['div_price'] = 1/data['average']
    return data
