from strategies.common import *

class NoneStrategyGenerator():
    def __init__(self,sign=[1,-1],weight=1,look_ahead=[1]):
        self.name = 'None'
        self.sign = sign
        self.weight = weight
        self.look_ahead=look_ahead

    def get_strategies_params(self):
        strategies_params = []
        for s in self.sign:
            strategy_name = 'None ' + ('long' if s > 0 else 'short')
            params = {}

            params['sign'] = s
            params['weight'] = self.weight
            for lh in self.look_ahead:
                strategy_name+=f' look_ahead {lh}'
                params['look_ahead'] = lh
                strategies_params.append((strategy_name,params))
        return strategies_params

    def transform_stock_data(self,filename, params, market_open, out_filename,n_minutes_in_day,asset):
        odata = get_data_dict(filename, market_open, delay=0,nminutes=n_minutes_in_day,asset=asset)
        data = OrderedDict()
        for p in params:
            if p in odata:
                data[p] = odata[p]
            else:
                data[p] = np.ones_like(odata['open'])*np.nan

        ref_field = data['open'][0]
        for key in ['open', 'close', 'high', 'low', 'average']:
            data[key] = data[key] / ref_field - 1
        data['turnover'] = np.log10(data['turnover']) / 6

        arr = np.array(list(data.values())).T
        np.save(out_filename, arr)

    def transform_stock_label(self,filename, params, delay, market_open, out_filename,n_minutes_in_day,asset):
        arrs = []
        for sname,strategy_params in self.get_strategies_params():
            arrs.append(self.transform_stock_label_per_strategy(filename, params, strategy_params, delay, market_open, out_filename, n_minutes_in_day, asset))
        np.save(out_filename, np.array(arrs))


    def transform_stock_label_per_strategy(self,filename, params,strategy_params, delay, market_open, out_filename,n_minutes_in_day,asset):
        odata    = get_data_dict(filename, market_open, delay=delay,nminutes=n_minutes_in_day,asset=asset)
        un_odata = get_data_dict(filename, market_open, delay=0,nminutes=n_minutes_in_day,asset=asset)
        label = OrderedDict()
        for p in params:
            label[p] = np.zeros_like(odata['close'])
        label['open2close'][0:(odata['open'].size-(strategy_params['look_ahead']-1))]  = (odata['close'][(strategy_params['look_ahead']-1)::]/odata['open'][0:(odata['open'].size-(strategy_params['look_ahead']-1))] - 1)*strategy_params['sign']*strategy_params['weight']
        label['close2close'][0:(odata['open'].size-(strategy_params['look_ahead']))]   = (odata['close'][(strategy_params['look_ahead'])::]/odata['close'][0:(odata['close'].size-(strategy_params['look_ahead']))] - 1)*strategy_params['sign']*strategy_params['weight']
        label['close2open'][0:(odata['open'].size-(strategy_params['look_ahead']))]    = (odata['open'][(strategy_params['look_ahead'])::]/odata['close'][0:(odata['close'].size-(strategy_params['look_ahead']))] - 1)*strategy_params['sign']*strategy_params['weight']

        label['div_price_open2close']  = odata['div_price']
        label['div_price_close2close'] = odata['div_price']
        label['div_price_close2open']  = odata['div_price']

        label['turnover']  = un_odata['turnover']

        arr = np.array(list(label.values())).T
        return arr

    def generate_orders(self,broker_runner,ticker,ticker_price,amount,params):
        assert False,'not implemented'