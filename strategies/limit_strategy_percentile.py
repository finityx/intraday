from strategies.common import *
from strategies.none_strategy import NoneStrategyGenerator
import ib_insync
import pandas_market_calendars as mcal

class LimitStrategyPercentile(NoneStrategyGenerator):
    def __init__(self,sign=[1,-1],limit_buy=[0.005],weight=1.0):
        super().__init__(sign,weight)
        self.name = 'LimitGenerator'
        self.limit_buy = limit_buy

    def get_strategies_params(self):
        strategies_params = []
        for s in self.sign:
            for tsl in self.limit_buy:
                strategy_name = 'Limit ' + ('long' if s > 0 else 'short')
                params = {}
                params['weight'] = self.weight
                params['sign'] = s
                strategy_name+=f' limit_buy {tsl}'
                params['limit_buy'] = tsl
                strategies_params.append((strategy_name,params))
        return strategies_params

    def transform_stock_label(self,filename, params, delay, market_open, out_filename,n_minutes_in_day,asset):

        arrs = []
        for sname,strategy_params in self.get_strategies_params():
            arrs.append(self.transform_stock_label_per_strategy(filename, params, strategy_params, delay, market_open, out_filename, n_minutes_in_day, asset))
        np.save(out_filename, np.array(arrs))


    def transform_stock_label_per_strategy(self,filename, params,strategy_params, delay, market_open, out_filename,n_minutes_in_day,asset):

        odata    = get_data_dict(filename, market_open, delay=delay,nminutes=n_minutes_in_day,asset=asset)
        un_odata = get_data_dict(filename, market_open, delay=0,nminutes=n_minutes_in_day,asset=asset)
        label = OrderedDict()
        for p in params:
            label[p] = np.zeros_like(odata['close'])

        for mode in ['open2close','close2close','close2open']:
            for k in range(odata['close'].size-1):
                start_mode,end_mode = mode.split('2')
                if strategy_params['sign']>0:
                    limit_price = (1-strategy_params['limit_buy'])*odata[start_mode][k]
                    label_curr = 0.0
                    nminutes = 1
                    if (limit_price>=odata['low'][k])&(limit_price<=odata['open'][k]):
                        label_curr = odata[start_mode][k+1]/limit_price - 1
                        nminutes = 10
                else:
                    limit_price = (1+strategy_params['limit_buy'])*odata[start_mode][k]
                    label_curr = 0.0
                    nminutes   = 1
                    if (limit_price <= odata['high'][k]) & (limit_price >= odata['open'][k]):
                        label_curr = -(odata[start_mode][k+1]/limit_price - 1)
                        nminutes = 10

                label[mode][k] = label_curr
                label[f'change_{mode}'][k] = strategy_params['sign']*(odata[start_mode][k+1]/odata[start_mode][k]-1)
                label[f'div_price_{mode}'][k] = odata['div_price'][k]
                label[f'duration'][k] = nminutes

        label['turnover']  = un_odata['turnover']
        label['weight'] = np.ones_like(label['weight']) * strategy_params['weight']
        arr = np.array(list(label.values())).T
        return arr

