import tensorflow as tf
import numpy as np
import os,dill
import shutil,copy
from datetime import datetime
from google.cloud import storage
import platform
import pandas_market_calendars as mcal
from google.cloud import bigquery
import time
import pandas as pd
import platform
if platform.system()=='Windows':
    from multiprocessing.pool import ThreadPool
    from threading import Thread

else:
    from multiprocessing.pool import ThreadPool
    from multiprocessing import Process as Thread




class QueryHandler():
    def __init__(self,replace_dates,asset,live,nthread=50):
        self.storage_client = storage.Client()
        self.query_jobs = []
        self.copy_jobs = []
        self.nq = 50
        self.nthread = nthread
        self.max_threads = 2000
        self.replace_dates = replace_dates
        self.asset = asset
        self.live = live

    def finalize(self):
        q_files = {}
        for j in self.query_jobs:
            if os.path.exists(j['path']) and (j['date'] not in self.replace_dates):
                continue
            if j['query'] not in q_files:
                q_files[j['query']] = j['path']
            else:
                if j['path']!=q_files[j['query']]:
                    self.copy_jobs.append((q_files[j['query']],j['path']))

        def process_q(q_group):
            tot_q = ' union all '.join([f'(select {i} as query_number,* from ({q})) \n' for i, q in enumerate(q_group)])
            if self.live:
                job_config = None
            else:
                job_config = bigquery.QueryJobConfig(priority=bigquery.QueryPriority.BATCH)

            query_job = bigquery.Client().query(tot_q,job_config=job_config)
            result = query_job.result().to_dataframe()
            for qn, df in result.groupby('query_number'):
                c_query = q_group[qn]
                df.to_csv(q_files[c_query])

        queries = list(q_files.keys())

        pool = ThreadPool(self.nthread)
        args = [queries[k:(k+self.nq)] for k in range(0,len(queries),self.nq)]
        pool.map(process_q,args)

        self.copy_jobs = list(set(self.copy_jobs))
        self.copy_jobs = [c for c in self.copy_jobs if os.path.exists(c[0])]
        if len(self.copy_jobs)==0:
            return

        if len(self.copy_jobs)==1:
            shutil.copy(*self.copy_jobs[0])
            self.copy_jobs = []
            return
        pool = ThreadPool(self.max_threads)
        pool.starmap(shutil.copy, self.copy_jobs)
        pool.close()
        self.copy_jobs = []
        self.query_jobs = []


    def append_qstr_path(self,qstr,path,date,ticker,data_name):
        self.query_jobs.append({'query':qstr,'path':path,'date':date,'ticker':ticker,'data_name':data_name})

class TransformationsHandler():
    def __init__(self,main_dir,tickers,strategies_params,ndays,market_open,data_sources,objectives,replace_dates,asset,live,n_minutes_in_day):
        self.jobs = []
        self.strategies_params=strategies_params
        self.market_open = market_open
        self.main_dir = main_dir
        self.replace_dates = replace_dates
        self.data_sources = data_sources
        self.objectives = objectives
        self.tickers = tickers
        self.max_threads = 2000
        self.asset = asset
        self.live = live
        self.n_minutes_in_day = n_minutes_in_day
        self.ndays = ndays

    def finalize(self):
        threads = []
        # self.jobs = [j for j in self.jobs if 'data' in j['data_name']]
        for j in self.jobs:
            os.makedirs(f"{self.main_dir}/numpy/{j['data_name']}" ,exist_ok=True)
            inpts = j['fcn_args']
            inpts['n_minutes_in_day'] = self.n_minutes_in_day
            inpts['market_open']  = self.market_open
            inpts['asset'] = self.asset
            inpts['out_filename'] = f"{self.main_dir}/numpy/{j['data_name']}/{j['date']}_{j['ticker']}.npy"

            if os.path.exists(inpts['out_filename']) and (j['date'] not in self.replace_dates):
                continue

            t = Thread(target=j['fcn'], kwargs=inpts)
            threads.append(t)
            t.start()

            while len(threads)>self.max_threads:
                time.sleep(0.01)
                threads = [t for t in threads if t.is_alive()]

        [t.join() for t in threads]

        def _float_feature(value):
            return tf.train.Feature(float_list=tf.train.FloatList(value=value))

        def create_one_tfrecord(dates_str,tickers,strategies_params,out_filename):
            writer = tf.io.TFRecordWriter(out_filename)
            data_sources = [ds for ds in self.data_sources]
            for o in self.objectives:
                data_sources+= o.get_data_sources()

            date_dict = {}
            for source in data_sources:
                if 'label' in source.name:
                    date_dict[source.name] = np.zeros([len(tickers), len(strategies_params), self.n_minutes_in_day, len(source.params)], dtype='float32')
                    date_str = dates_str[-1]
                    for it, ticker in enumerate(tickers):
                        filename = f"{self.main_dir}/numpy/{source.name}/{date_str}_{ticker}.npy"
                        if os.path.exists(filename):
                            date_dict[source.name][it] = np.load(filename)

                else:
                    date_dict[source.name] = np.zeros([len(tickers),len(dates_str),self.n_minutes_in_day, len(source.params)], dtype='float32')

                    for idate,date_str in enumerate(dates_str):
                        for it, ticker in enumerate(tickers):
                            filename = f"{self.main_dir}/numpy/{source.name}/{date_str}_{ticker}.npy"
                            if os.path.exists(filename):
                                date_dict[source.name][it][idate] = np.load(filename)

            features = {k: _float_feature(v.reshape(-1)) for k, v in date_dict.items()}
            example = tf.train.Example(features=tf.train.Features(feature=features))
            writer.write(example.SerializeToString())
            writer.close()

        os.makedirs(f"{self.main_dir}/tf_record/", exist_ok=True)

        threads = []
        dates = sorted(list(self.market_open.keys()))

        for idate in range(self.ndays,len(dates)+1):
            c_dates = dates[(idate - self.ndays):idate]
            date = dates[idate-1]
            out_filename = f"{self.main_dir}/tf_record/{date}.tfrec"
            if os.path.exists(out_filename) and (date not in self.replace_dates):
                continue
            t = Thread(target=create_one_tfrecord, kwargs={'dates_str':[d.isoformat() for d in c_dates],'strategies_params':self.strategies_params,'tickers':self.tickers,'out_filename':out_filename})
            threads.append(t)
            t.start()
            while len(threads)>self.max_threads:
                time.sleep(0.01)
                threads = [t for t in threads if t.is_alive()]
        [t.join() for t in threads]

    def add_job(self,ticker,date,data_name,fcn,fcn_args):
        assert type(fcn_args)==dict
        self.jobs.append({'ticker':ticker,'date':date,'data_name':data_name,'fcn':fcn,'fcn_args':fcn_args})

    def run_debug(self):
        for j in self.jobs:
            j['fcn'](**j['fcn_args'],market_open=self.market_open,out_filename='aaaaa')

class Data2Net():
    def __init__(self,main_path,tickers,ndays,strategy_generators,start_date,end_date,data_sources,objectives,replace_dates = [],asset='STOCK',live=True,n_minutes_in_day=390):
        self.main_path = main_path
        self.ndays = ndays
        self.tickers = tickers
        self.strategy_generators = strategy_generators
        self.replace_dates = replace_dates
        nyse = mcal.get_calendar('NYSE')

        if   asset == 'STOCK':
            market_open = nyse.schedule(start_date, end_date).market_open.to_list()
            self.open_times = [d.time() for d in market_open]

        elif asset == 'CRYPTO':
            market_open = pd.date_range(start_date, end_date,freq='d')
            zero_date = datetime(2020,1,1).time()
            self.open_times = [zero_date for d in market_open]
        else:
            assert False, 'asset must be CRYPTO/STOCK'

        self.dates = [d.date() for d in market_open]

        self.market_open = dict(zip(self.dates,self.open_times))
        self.data_sources = data_sources
        self.objectives = objectives
        self.asset  = asset
        self.live   = live
        self.n_minutes_in_day = n_minutes_in_day

    def get(self):
        q_handler = QueryHandler(replace_dates = self.replace_dates,asset=self.asset,live=self.live)

        for ds in self.get_datasources():
            dr = f'{self.main_path}/data/{ds.name}'
            os.makedirs(dr,exist_ok=True)
            ds.get(main_path=dr,tickers=self.tickers,dates=self.dates,query_handler = q_handler)
        q_handler.finalize()

    def transform(self):
        curr_snames = self.strategy_generators.get_strategies_params()
        self.strategy_name  =[si[0] for si in curr_snames][0]
        self.strategies_params =[si[1] for si in curr_snames]

        tranformations_handler = TransformationsHandler(main_dir=f'{self.main_path}/transformed_data',market_open=self.market_open,ndays=self.ndays,
                                                        tickers=self.tickers,strategies_params=self.strategies_params,
                                                        data_sources= self.data_sources,objectives=self.objectives,replace_dates = self.replace_dates,
                                                        asset=self.asset,live=self.live,n_minutes_in_day =self.n_minutes_in_day)

        for ds in self.get_datasources():
            dr = f'{self.main_path}/data/{ds.name}'
            ds.transform(data_dir=dr,strategy_generator=self.strategy_generators,tranformations_handler = tranformations_handler)
        tranformations_handler.finalize()

    def get_datasources(self):
        data_sources = [ds for ds in self.data_sources]
        for o in self.objectives:
            data_sources += o.get_data_sources()
        return data_sources

    def update_table(self,curr_date_time):
        for ds in self.get_datasources():
            ds.update_table(curr_date_time)


if __name__=='__main__':
    from data.stock_data import *
    import time
    from objective import objective
    from strategies import none_strategy,trailing_sl_strategy
    qst = f""" select ticker,max_invest from algo_dataset.ticker_turnover order by max_invest desc """

    df = client.query(qst).result().to_dataframe()
    tickers = df['ticker'].tolist()


    objectives = []
    d2n = Data2Net(main_path=f'd:\intdy',tickers=tickers[0:10],start_date='2021-01-01',end_date='2021-05-01',data_sources=[StockData()],
                   objectives=[objective.Objective(name='normal',label=StockLabel(delay=2))],replace_dates=[datetime(2021,1,4).date()],
                   strategy_generators= [trailing_sl_strategy.TrailingSLStrategyGenerator()])

    t  = time.time()

    d2n.get()
    print(time.time()-t)
    d2n.transform()
    print(time.time() - t)